r"""
Test of parallel computation of Maass forms.
"""
import json
import os

from psage import *
import mwfdb
import dbbase
from mwfdb.maass_forms_db import *
from mwfdb.find_maass_forms import *
from dbbase.dicts import DictLowerJson
from sage.modular.arithgroup.arithgroup_generic  import ArithmeticSubgroup

import logging
log = logging.getLogger(__name__)

# if not hasattr(Maassform_DB,"query"):
#     setup_maass_db()

# Example of Maass form
M = MaassWaveForms(Gamma0(1))
MF = None
for dir in [
        "/home/pmzfs/Devel/maassformdb/data/", ## preferred
        "/home/fredrik/Programming/Projects/maassformdb/data/", ## preferred
                "/home/stromberg/program/maassformdb/data/",    ## preferred
                os.getenv("HOME")+"/Dropbox/Data/maaassformdb/data" ## falback
            ]:
    try:
        MF=MaassDBFiles(dir,verbose=0)
    except RuntimeError:
        pass
if MF is None:
    log.critical("No Files DB!")
host = os.getenv('MONGODB_HOST','localhost')
port = os.getenv('MONGODB_PORT', 37010)
user = os.getenv('MONGODB_USER', 37010)
password =  os.getenv('MONGODB_PASSWORD','')
DB = MaassDBMongo(host=host,port=port,user=user,password=password)
import datetime

def Maasswaveform_from_DB(r):
    r"""
    Construct a Maass waveform from a Maassform_DB instance.
    """
    if isinstance(r,dict):
        r = find_MaassformDB(r).first()

    G = MySubgroup(o2=r.group_p2,o3=r.group_p3)
    char_sage = getDircharSageFromConrey(DB,9,1)
    M = MaassWaveForms(G,weight = r.weight,sym_type = r.symmetry,ch=char_sage,verbose=1)
    d = r.dimension
    numc = r.numc
    coeffs = []
    cusp_transf = {}
    for i in range(len(G.cusps())):
        cusp = G.cusps()[i]
        for cuspdb in r.group_cusps_list:
            a = cuspdb.a; b = cuspdb.b
            c = Cusp(a,b)
            if G.are_equivalent(cusp,c):
                cusp_transf[ (a,b) ] = i #cusp
                break
    assert len(cusp_transf) == G.ncusps()
    res = []
    #print "cusp_transf=",cusp_transf
    #print [x.id for x in cusp_transf]
    #return r
    for i in range(d):
        c = {}
        for cusp in range(len(cusp_transf)):
            c[cusp]={}
            for x in r.coefficient_list:
                c[cusp_transf[(x.cusp.a,x.cusp.b)]][x.nr] = x.value
            F = Maasswaveform(M,r.eigenvalue,compute = 0,coefficients={0:c})
            res.append(F)
    return res



def insert_noncongruence_form(MF):
    rec = {u'Character': 1, u'Contributor': u'HT', u'Cusp_evs': [1], u'Dim': 1, u'Dimension': 1,
           u'Eigenvalue': 9.53369526135355, u'Error': 2.0894397323445446e-13,
           u'M0': 0, u'Norm': 1.0, u'Symmetry': 1, u'Weight': 0.0, u'Y': 0}
    G = MySubgroup(Gamma0(2))
    rec['group_p2']=G.permS.export_as_string()
    rec['group_p3']=G.permR.export_as_string()
    rec['group_index']=G.index()
    rec['group_sign']=G.signature()
    date = str(datetime.datetime.now()).split(".")[0]
    print "rec=",rec
    metadata = "Creator: Fredrik Stromberg, Email: fredrik314@gmail.com, URL:http://www.github.com/fredstro/psage, date:{0}, License: CC BY-AS, Algorithm: Psage".format(date)
    print "metadata=",metadata
    return MF.format_record(rec,metadata=metadata)


red = "\033[31m"   ## ANSI red text
green = "\033[32m"   ## ANSI red text
yellow= "\033[33m"   ## ANSI yellow text
def show_table_summary(MF,**kwds):
    r"""
    Show a summary of what exists in the table
    """
    gamma0 = {}
    generic = {}
    allforms=[]
    badforms=[]
    s=" Database of Maass forms"
    levels = []
    groups = []
    weights = []
    characters = {}#
    problems=kwds.get('problems',False) #] if True we only print problematic groups, i.e. those where the dichotomy symmetry <=> maass forms fails in either direction.
    verbose=kwds.get('verbose',0)
    for nrange in MF.listdir('gamma0'):
        dname = 'gamma0/{0}'.format(nrange)
        for n in MF.listdir(dname):
            if n not in levels:
                levels.append(n)
            gamma0[n]={}
            dname1 = '{0}/{1}'.format(dname,n)
            for wt in MF.listdir(dname1):
                if wt not in weights:
                    weights.append(wt)
                gamma0[n][wt]={}
                dname2 = '{0}/{1}'.format(dname1,wt)
                for ch in MF.listdir(dname2):
                    gamma0[n][wt][ch]={}
                    dname3 = '{0}/{1}'.format(dname2,ch)
                    for R in MF.listdir(dname3):
                        dname4 = '{0}/{1}'.format(dname3,R)
                        gamma0[n][wt][ch][R]={}
                        for sym in MF.listdir(dname4):
                            fname =  '{0}/{1}/maassform.json'.format(dname4,sym)
                            rec = MF.read_file(fname,format='json')
                            gamma0[n][wt][ch][R]={'form':rec}
                            cname =  '{0}/{1}/coefficients.json'.format(dname4,sym)
                            c = MF.read_file(cname,format='json')
                            #print "c=",c,type(c)
                            num =  c['coefficients']['0']['num']
                            cm1 =  c['coefficients']['0']['coefficients']['0']['-1']
                            gamma0[n][wt][ch][R]['numc']=num
                            gamma0[n][wt][ch][R]['c(-1)']=cm1
    t='generic/014/14_01_00_02_01/7b385e9c16d42a/0.0'
    for mu in MF.listdir('generic'):
        dname = 'generic/{0}'.format(mu)
        generic[mu]={}
        for sig in MF.listdir(dname):
            generic[mu][sig]={}
            dname1 = '{0}/{1}'.format(dname,sig)
            for p3 in MF.listdir(dname1):
                generic[mu][sig][p3]={}
                dname2 = '{0}/{1}'.format(dname1,p3)
                for wt in MF.listdir(dname2):
                    generic[mu][sig][p3][wt]={}
                    dname3 = '{0}/{1}'.format(dname2,wt)
                    for R in MF.listdir(dname3):
                        #if dname3==t:
                        #    print "R=",R
                        #    sig_t = sig
                        #    g_t = p3
                        if 'checking' in R:
                            tmp,R1,R2=R.split('_')
                            checked_file = 'checked_{0}_{1}'.format(R1,R2)
                            #print "Checked file:",checked_file
                            if checked_file in MF.listdir(dname3):
                                # remove the 'checking' file since we have already completed the check.
                                fname =  '{0}/{1}'.format(dname3,R)
                                #print "Removing ",fname
                                MF.delete_file(fname)
                            continue
                        if 'checked' in R:
                            #continue
                            tmp,R1,R2=R.split('_')
                            if not generic[mu][sig][p3][wt].has_key('checked'):
                                generic[mu][sig][p3][wt]['checked']=[]
                            generic[mu][sig][p3][wt]['checked'].append((R1,R2))
                            continue
                        fname =  '{0}/{1}/maassform.json'.format(dname3,R)
                        rec = MF.read_file(fname,format='json')
                        generic[mu][sig][p3][wt][R]={'form':rec,'filename':fname}
                        cname =  '{0}/{1}/coefficients.json'.format(dname3,R)
                        c = MF.read_file(cname,format='json')
                        if rec.get('error',0)>1e-10:
                            badforms.append(rec)
                            rec = {}
                        if rec=={}:
                            # remove this record
                            if MF.path_exists(fname):
                                MF.delete_file(fname)
                            if MF.path_exists(cname):
                                MF.delete_file(cname)
                            dname5 = '{0}/{1}/'.format(dname3,R)
                            if MF.path_exists(dname5):
                                MF.delete_dir(dname5)
                        if 'coefficients' not in c:
                            print "c=",c.keys()
                            print "cname=",cname
                            continue
                        num =  c['coefficients']['0']['num']
                        cm1 =  c['coefficients']['0']['coefficients']['0']['-1']
                        generic[mu][sig][p3][wt][R]['numc']=num
                        generic[mu][sig][p3][wt][R]['c(-1)']=cm1
                        allforms.append(rec)
#    print generic['014'][sig_t][g_t]['0.0'].keys()
    for ix in generic.keys():
        if kwds.get('index')<>None and kwds.get('index')<>int(ix):
            continue

        s = "Index {0} \n".format(ix)
        for sig in generic[ix].keys():
            sigl = [int(x) for x in sig.split("_")]
            s+= "Signature : {0} \n".format(sigl)
            for g in generic[ix][sig].keys():
                checked = generic[ix][sig][g]['0.0'].get('checked')
                R = MyPermutation(g)
                sg = ""
                sg += "\t {0}\t ".format(R)
                #print "g=",g
                #print "R=",R.cycles()
                #print "sigl=",sigl
                G = group_from_sig_p3(sigl,R)
                sym=0
                if G.has_modular_correspondence():
                    sg += G.modular_correspondence_string()
                    sym=1
                if G._has_symmetry_type_IIb():
                    i,j,A = G._symmetry_type_IIb[0]
                    if G.ncusps()==2:
                        sg += "Reflection in cusp {0}".format(G.cusps()[i])
                        sym=3
                if sigl[1]==1:
                    sg+=" Cycloidal!"; sym=1
                if sym==0:
                    sg+=" No symmetry!       "
                # Check if ew have a subgroup of something....
                #DBG = DBgroupfromMyGroup(G)
                #supergroups = DBG.supergroups()
                supergroups = find_supergroups(G)
                if supergroups<>None:
                    for g1 in supergroups:
                        G1 = g1 # MyGroupfromDBgroup(g1)
                        if G1.is_congruence()==1:
                            sg+=" subgroup of congruence group! "; sym=2
                        elif G1.is_symmetric()==1:
                            sg+=" subgroup of symmetric group! "; sym=2
                if checked <> None:
                    sg+= "\t  Checked: {0} ".format(checked)
                evs = generic[ix][sig][g]['0.0'].keys()
                evs.sort()
                sg+="\n"
                if 'checked' in evs:
                    i=evs.index('checked')
                    evs.pop(i)
                #if problems and ((len(evs)>0 and sym<>0) or (len(evs)==0 and sym==0)):
                #    continue
                #print s
                #if g==g_t and sig==sig_t:
                #    print "Rlist3",generic[ix][sig][g_t]['0.0'].keys()
                #    print "evs1=",evs
                for ev in evs:
                    if ev<>'checked':
                        if sym==0:
                            sg+= "{0}\t \t {1} \t {2}{3} \n".format(red,ev, generic[ix][sig][g]['0.0'][ev]['filename'],"\033[m")
                        elif sym==2:
                            sg+= "{0}\t \t {1} \t {2}{3} \n".format(green,ev, generic[ix][sig][g]['0.0'][ev]['filename'],"\033[m")
                        elif sym==3:
                            sg+= "{0}\t \t {1} \t {2}{3} \n".format(yellow,ev, generic[ix][sig][g]['0.0'][ev]['filename'],"\033[m")
                        else:
                            sg+= "\t \t {0} \t {1} \n".format(ev, generic[ix][sig][g]['0.0'][ev]['filename'])
                if problems and ((len(evs)>0 and sym<>0) or (len(evs)==0 and sym==0)):
                    pass
                else:
                    if verbose>1:
                        print "G=",G
                        print "g=",g
                        print "sig= =",sig
                        print "evs=",evs
                        print "sym=",sym
                        print "g=",g
                        print "Rlist",generic[ix][sig][g]['0.0'].keys()
                    s += sg
        print s


    return # generic,allforms,badforms


def group_from_sig_p3(sig,p3):
    mu,h,e2,e3,g=sig
    Sl=[0 for i in range(mu)]
    j=0
    for j in range(e2):
        Sl[j]=j+1
    j = e2
    while j<mu-1:
        Sl[j]=j+2
        Sl[j+1]=j+1
        j=j+2
    p2 = MyPermutation(Sl)
    p3 = MyPermutation(p3)
    G = MySubgroup(p2,p3)
    return G

def insert_records_from_mongo_to_files(DB,MF,search,**kwds):
    l = DB.get_Maass_forms(search,limit=2000,**kwds)
    print "Will insert {0} records!".format(len(l))
    for r in l:
        #print "r=",r
        #print "id=",id
        #MF=MaassDBFiles("/home/purem/cvzx53/extra/data/maassformdb/",verbose=0)
        if r['Contributor']=='FS':
            creator = 'Fredrik Stromberg <fredrik314@gmail.com>'
            date = r.get('date','2012')
            date = str(date).split(".")[0]
            algorithm = "Implementation in Psage"
            url = "http://www.github.com/fredstro/psage"
            mylicense = "CC BY-SA"
        elif r['Contributor']=='HT':
            creator = 'Holger Then'
            date = "2011"
            algorithm = "Implementation in C++"
            url = ""
            mylicense = ""
        N = r['Level']
        G = MySubgroup(Gamma0(N))
        cusps =  [(int(x.numerator()),int(x.denominator())) for x in G.cusps()]
        metadata = "Creator: {0}, Date: {1}, Algorithm: {2}, URL: {3}, License:{4}".format(creator,date,algorithm,url,mylicense)
        print "cusps=",cusps,type(cusps),type(cusps[0])
        r['group_cusps']=cusps
        MF.insert_record(r,metadata=metadata,type='form')
        id = r['_id']
        print "id for coefficients=",id
        C = DB.get_coefficients({'_id':id},get_date=1)
        C,date = C[0]
        print "upload date",date
        creator = 'Fredrik Stromberg <fredrik314@gmail.com>'
        algorithm = "Implementation in Psage"
        url = "http://www.github.com/fredstro/psage"
        mylicense = "CC BY-SA"
        #print "c5000=",C[0][0][0][5000]
        metadata = "Creator: {0}, Date: {1}, Algorithm: {2}, URL: {3}, License:{4}".format(creator,date,algorithm,url,mylicense)
        #print "\n r=",r
        if len(C)>0:
            MF.insert_coefficients(r,{'data':C,'metadata':metadata})
        #return
   #MF.insert_coefficients(r,{'data':C,'metadata':{'creator':'Fredrik Stromberg','date':'2012'}})
    #return



def find_MaassformDB(F={},verbose=0,**kwds):
    r"""
    Input can e.g. be the output dict from an instance of MongoDB
    """
    F=lower_case_dict(merge_dicts(F,kwds))
    level = int(F.get('level')) if F.has_key('level') else None
    character_no = int(F.get('character')) if F.has_key('character') else None
    eigenvalue = float(F.get('eigenvalue')) if F.has_key('eigenvalue') else None
    symmetry = int(F.get('symmetry')) if F.has_key('symmetry') else None
    weight = float(F.get('weight')) if F.has_key('weight') else None
    # Any subgroup which is not Gamma0 has to be supplied explicitly as a MySubgroup instance
    if F.has_key('group'):
        G = F['group']
        if isinstance(G,ArithmeticSubgroup):
            G = MySubgroup(G)
        if not isinstance(G,MySubgroup_class):
            raise ValueError,"Need group to be a MySubgroup class"
        group = DBgroupfromMyGroup(G,commit=0)
    elif level<>None:
        G = MySubgroup(Gamma0(level))
        group = DBgroupfromMyGroup(G,commit=0)
    else:
        group = None
    if character_no <> None:
        character = get_Character_DB(level,character_no)
    else:
        character = None
    eps = float(F.get('eps',1e-8)); r1=None; r2=None
    if eigenvalue <> None:
        r1 = eigenvalue - eps
        r2 = eigenvalue + eps
    if verbose>0:
        print "eigenvalue=",eigenvalue
        print "error tolerance=",eps
        print "r1=",r1
        print "r2=",r2
        print "group=",group
        print "symmetry=",symmetry
        print "weight=",weight
        print "char_no=",character_no
        if character<>None:
            print "character=",character
            print "character_id=",character.id

    f = Maassform_DB.query
    if eigenvalue <> None:
        f = f.filter(
            Maassform_DB.eigenvalue<=r2,
            Maassform_DB.eigenvalue>=r1)
    if group <> None:
        f = f.filter(
            Maassform_DB.group_p2==group.p2,
            Maassform_DB.group_p3==group.p3)
    if level <> None:
        f = f.filter(
            Maassform_DB.group_level==group.generalised_level,
            Maassform_DB.group_level==group.generalised_level)
    if symmetry <> None:
        f = f.filter(
            Maassform_DB.symmetry == symmetry)
    if weight <> None:
        f = f.filter(
            Maassform_DB.weight == weight)
    if character <> None:
        f = f.filter(
            Maassform_DB.character_id == character.id)
    return f

def find(self,data={},**kwds):
    r"""
        Find Maass forms matching the data
        """

    find_data = arg_to_search_parameters(data,**kwds)
    q = Maassforms_DB.filter(Maassform_DB.eigenvalue>=r1,
                             Maassform_DB.eigenvalue<=r2,
                             Maassform_DB.group.generalised_level>=level1,
                             Maassform_DB.group.generalised_level<=level2
                             )





def construct_query(data={},**kwds):
    r"""
    Take user supplied search parameters and extract the proper values in a format that can be used.
    """
    data = lower_case_dict(data)
    kwds = lower_case_dict(kwds)
    if isinstance(data,(int,Integer)):
        data={'Level':data}
    tol=data.get('tol',kwds.get('tol',1e-6))
    R = data.get('eigenvalue',data.get('r',kwds.get('eigenvalue',kwds.get('r',None))))
    R1 = data.get('eigenvalue1',data.get('r1',kwds.get('eigenvalue1',kwds.get('r1',R))))
    R2 = data.get('eigenvalue2',data.get('r2',kwds.get('eigenvalue2',kwds.get('r2',R))))
    level=data.get('level',data.get('n',kwds.get('level',kwds.get('n',None))))
    level1=data.get('l1',data.get('level1',kwds.get('l1',kwds.get('level1',None))))
    level2=data.get('l2',data.get('level2',kwds.get('l2',kwds.get('level2',None))))
    ch=data.get('char',data.get('ch',kwds.get('char',kwds.get('ch',None))))
    ch1=data.get('ch1',data.get('char1',kwds.get('ch1',kwds.get('char1',ch))))
    ch2=data.get('ch2',data.get('char2',kwds.get('ch2',kwds.get('char2',ch))))
    wt=data.get('wt',data.get('weight',kwds.get('wt',kwds.get('weight',None))))
    wt1=data.get('wt1',data.get('weight1',kwds.get('wt1',kwds.get('weight1',wt))))
    wt2=data.get('wt2',data.get('weight2',kwds.get('wt2',kwds.get('weight2',wt))))
    dim=data.get('d',data.get('dim',kwds.get('d',kwds.get('dim',None))))
    dim1=data.get('d1',data.get('dim1',kwds.get('d1',kwds.get('dim1',dim))))
    dim2=data.get('d2',data.get('dim2',kwds.get('d2',kwds.get('dim2',dim))))
    numc=data.get('nc',data.get('numc',kwds.get('nc',kwds.get('numc',None))))
    numc1=data.get('nc1',data.get('numc1',kwds.get('nc1',kwds.get('numc1',numc))))
    numc2=data.get('nc2',data.get('numc2',kwds.get('nc2',kwds.get('numc2',numc))))
    symmetry = data.get('symmetry',kwds.get('symmetry',None))
    ## Build a search, starting with the easiest parameters
    list_of_keys = ['eigenvalue','group_level','ch','wt','dim','numc']
    filter_params = {'eigenvalue':(R,R1,R2),'group_level':(level,level1,level2),
                     'character_n':(ch,ch1,ch2),'weight':(wt,wt1,wt2),
                     'dimension':(dim,dim1,dim2),'numc':(numc,numc1,numc2)}
    q = Maassform_DB.query
    for key in filter_params.keys():
        if filter_params[key][0]<>None:
            q = append_query(q,key,'==',filter_params[key][0])
        else:
            q = append_query(q,key,'>=',filter_params[key][1])
            q = append_query(q,key,'<=',filter_params[key][2])
    if q.count()<=1:
        return q
    # Else filter on possible symmetry
    q = append_query(q,'symmetry','==',symmetry)
    if q.count()<=1  or level<>None or level1<>None or level2<>None:
        return q
    # Else we see if we also have a group to filter on
    ## Check if we have a group which is not Gamma0(N)
    group_p2=data.get('group_p2',kwds.get('group_p2',None))
    group_p3=data.get('group_p2',kwds.get('group_p2',None))
    group_name=data.get('group_name',kwds.get('group_name',None))
    group = data.get('group',kwds.get('group',None))
    if group_name==None and group_p2==None and group_p3==None and group==None:
        return q
    if isinstance(group,MySubgroup_class):
        group = DBgroupfromMyGroup(G,commit=0)
    if isinstance(group,SL2Zsubgroup):
        p2 = group.p2; p3 = group.p3
    else:
        if isinstance(group_p2,str):
            p2 = group_p2
        elif isinstance(group_p2,MyPermutation):
            group_p2.set_rep(3); p2 = str(group_p2)
        if isinstance(group_p3,str):
            p3 = group_p3
        elif isinstance(group_p3,MyPermutation):
            group_p3.set_rep(3); p3 = str(group_p3)
    if isinstance(p2,(str,unicode)) and isinstance(p3,(str,unicode)):
        q = append_query(q,"group_p2",group.p2)
        q = append_query(q,"group_p3",group.p3)
    elif isinstance(group_name,(str,unicode)):
        q = append_query(q,"group_name",group_name)
    return q


## Helper functions

def append_query(q,tag='level',op='==',value=1,verbose=0):
    assert isinstance(q,sqlalchemy.orm.query.Query)
    if value == None:
        return q
    if isinstance(value,float):
        value = RR(value)  # necessary in order to not lose precision in the eval below
    qfilter = eval("Maassform_DB.{0}{1}{2}".format(tag,op,value))
    if verbose>0:
        print "value(RHS)=",value
        print "filter=",qfilter
    return q.filter(qfilter)



def MaassformDB(F={},**kwds):
    r"""
    Create an instance of Maassform_DB or pull an existing one from the database.
    """
    input_args = DictLowerJson(F)
    input_args.update(kwds)
    level = input_args.get('level')
    character_mod = input_args.get('character_mod')
    character_no = input_args.get('character_no')
    if character_mod<>None and level<>None:
        character_mod = level
    else:
        character_no = input_args.get('character',0)
    eigenvalue = input_args.get('eigenvalue',0.0)
    symmetry = input_args.get('symmetry',0)
    weight = input_args.get('weight',0)
    if isinstance(level,(int,Integer)) and level > 0:
        G = MySubgroup(Gamma0(level))
    else:
        ## Check if we supplied a group
        G = F.input_args.get('group')
    if isinstance(G,MySubgroup_class):
        group = DBgroupfromMyGroup(G)
    elif isinstance(G,SL2Zsubgroup):
        group = G
    else:
        raise ValueError,"There is no group specified in the input! Got group = {0}".format(G)
    dim = int(F.get('Dim',1))
    character = CharacterDB(level,character_no)
    ## Check if there is a Maass form already in the database matching this.
    ## This is the error tolerance in the eigenvalue...
    tol = input_args.get('tol',1e-12)
    eigenvalue1=eigenvalue-tol; eigenvalue2=eigenvalue+tol
    q = construct_query(group_p2=group.p2,group_p3=group.p3,
                        eigenvalue1=eigenvalue1,eigenvalue2=eigenvalue2,
                        character_mod = character_mod,character_nr=character_nr,
                        symmetry = symmetry,
                        weight=weight)
#    f = find_MaassformDB(level=level,group=group)

    # Data used to compute this Maass form
    M0 =  input_args.get('M0',0)
    Y =  input_args.get('Y',0)
    error = input_args.get('Error',0)
    # metadata
    contributor = input_args.get('Contributor','')
    metadata = input_args.get('Metadata','') # could be e.g. program names
    date = input_args.get('date','')
    numc = input_args.get('Numc',0)
#    res = find_MaassformDB_from_dict(F)

    if q.count()>1: # Maass form exists in database and constructing data is not unique
        print "More than one Maassform in database match the form you want to create/insert. Please use a more detailed specification!"
        return -1
    elif q.count()==0:
        print "Insert new form !"
        f = Maassform_DB(eigenvalue=eigenvalue,
                         group_p2=group.p2,group_p3=group.p3,group_name = group.name,
                         group_index = group.psl2z_index,
                         #group_cusps = str(map(lambda x:(x.numerator(),x.denominator()), GG.cusps()))
                         level = level,
                         character_mod = character_mod,
                         character_nr = character_nr,
                         symmetry = symmetry,
                         weight = weight,
                         contributor = contributor,
                         metadata = metadata,
                         date = date,
                         M0 = M0,
                         Y = Y,
                         error = error)
    C = f.get('Coefficients',kwds.get('Coefficients'))

def insert_Maassforms(l,DB,verbose=0,commit_at_end=1,add_coeffs=1):
    if isinstance(l,(list,tuple)):
        for F in l:
            insert_Maassforms(F,DB,verbose=verbose,commit_at_end=commit_at_end,add_coeffs=add_coeffs)
    elif isinstance(l,dict):
        F = lower_case_dict(l)
        f = create_MaassformDB_from_dict(F,verbose=0,commit=0)
        if add_coeffs==1:
            ## If we have previous coefficients we make a check that we don't add the same again.
            print "numc=",f.numc
            if f.numc>0:
                add_coefficients(DB,l,check=True)
            else:
                add_coefficients(DB,l,check=False)
    else:
        raise ValueError,"Need input as dict!"

    # and then commit
    if commit_at_end==0:
        return
    try:
        mwfdb_session.commit()
    except:
        mwfdb_session.rollback()
    finally:
        mwfdb_session.close()


def create_MaassformDB_from_dict(F,verbose=0,commit=1):
    r"""
    Input can e.g. be the output dict from an instance of MongoDB
    """
    F = lower_case_dict(F)
    res = find_MaassformDB(F)
    if res.count() == 1:
        return res[0]
    elif res.count() > 1:
        raise ValueError,"Warning! More than one Maass form match this description!"
    level = int(F.get('level',1))
    character_no = int(F.get('character',0))
    eigenvalue = float(F.get('eigenvalue',0))
    symmetry = int(F.get('symmetry',0))
    weight = float(F.get('weight',0))
    G = MySubgroup(Gamma0(level))
    group = DBgroupfromMyGroup(G,commit=0)
    cusps = [ (x.numerator(),x.denominator()) for x in G.cusps()]
    if verbose>0:
        print "group=",group
        print "cusps=",cusps
    dim = int(max(F.get('dim',1),1))
    character = get_Character_DB(modulus=level,number=character_no)
    # Data used to compute this Maass form
    M0 =  int(F.get('m0',0))
    Y =  float(F.get('y',0))
    error = float(F.get('error',0))
    # metadata
    contributor = unicode(F.get('contributor',''))
    metadata = unicode(F.get('metadata','')) # could be e.g. program names
    date = (F.get('date',''))
    if date=='':
        date = None
    if G.is_Gamma0():
        group_name=str(G)
    else:
        group_name=''

    f = Maassform_DB(eigenvalue=eigenvalue,
                     group_p2=group.p2,
                     group_p3=group.p3,
                     group_level=group.generalised_level,
                     group_index=group.psl2z_index,
                     group_name = group_name,
                     symmetry = symmetry,
                     weight = weight,
                     character = character,
                     contributor = contributor,
                     metadata = metadata,
                     date = date,
                     M0 = M0,
                     Y = Y,
                     error = error)
    for a,b in cusps:
        f.group_cusps.append((int(a),int(b)))

    print "f=",f
    ## If we have Atkin-Lehner eigenvalues we add them
    cusp_evs = F.get('cusp_evs')
    if G.ncusps()>1:
        for i in range(G.ncusps()):
            a = int(G.cusps()[i].numerator())
            b = int(G.cusps()[i].denominator())
            if len(cusp_evs)>i:
                c =  cusp_evs[i]  ## We usually only have 1 or -1
                if c==1:
                    ev = RootOfUnity_DB(order=2,exponent=0)
                elif c==-1:
                    ev = RootOfUnity_DB(order=2,exponent=1)
                elif hasattr(c,'len') and len(c)>1:
                    RootOfUnity_DB(order=c[0],exponent=c[1])
                elif c==0 or c==None: # Means it is not set
                    continue
                else:
                    raise ValueError,"Can not construct eigenvalue from {0}".format(c)
            else:
                ev = None
            f.add_atkin_lehner({(a,b):ev})
    if commit == 0:
        return f
    try:
        mwfdb_session.commit()
        if verbose>0:
            print "Commited!"
    except Exception as e: # InvalidRequestError:
        print "e=2",e
        if verbose>0:
            print "Rolling back!"
        mwfdb_session.rollback()
    finally:
        if verbose>0:
            print "Closing!"
        mwfdb_session.close()
    return f

def add_coefficients(DB,F,check=True):
    ## coefficients can either be in the dictionary or in a separate file
    F = lower_case_dict(F)
    numc = F.get('numc',0)
    Coefficients = F.get('coefficients',[])
    #print "numc=",numc
    C = []
    C = DB.get_coefficients({'_id':F.get('_id',0)})
    if len(C)>1:
        print "Not unique coefficients!"
        return
    elif len(C)==0:
        print "No coefficients!"
        return
    else:
        C = C[0]
    #return C
    if C==[]:
        print "No coeficients to add!"
        return 0 #C = Coefficients
    f = find_MaassformDB(F).all()
    if len(f)==0:
        print "No function to add to!"
        return
    level = F.get('level',None)
    if level == None:
        return
    G = MySubgroup(Gamma0(level))
    add_coefficients_from_dict(f[0],G.cusps(),C,check)

def get_Cusp_Maassform(a,b):
    res = Cusp_Maassform.query.filter(Cusp_Maassform.a==int(a),Cusp_Maassform.b==int(b)).all()
    if len(res)==0:
        res = Cusp_Maassform(a=a,b=b)
    return res[0]

def add_coefficients_from_dict(f,cusps,C,check=True):
    r"""
    Add coefficients from the dictionary C to the MaassForm_DB F
    """
    dim = max(1,f.dimension)
    for r in range(dim):
        for i in range(len(cusps)):
            ca = cusps[i].numerator()
            cb = cusps[i].denominator()
            cusp = get_Cusp_Maassform(ca,cb)
            #Cusp_Maassform.query.filter(Cusp_Maassform.a==int(ca),Cusp_Maassform.b==int(cb))
            for n in C[r][i].keys():
                c = C[r][i][n]
                #print "c=",c
                #if isinstance(c,complex):
                #    cr = c.real; ci=c.imag
                #else:
                #    cr = c.real(); ci=c.imag()
                #f.add_coefficients(ca,cb,n,cr,ci)
                f.add_one_coefficient(r,cusp,n,c,check)



def get_M_and_Y_new(S,R,M0,eps):
    r""" Computes the  truncation point M>=M0 for Maass waveform with parameter R.

    INPUT:

        - ``R`` -- spectral parameter, real
        - ``Y`` -- height, real > 0
        - ``eps`` -- desired error


    OUTPUT:

        - ``M`` -- point for truncation

    EXAMPLES::ubuntuusers.de


        sage: get_M_for_maass(9.533,0.5,1E-16)
        12
        sage: get_M_for_maass(9.533,0.5,1E-25)
        18


    """
    ## initial value of Y
    Y = (log(2.0)-log(eps)-0.5*log(RR(M0))+RR.pi()*R*0.5)/(2*RR.pi()*RR(M0))
    if Y<0 or Y>S.group().minimal_height():
        raise ArithmeticError,"Could not get a good point"
    ## Now change M
    minm=ceil((12.0*R**0.3333+R)/2.0/RR.pi()/Y)
    print "minm=",minm
    print "Y=",Y
    ## Use low precision
    try:
        for m in range(minm+1,10000,3):
            erest=maass.maass_forms_alg.err_est_Maasswf(Y,m,R,1)
            print "erest=",erest
            Y1 = (log(2.0)-log(eps)-0.5*log(RR(m))+RR.pi()*R*0.5)/(2*RR.pi()*RR(m))
            print "Y1=",Y1

            if erest<eps:
                raise StopIteration()
    except StopIteration:
        return m,Y
    raise Exception," No good value for truncation was found!"


def Maass_waveforms_from_mongoDB(s,with_id=True,id_not=[],verbose=0):
    from sage.all import is_prime
    from compmf import character_conversions
    res = []
    q = DB.get_Maass_forms(s)
    for r in q:
        if r['_id'] in id_not:
            continue
        G = MySubgroup(Gamma0(r['Level']))
        if r.get('Conrey',0)==1:
            x = character_conversions.dirichlet_character_sage_from_conrey_character_number(r['Level'],r['Character'])
        else:
            x = character_conversions.sage_character_from_number(r['Level'],r['Character'])
        M = MaassWaveForms(G,weight = r['Weight'],sym_type = r['Symmetry'],ch=x,verbose=1)
        d = r.get('Dimension',1)
        numc = r.get('Numc',0)
        if r.get('coeff_id') is None:  ## We probably have other contributor than me....
            N = r['Level']
            if not is_prime(N):
                print "we don't have coefficients of corect format and not prime level for r={0}!".format(r['Eigenvalue'])
                continue
            if r.get('Fricke') is None:
                continue
            c = r.get('Coefficient')
            #return 
            if not c:
                c = r.get('Coefficients')
            if not c:
                continue
            coeffs = {0:{0:{0:{},1:{}}}}
            v = r['Fricke']
            for  i in range(len(c)):
                coeffs[0][0][0][i+1] =c[i]
                coeffs[0][0][1][i+1] =v*c[i]
        else:
            coeffs = DB.get_coefficients({'_id':r['_id']})
        
        #    print "coeffs=",coeffs[0][0][1].keys()
#        return 
        if not coeffs:
            print "No coefficient for M={0} and r={1} with id={2}".format(M,r['Eigenvalue'],r['_id'])
            continue
        #if isinstance(coeffs,list):
        ##    if len(coeffs)==1:
        #        coeffs = {0:coeffs[0]}
        
        for i in range(d):
            try:
                F = Maasswaveform(M,r['Eigenvalue'],compute = 0,dim=d,coefficients=coeffs[i])
            except ValueError:
                print "Failed for id={0} and coeffs = {1},{2}".format(r['_id'],coeffs.keys(),coeffs[0].keys())
            if with_id:
                res.append((F,r['_id']))
            else:
                res.append(F)
            if verbose>0:
                print "constructed Maassform of level {0} and eigenvalue {1}".format(F.level(),F.eigenvalue())
            print "F=",F
#    if len(res)==1:
#        return res[0]
    return res

def generate_plots(s,xlim=(-0.5,0.5),ylim=(0,2),num_pts=150,dpi=300,ncpus=8):
    r"""

    """
    idlist = DB._mongo_db['maassform_plots'].find({"num_pts":{"$gt": int(num_pts-1)},"dpi":{"$gt": int(dpi-1)}}).distinct('maass_id')
    l = Maass_waveforms_from_mongoDB(s,id_not=idlist)
    args = []
    for F,fid in l:
        q = DB._mongo_db['maassform_plots'].find({'maass_id':fid,"num_pts":{"$gt": int(num_pts-1)},"dpi":{"$gt": int(dpi-1)}})
        if q.count() == 0:  ## If it is not in the database
            print "Making new plot for N:{0} R:{1}!".format(F.level(),F.eigenvalue())
            args.append((F,fid,xlim,ylim,num_pts,dpi))
    if len(args)>0:
        if ncpus>=32:
            return list(make_plot_for_F32(args))
        if ncpus>=8:
            return list(make_plot_for_F8(args)) 
        if ncpus>=4:
            return list(make_plot_for_F4(args))
        if ncpus>=2:
            return list(make_plot_for_F2(args))
        else:
            return list(make_plot_for_F1(args))
            
@parallel(ncpus=32)
def make_plot_for_F32(F,mid,xlim,ylim,num_pts,dpi):
    return make_plot_for_F1(F,mid,xlim,ylim,num_pts,dpi)
    
@parallel(ncpus=8)
def make_plot_for_F8(F,mid,xlim,ylim,num_pts,dpi):
    return make_plot_for_F1(F,mid,xlim,ylim,num_pts,dpi)
@parallel(ncpus=4)
def make_plot_for_F4(F,mid,xlim,ylim,num_pts,dpi):
    return make_plot_for_F1(F,mid,xlim,ylim,num_pts,dpi)
@parallel(ncpus=2)
def make_plot_for_F2(F,mid,xlim,ylim,num_pts,dpi):
    return make_plot_for_F1(F,mid,xlim,ylim,num_pts,dpi)    
@parallel(ncpus=1)    
def make_plot_for_F1(F,mid,xlim,ylim,num_pts,dpi):
    import pymongo
    import bson
    import tempfile
    g = F.plot(xlim,ylim,num_pts)
    print "plot done!"
    filename = 'plot-{0}-{1:0>12.4f}-{2:0>4}-{3:0>4}.png'.format(F.level(),F.eigenvalue(),dpi,num_pts)
    tmpfilename = "/tmp/{0}.png".format(mid)
    g.savefig(tmpfilename,dpi=dpi)
    data = open(tmpfilename).read()
    db = DB._mongo_db['maassform_plots']
    idp = db.insert({'level': int(F.level()),
                        'dpi':int(dpi),
                        'num_pts':int(num_pts),
                        'eigenvalue':float(F.eigenvalue()),
                        'maass_id':mid,
                        'plot': bson.binary.Binary(dumps(data))})
    if idp:
        print "inserted {0}".format(idp)
    os.unlink(tmpfilename)


def check_coefficients_for_one_form(maassid,db=DB):
    if isinstance(maassid,dict):
        maassid = maassid.get('_id')
    data=db.get_Maass_forms(maassid)
    if data == []:
        raise ValueError,"No Maassform found" 
    f = data[0]
    coeffs = DB.get_coefficients(f)
    if len(coeffs)==1:
        coeffs = coeffs[0]
        if len(coeffs)!= 1:
            raise StopIteration,""
        ## Now check that we have the correct number of components
        num_cusps = Gamma0(f.get('level',f.get('Level'))).ncusps()
        if num_cusps != len(coeffs):
            print "Number of components is not correct"
        print "Numc=",len(coeffs)
    if coeffs  == []:
        print "No coefficients found for this Maassform"
        fs = gridfs.GridFS(self._mongo_db,'Coefficients')
        coeffid = f['coeff_id']
        print "exists=",fs.exists(coeffid)
 #       DB._collection.update_one({'_id':f['_id'],'Numc':int(0),'coeff_id'
        

def dict_depth(d, depth=0):
    if not isinstance(d, dict) or not d:
        return depth
    return max(dict_depth(v, depth+1) for k, v in d.iteritems())


r"""

Some coefficients seem to be broken! We need to do some fix!


"""




def fix_coefficients(N1=1,N2=1,dryrun=False):
    r"""
    Go through the database and recompute coefficients.
    For some strange reason a lot of Maassforms does not have C_1(4) defined... 
    """
    import gridfs
    from psage.modform.maass.maass_forms_phase2 import phase2
    FL = DB._mongo_db['Coefficients.files'].find({'level':{"$lt":int(N2+1),"$gt":int(N1-1)}})
    fs = gridfs.GridFS(DB._mongo_db,'Coefficients')
    for r in FL:
        ## WE also check if we have an old form:
        # check the linked Maass form
        maass_id=r['maass_id']
        form = DB._mongo_db['FS'].find_one({'_id':maass_id})
        level = form['Level']; weight = form['Weight']; numc=form['Numc']
        eigenvalue=form['Eigenvalue']
        ch=r.get('Character',1)
        st = form.get('Symmetry',-1)
        print "form=",form
        ## Do we have an oldform? If so we remove it.
        R1 = eigenvalue - 1e-10
        R2 = eigenvalue + 1e-10

        try: 
            for M in divisors(level):
                if M == level:
                    continue
                s= {'Level':int(M),'Eigenvalue':{"$gt":R1,"$lt":R2}}
                print "s=",s
                newform = DB._mongo_db['FS'].find(s)
                if newform.count() == 1:
                    print "Eigenvalue {0} at level {1} is old!".format(eigenvalue,level)
                    print "Remove it!"
                    if dryrun == 0:
                        fs.delete(r['_id'])
                        DB._mongo_db['FS'].remove({'_id':r['_id']})
                    raise StopIteration()
        except StopIteration:
            continue
        
        print "r=",r
        coeffs = loads(fs.get(r['_id']).read())
        try: 
            c2 = coeffs[0][0].get(1,{}).get(2)
            c3 = coeffs[0][0].get(1,{}).get(3)
            c4 = coeffs[0][0].get(1,{}).get(4)
        except KeyError:
            c2 = None;c3=None; c4=None
            print "could not get coeffs from : ",coeffs
#            raise KeyError

        Rst =  str(eigenvalue).split(".")
        Rst = (Rst[0] + "." + Rst[1][0:12])[0:12]  
        fname = 'c-{0}-{1}-{2}-{3}-{4}'.format(level,weight, ch, st, Rst)
        if r['filename']<>fname or eigenvalue<>r['eigenvalue']:
            print "Updating filename from {0} to {1} since R={2}".format(r['filename'],fname,eigenvalue)
            DB._mongo_db['Coefficients.files'].update({'_id':r['_id']},{"$set":{'filename':fname,'eigenvalue':eigenvalue}},multi=False,upsert=True)
        
        if c2 is None or c3 is None or c4 is None:
            print "Need to recompute coefficients!"
            #fname = r['filename'];
            f = Maass_waveforms_from_mongoDB({'_id':r['maass_id']})[0][0]
            # Then recompute coefficients
            #F = f.space().get_element(f.eigenvalue(),Yset=f._Y,Mset=f._M0,dim=f._dim,do_par=True,ncpus=4)
            print "f=",f
            f.space()._verbose=0
            ## Remove old record
            if numc>r['numc']:
                C = phase2(f,numc,verbose=0)
            else:
                C = phase2(f,10)
            print "Got coefficients from phase2!"
            fid = r['_id']
            if dryrun == 0:
                newid = fs.put(dumps(C),filename=fname,maass_id=maass_id,level=level,
                               weight=weight,eigenvalue=eigenvalue,numc=numc)
                DB._mongo_db['FS'].update({'_id':r['maass_id']},{"$set":{'coeff_id':newid}},upsert=True,multi=False)       
                fs.delete(fid)
            else:
                print "Did not modify the database!"

def insert_one_maass_form(F):
    import datetime
    from psage.modform.maass.maass_forms import MaassWaveformElement_class
    if not isinstance(F,MaassWaveformElement_class):
        raise ValueError
    if not F.group().is_congruence():
        raise NotImplementedError
    if F.character().is_trivial():
        char = 1
    else:
        char = DirichletGroup_conrey(F.level()).from_sage_character(F.character()).number()
    rec = {
        "Level":int(F.level()),
        "Weight":float(F.weight()),
        "Character":int(char),
        "Symmetry":int(F.sym_type()),
        "Eigenvalue":float(F.eigenvalue()),
        "Contributor":"FS"
        }
    if F.atkin_lehner_eigenvalues() != {}:
        res["Cusp_evs"]=F.atkin_lehner_eigenvalues()
    ## Check if a matching record exists:
    s = rec
    r = float(F.eigenvalue())
    tol = 1e-6
    s.update({'Eigenvalue':{"$gte": r -float(tol),"$lt":r +float(tol)}})
    c = DB._collection.find(s).count()
    print "s=",s
    if c > 1:
        print "More than one matching records!"
        for r in DB._collection.find(rec):
            print r
    elif c==1:
        ## Updating...
        r = DB._collection.find_one(rec)
        print "Updating the record:{0}".format(r)
        if r['Error'] < F.test(format='float'):
            print "Old record is more accurate so we don't update the Eigenvalue"
        else:
            toset = {
                "Error":F.test(format='float'),
                "M0":int(F.M0()),
                "Y":float(F.Y()),
                "date":datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d %T' ),
                "Dim":int(F.dim())}
        ## Then check coefficients.
        ## First from daabase.
        c = DB.get_coefficients({'_id':r['_id']})
        if c is None or c == []:
            c = r.get('Coefficients',[])
        if c is None or c == []:
            print "No coefficients in DB!"
            insert_coeffs = True
        else:
            ## Check if we provide more coefficients than are there at the moment.
            d = dict_depth(c)
            if d == 3:
                n = len(c[0][0][0])
                print "n um coeffs=",n
            else:
                raise ValueError,"Coefficients should be re-inserted! depth={0}".format(d)
    elif c == 0:
        insert_form_as_new(F)


def insert_form_as_new(F,db=DB):
    pass

def check_if_record_has_coeffs(maassid,db=DB):
    f = db.get_Maass_forms(maassid)
    if len(f) > 1:
        raise ValueError,"Multiple records found!"
    if f.get('Contributor') == 'FS':
        c = db.get_coefficients(f)
        c = c[0]
        if dict_depth(c) != 3:
            print "Wrong format coefficients"
            return
        print "numc=",len(c[0][0][0])
    elif f.get('Contributor') == 'HT':
        c = r['Coefficients']
        print "numc=",len(c)
