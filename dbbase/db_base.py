## -*- coding: utf-8 -*-
#*****************************************************************************
#  Copyright (C) 2013 Fredrik Strömberg <stroemberg@mathematik.tu-darmstadt.de>,
#
#  Distributed under the terms of the GNU General Public License (GPL)
#
#    This code is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#  The full text of the GPL is available at:
#
#                  http://www.gnu.org/licenses/
#*****************************************************************************
r"""
Implements a base class for (json)-file-based databases.

"""

import os
import json
from copy import copy,deepcopy
from string import join
import collections
from dicts import TransformedDictJson,DictReqKeys,DictLowerJson
from files import Files_class

class Metadata_data_sets(DictReqKeys,DictLowerJson):
    r"""
    A base class for metadata objects of data sets in databases.

    """
    _required_keys = ['date','creator','license'] # Minimum set of information which is needed.


class MyDB_class(object):
    r"""
    Base class for abstract databases with json storage.
    Different implementations will subclass from this class as well as an class
    specifying the database implementation.
    """
    _data_fields = []      # A list of possible data field
    _data_fields_maps = [] # Maps to apply to te above fields
    _required_fields = []  # List of fields which are required.
    _metadata_class = Metadata_data_sets
    _dict_class = DictLowerJson
    # String which contains information about the database. This is treated differently in different
    # Database types.
    _db_description = "This directory contains a database"
    # List of accepted keywords used for formatting etc. (names of data fields are always allowed)
    _optional_keywords = ['overwrite','update','check','verbose']

    # In the case of a tuple, at least one of the fields has to be supplied
    def __init__(self,data_fields = [], data_fields_maps = [], required_fields = [],**kwds):
        r"""
        Initialize self.
        """
        if data_fields <> []:
            self._data_fields = data_fields
        if data_fields_maps <> []:
            self._data_fields_maps = data_fields_maps
        if required_fields <> []:
            self._required_fields = required_fields
        self._verbose = kwds.get('verbose',0)
        self._data_fields_name = [ t[0] for t in self._data_fields]
        self._req_fields_name = [ t[0] for t in self._required_fields]
        self._check_data_fields_maps()
        self._all_keys = deepcopy(self._optional_keywords)
        self._all_keys.extend(self._metadata_class._required_keys)
        for l in self._data_fields:
            for t in l:
                for k in t:
                    self._all_keys.append(k)

    def _check_data_fields_maps(self):
        if len(self._data_fields)<>len(self._data_fields_maps):
            print "data_fields=",self._data_fields
            print "data_fields_maps=",self._data_fields_maps
            raise ValueError,"Need to supply the same number of maps as types of elements!"

    def __repr__(self):
        r"""

        String representation of self.


        EXAMPLES::


        sage: DB=MyDB_class()
        sage: DB
        Dummy Maass waveforms database. Please use one of the derived classes!

        """
        return "Dummy Maass waveforms database. Please use one of the derived classes!"

    def required_fields(self):
        r"""
        Display the fields which needs to be supplied when creating a new record.

        EXAMPLES::


        sage: DB=MyDB_class(data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        sage: DB.required_fields()
        ['author']


        """
        return self._required_fields

    def _check_required_fields(self,record,**kwds):
        r"""
        Check that a data record has the required keys.

        EXAMPLES:


        sage: DB=MyDB_class(data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        sage: DB._check_required_fields({'data':1})
        Traceback (most recent call last)
        ...
        ValueError: One of the fields: author is required!
        sage: DB.check_completeness_of_record({'data':1,'author':'Fredrik'})
        True


        """
        for fld in self._required_fields:
            if not isinstance(fld,(tuple,list)):
                fld = fld
            if [ key in record for key in fld].count(True) == 0 and [key in kwds for key in fld].count(True) == 0:
                raise ValueError,"One of the fields: {0} is required! \n Got:{1}".format(fld,record)
        return True

    def check_record(self,record,**kwds):
        r"""
        Check that record has necessary keys and that sufficient metadata is supplied.
        """
        if not self._check_required_fields(record):
            return False
        metadata = self.extract_metadata(record,**kwds)
        return True

    def check_metadata(self,record,**kwds):
        r"""
        Check that necessary metadata is supplied.
        """


    def format_record(self,record={},**kwds):
        r"""
        Format a record using a customized formatting function.

        EXAMPLES::

        sage: D=MaassDB_Class()
        sage: d={u'Character': 1, u'Contributor': u'HT', u'Cusp_evs': [1], u'Dim': 1, u'Dimension': 1,
        u'Eigenvalue': 9.53369526135355, u'Error': 2.0894397323445446e-13,
        u'Level': 1, u'M0': 0, u'Norm': 1.0, u'Symmetry': 1, u'Weight': 0.0, u'Y': 0}
        sage: MF.format_record(d)
        {'eigenvalue': 9.53369526135355, 'license': u'CC BY-SA', 'group': ('gamma0', 1, [(1, 0)]), 'weight': 0.0, 'symmetry': 1, 'creator': u'Fredrik Stromberg', '_is_formatted': 1, 'dim': 1, 'error': 2e-13, 'date': u'2012', 'algorithm': u'Psage', 'metadata': {'license': u'CC BY-SA', 'creator': u'Fredrik Stromberg', 'meta-meta': u'', 'date': u'2012', 'algorithm': u'Psage'}}

        """
        return self.transform_record(record,**kwds)

    def transform_record(self,record,**kwds):
        r"""
        Apply the corresponding maps to the fields and transform keys to strings and pick the first representative of the list of names for the field.

        Note: We do not want to transform metadata in this routine.

        EXAMPLES::

        sage: DB=MyDB_class(data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        sage: rec={'data':1,'author':'Fredrik'}
        sage: type(rec['data'])
        <type 'sage.rings.integer.Integer'>
        sage: rec1=DB.transform_record(rec)
        sage: rec1
        {'data': 1, 'author': 'Fredrik'}
        sage: type(rec1['data'])
        <type 'int'>


        """
        res = self.to_dict(record)
        for i in range(len(self._data_fields)):
            f = self._data_fields_maps[i]
            for fld_names in self._data_fields[i]:
                for name in fld_names:
                    if name in res:
                        res[fld_names[0]]=f(res[name])
                        if name <> fld_names[0] and name.find('meta')<0:
                            res.pop(name)
                        break
        return res

    def find(self,search_data={},**kwds):
        r"""
        Find records in the database.

        EXAMPLES::

        sage: DB=MyDB_class(data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        sage: DB.find({})
        Traceback (most recent call last)
        ...
        NotImplementedError: Needs to be implemented in subclasses

        """
        raise NotImplementedError,"Needs to be implemented in subclasses"

    def metadata(self,*args,**kwds):
        r"""
        Construct an element of the metadata class of self.

        EXAMPLES::


        sage: DB=MyDB_class(data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        sage: DB.metadata({'creator':'Fredrik Stromberg','license':'CC','date':'2012'})
        {'date': '2012', 'license': 'CC', 'creator': 'Fredrik Stromberg'}

        """
        return self._metadata_class(*args,**kwds)

    def extract_metadata(self,data={},*args,**kwds):
        r"""
        Extract metadata from record. We pull the fields which are required in self._metadata_class
        as well as fields named 'metadata', 'meta' and 'meta-x' for any string 'x'.
        If the required fields are not supplied we try to extract them from the string 'metadata'

        INPUT:

        - metadata specified by keywords and/or a dictionary. If given in text-format fields should be of the form 'key:value' and separated by ','. The keywords can not contain ':' or ','.


        KEYWORDS:

        - 'purge' -- int. set to 1 if you want to purge the original record of all metadata.

        EXAMPLES::

        sage: MF=MyDB_class()
        sage: s='Date:2012, Creator:Fredrik Stromberg, License:CC BY-SA,Copyright:FS,Algorithm:Psage, Sage-version:5.11'
        sage: MF.extract_metadata(metadata=s)
        {'license': 'CC BY-SA', 'copyright': 'FS', 'algorithm': 'Psage', 'sage-version': '5.11', 'creator': 'Fredrik Stromberg', 'date': '2012'}
        sage: s='Date:2012, Creator:Fredrik Stromberg'
        sage: MF.extract_metadata(metadata=s)
        Traceback (most recent call last)
        ...
        KeyError: Metadata for this class needs the following keys:['date', 'creator', 'license', 'algorithm']. Missing key(s):license
        sage: MF.extract_metadata(metadata=s,license='CC',algorithm='Psage')
        {'date': '2012', 'algorithm': 'Psage', 'license': 'CC', 'creator': 'Fredrik Stromberg'}


        sage:
        """
        check = data.get('check',kwds.get('check',1))==1
        meta_keys = deepcopy(self._metadata_class._required_keys)
        verbose = kwds.get('verbose',0)
        if isinstance(data,self._dict_class):
            data_d = data
        else:
            data_d = self.to_dict(data)
        if 'metadata' in data and isinstance(data_d['metadata'],dict):
            meta = data_d.pop('metadata')
        elif 'metadata' in kwds and isinstance(kwds['metadata'],dict):
            meta = kwds.pop('metadata')
        else:
            meta = data_d
        try:
            meta = self.metadata(meta,**kwds)
        except KeyError:
            metatext = join([meta.pop('metadata',''),kwds.pop('metadata',''),
                             meta.pop('meta',''),kwds.pop('meta','')])
            s = metatext.lower()
            s0=copy(metatext)
            # We want to keep a copy of the original metadata text woth the above fields removed
            # In case a creator supplies his own metadata.
            #for key in self._metadata_class._required_keys:
            #    i = s.find(key); l=len(key)+1
            #    if i<0:
            #        continue
            #    for j in range(i+l,len(s)): # find the first non-alphanumeric character
            #        x = s[j]
            #        if s[j] in [",",";"]:
            #            break
            #    data[key]=metatext[i+l:j].strip()
            #    s0 = s0.replace(metatext[i:j+1],"") # Removing, e.g. "Creator: Fredrik,"
            #print "s0=",s0
            ## Dealing with escaped , and : characters
            #s0 = s0.replace("\,","[comma]").replace("\\,","[comma]")
            #s0 = s0.replace("\:","[colon]").replace("\\:","[colon]")
            l0 = s0.split(",")
            for keyval in l0:
                keyval = keyval.split(":",1)
                if verbose>0:
                    print "keyval=",keyval
                if len(keyval)<>2:
                    raise ValueError,"Metadata string not correctly formatted! Remember to escape ':' in the text! Got: {0}".format(s0)
                key,val = keyval
                key = key.strip(); val=val.strip()
                data_d[key]=val.replace("[comma]",",").replace("[colon]",":")
                meta_keys.append(data_d.__keytransform__(key))
            try:
                meta = self.metadata(data_d,**kwds)
            except KeyError as e:
                missing = e.message.split(":")[-1].strip()
                s = "Metadata for this class needs the following keys: {0}".format(missing)
                if check:
                    raise KeyError,s
                else:
                    meta = deepcopy(data_d)
            if verbose>0:
                print "meta=",meta
            #s0 = s0.strip()
            #if len(s0)>0: # If there is any metadata left we add it in meta-meta
            #    meta['meta-meta']=s0
            # Remove keys not associated with metadata
        if verbose>0:
            print "meta_keys=",meta_keys
        for key in meta.keys():
            #if key not in meta._required_keys and not key.find('meta')>=0:
            if key not in meta_keys and not key.find('meta')>=0:
                meta.pop(key)
        if verbose>0:
            print "meta1=",meta
        # purge the metadata from the original record
        if kwds.get('purge',0)==1:
            for key in meta.keys():
                data_d.pop(key,None)
                if isinstance(data,self._dict_class):
                    data = data_d.store
        return meta


    def to_dict(self,*args,**kwds):
        r"""
        Construct an element of the dict class of self.

        EXAMPLES::

        sage: MF = MaassDB_class()
        sage: d={'Eigenvalue':9.53369526135355,'Dim':1,'Error':2e-13,'Level':1,'Symmetry':1,'Weight':0,'metadata':"Creator: Fredrik Stromberg, License: CC BY-SA, Algorithm: Psage, Date:2012"}
        sage: MF.to_dict(d)
        {'eigenvalue': 9.53369526135355, 'weight': 0, 'symmetry': 1, 'level': 1, 'dim': 1, 'error': 2.00000000000000e-13, 'metadata': 'Contributor: Fredrik Stromberg, License: CC BY-SA, Algorithm: Psage, Date:2012'}


        """
        return self._dict_class(*args,**kwds)

    def show_table_info(self,**kwds):
        r"""
        Display information about the data contained in the database.

        EXAMPLES::

        sage: DB=MyDB_class(data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        sage: DB.show_table_info()
        Traceback (most recent call last)
        ...
        NotImplementedError: Needs to be implemented in subclasses

        """
        raise NotImplementedError,"Needs to be implemented in subclasses"

    def insert_record(self,record={},**kwds):
        r"""
        Insert a record into the database.

        EXAMPLES::

        sage: DB=MyDB_class(data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        sage: sage: DB.insert_record({})
        Traceback (most recent call last)
        ...
        NotImplementedError: Needs to be implemented in subclasses
        sage: DB=FilesDB_class('/tmp/test/',data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        s

        """
        check=record.get('check',kwds.get('check',0))
        record  = self.format_record(record,**kwds)
        if check<>0:
            self.check_record(record,**kwds)
            self.extract_metadata(record,**kwds)
        if not hasattr(self,'insert_record_into_db'):
            raise NotImplementedError,"Instances need to inherit from class providing the method insert_into_db"
        if kwds.get('verbose',0)>0:
            print "Inserting into database!"
        return self.insert_record_into_db(record,**kwds)

    def read_record(self,record={},**kwds):
        r"""
        Read a record from the database.

        EXAMPLES::


        sage: DB=MyDB_class(data_fields=[[('author',)],[('data',)]],data_fields_maps=[str,int],required_fields=['author'])
        sage: sage: DB.insert_record({})
        Traceback (most recent call last)
        ...
        NotImplementedError: Needs to be implemented in subclasses

        """
        raise NotImplementedError,"Needs to be implemented in subclasses."


    def update_record(self,record={},**kwds):
        r"""
        Update a record in the database.

        """
        try:
            record = self.format_record(record,**kwds)
        except ValueError:
            raise ValueError,"The record does not describe a unique element!"
        record1 = self.read_record(record)
        for key in record: # Updating json record
            record1[key]=record[key]
        self.insert_record_into_db(record1,overwrite=True,**kwds)



class MyDBJson_class(MyDB_class):
    r"""
    Base class for databases storing data as json serialized strings.
    """

    def record_to_json(self,input={},**kwds):
        r"""
        Construct a json string representing a Maass form.

        EXAMPLES::


        sage: D=MaassDB_Class()
        sage: d={u'Character': 1, u'Contributor': u'HT', u'Cusp_evs': [1], u'Dim': 1, u'Dimension': 1,
        u'Eigenvalue': 9.53369526135355, u'Error': 2.0894397323445446e-13,
        u'Level': 1, u'M0': 0, u'Norm': 1.0, u'Symmetry': 1, u'Weight': 0.0, u'Y': 0}
        sage: MF.format_record(d)
        {'eigenvalue': 9.53369526135355, 'license': u'CC BY-SA', 'group': ('gamma0', 1, [(1, 0)]), 'weight': 0.0, 'symmetry': 1, 'creator': u'Fredrik Stromberg', '_is_formatted': 1, 'dim': 1, 'error': 2e-13, 'date': u'2012', 'algorithm': u'Psage', 'metadata': {'license': u'CC BY-SA', 'creator': u'Fredrik Stromberg', 'meta-meta': u'', 'date': u'2012', 'algorithm': u'Psage'}}
        """
        return self.format_record(input,**kwds).json()



class FilesDB_class(MyDBJson_class,Files_class):
    r"""
    Base class for working with a database consisting of files with json serialised objects.
    """

    def __init__(self,dir,host='',username='',**kwds):
        r"""
        Init self.
        """
        MyDBJson_class.__init__(self,**kwds)
        Files_class.__init__(self,dir,host,username,**kwds)


    def filename_of_record(self,record,**kwds):
        r"""
        Return the filename corresponding to the record.

        EXMPLES::


        sage: DB=FilesDB_class('/tmp/test/',data_fields=[[('level',)],[('weight',)],[('data',)]],data_fields_maps=[int,float,str],required_fields=['level','weight'])
        sage: DB.insert_record({'level':int(1),'weight':float(0.5),'data':'Dedekind eta function'})
        sage: DB.insert_record_int_db(rec)
        sage:

        """
        filename = record.pop('filename',kwds.pop('filename',None))
        if filename == None:
            raise NotImplementedError,"Either supply a filename or use a subclass which implements a suitable 'filename_of_record' function!"
        return filename

    def read_record(self,record={},**kwds):
        r"""
        Read a record from the database.

        EXAMPLES::


        sage: DB=FilesDB_class('/tmp/test/',data_fields=[[('level',)],[('weight',)],[('data',)]],data_fields_maps=[int,float,str],required_fields=['level','weight'])
        sage: rec={'level':int(1),'weight':float(0.5),'data':'Dedekind eta function'}
        sage: DB.insert_record_int_db(rec)
        sage: DB.read_record(rec,filename="eta.json")
        {u'data': u'Dedekind eta function', u'level': 1, u'weight': 0.5}



        """
        filename = self.filename_of_record(record,**kwds)
        return json.loads(self.read_file(filename))

    def record_to_json(self,record):
        r"""
        Attempt to JSON-serialize the record.

        EXAMPLES::

        sage: DB = FilesDB_class('/tmp/test/',data_fields=[[('level',)],[('weight',)],[('data',)]],data_fields_maps=[into,float,str],required_fields=['level','weight'])
        sage: rec =  {'data': 'Dedekind eta function', 'weight': 0.5, 'level': 1}
        sage: DB.record_to_json(rec)
        Traceback (most recent call last)
        ...
        TypeError: Record must be Json serializable! {'data': 'Dedekind eta function', 'weight': 0.5, 'level': 1} is not JSON serializable!
        sage: d=DB.format_record(rec); type(d)
        <class 'dicts.TransformedDictJson'>
        sage: DB.record_to_json(d)
        '{"data": "Dedekind eta function", "weight": 0.5, "level": 1}'

        """
        if hasattr(record,'json'):
            return record.json()
        try:
            return json.dumps(record)
        except TypeError as e:
            raise TypeError,"Record must be Json serializable! {0}!".format(e.message)

    def insert_record_into_db(self,record={},**kwds):
        r"""
        Insert a record into the database.


        EXAMPLES::


        sage: DB=FilesDB_class('/tmp/test/',data_fields=[[('level',)],[('weight',)],[('data',)]],data_fields_maps=[int,float,str],required_fields=['level','weight'])
        sage: rec={'level':1,'weight':0.5,'data':'Dedekind eta function'}
        sage: DB.insert_record_int_db(rec)
        Traceback (most recent call last)
        ...
        NotImplementedError: Either supply a filename or use a subclass which implements a suitable 'filename_of_record' function!
        sage: DB.insert_record(rec,filename='eta.json')
        sage: ! cat /tmp/test/eta.json && echo "\n"
        {"data": "Dedekind eta function", "weight": 0.5, "level": 1}

        """
        self.format_record(record,**kwds)
        filename = self.filename_of_record(record,**kwds)
        overwrite = record.get('overwrite',kwds.get('overwrite'))
        s = self.record_to_json(record)
        self.create_and_write_to_file(s,filename,overwrite)

