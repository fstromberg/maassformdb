## -*- coding: utf-8 -*-
#*****************************************************************************
#  Copyright (C) 2013 Fredrik Strömberg <stroemberg@mathematik.tu-darmstadt.de>,
#
#  Distributed under the terms of the GNU General Public License (GPL)
#
#    This code is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#  The full text of the GPL is available at:
#
#                  http://www.gnu.org/licenses/
#*****************************************************************************
r"""

Implements base classes for dictionaries with special properties making them suitable for storage in json-based databases.


"""


# The databases here are based on json serialisation of dictionaries.
# We therefore need some specialised dictionary classes.

import collections
import json
from string import join
from sage.arith.all import gcd



# Note defaultdict has the advantage of being json serializable
# unfortunately the update function doesn't work properly....
class TransformedDict(collections.MutableMapping): #,defaultdict):
    """A dictionary which applies an arbitrary key-altering function before accessing the keys.
    Taken from: http://stackoverflow.com/questions/3387691/python-how-to-perfectly-override-a-dict
    """
    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))

    def __repr__(self):
        return self.store.__repr__()
        
    def __getitem__(self, key):
        return self.store[self.__keytransform__(key)]

    def __setitem__(self, key, value):
        self.store[self.__keytransform__(key)] = self.__valuetransform__(value)

    def __delitem__(self, key):
        del self.store[self.__keytransform__(key)]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __keytransform__(self, key):
        return key

    def __valuetransform__(self, val):
        if isinstance(val,(dict,TransformedDict)):
            #print "Got a dict:",val
            return self.__class__(val)
        elif isinstance(val,list):
            #print "Got a list:",val
            return [self.__valuetransform__(x) for x in val]
        elif isinstance(val,tuple):
            #print "Got a tuple:",val
            #print "types=",[type(x) for x in val]
            #print "transformed=",tuple([self.__valuetransform__(x) for x in val])
            return tuple([self.__valuetransform__(x) for x in val])
        else:    
            #print "Got a value:",val
            return self._valuetransform(val)

    def _valuetransform(self, val):
        return val 
   
class TransformedDictJson(TransformedDict):
    r"""
    Implements a dict where the values are Jsonizable.
    """

    from sage.rings.integer import is_Integer
    from sage.rings.real_mpfr import is_RealNumber
    
    def _valuetransform(self, value):

        if self.is_Integer(value):
            return int(value)
        if self.is_RealNumber(value):
            try:
                prec = value.prec()
            except AttributeError:
                prec = 53
            if prec==53:    
                return float(value)
            else:
                return str(value)            
        else:
            try:
                s=json.dumps(value)
                res = value
            except TypeError:
                res = str(value)
            return res
        
    def json(self):
        r""" Return a json dump of self.

        EXAMPLES::


            sage: t=DictLowerJson({'data':[{1:2,'d':{3:4}} ]})
            sage: s=t.json(); s
            '{"data": [{"1": 2, "d": {"3": 4}}]}'
            sage: json.loads(s)==t.store
            True

        Also works recursively

            sage: t1=DictLowerJson({'data':t})
            sage: s=t1.json(); s
            '{"data": "{\'data\': [{\'1\': 2, \'d\': {\'3\': 4}}]}"}'
            sage: t1.store==json.loads(s)
            True

            """
        return json.dumps(self,default = lambda x:x.store)

class TransformedDictYaml(TransformedDict):
    r"""
    Implements a dict where the keys are lower-case strings (to make sure that it can be json-ized).
    """
    
    from sage.rings.integer import is_Integer
    from sage.rings.real_mpfr import is_RealNumber
    from sage.rings.rational import is_Rational
    def _valuetransform(self, value):
        if self.is_Integer(value):
            return int(value)
        if self.is_RealNumber(value):
            try:
                prec = value.prec()
            except AttributeError:
                prec = 53
            if prec==53:    
                return float(value)
            else:
                return str(value)
        elif self.is_Rational(value):
            return RationalYAML(value.numerator(),value.denominator())
        else:
            try:
                s=yaml.safe_dump(value)
                res = value
            except TypeError:
                res = str(value)
            return res

    def __keytransform__(self,key):
        return self._valuetransform(key)
        
    def yaml(self):
        r""" Return a yaml dump of self.

        EXAMPLES::


            sage: t=DictLowerJson({'data':[{1:2,'d':{3:4}} ]})
            sage: s=t.json(); s
            '{"data": [{"1": 2, "d": {"3": 4}}]}'
            sage: json.loads(s)==t.store
            True

        Also works recursively

            sage: t1=DictLowerJson({'data':t})
            sage: s=t1.json(); s
            '{"data": "{\'data\': [{\'1\': 2, \'d\': {\'3\': 4}}]}"}'
            sage: t1.store==json.loads(s)
            True

            """
        return yaml.safe_dump(self.store) #,Dumper = lambda x:x.store)
        
        
    
class DictLower(TransformedDict):
    r"""
    Implements a dict where the keys are lower-case strings (to make sure that it can be json-ized).
    """       
    def __keytransform__(self, key):
        if not isinstance(key,basestring):
            key = str(key)
        return key.lower()

class DictLowerJson(DictLower,TransformedDictJson):
    r"""
    Transforms keys to lower case strings and values to int and float if possible.
    """
    
class DictReqKeys(collections.MutableMapping):
    r"""
    A base class for dictionaries with required keys
    """
    _required_keys = []
    
    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs)) # set keys
        self.check_keys() # check keys

    def __repr__(self):
        return self.store.__repr__()

    def required_keys(self):
        return self._required_keys
            
    def check_keys(self):
        r"""
        Check that the required keys are supplied.
        """
        s = []
        for key in self.required_keys():
            if key not in self:
                s.append("{0}".format(key))
        if s<>[]:
            raise KeyError,"This dictionary needs to have the following required fields:{0}. Missing field(s): {1}".format(self._required_keys,join(s,", "))
