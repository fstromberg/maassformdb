## -*- coding: utf-8 -*-
#*****************************************************************************
#  Copyright (C) 2013 Fredrik Strömberg <stroemberg@mathematik.tu-darmstadt.de>,
#
#  Distributed under the terms of the GNU General Public License (GPL)
#
#    This code is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#  The full text of the GPL is available at:
#
#                  http://www.gnu.org/licenses/
#*****************************************************************************
r"""
Implements a base class for (json)-file-based databases.

""" 

import os
import json
from string import join
import collections 
from dicts import DictReqKeys,DictLowerJson
try:
    import paramiko
except:
    # Make a Dummy class providing the methods we use...
    class paramiko(object):
        def __init__(self):
            raise NotImplementedError,"Please install the paramiko module if you want to work with remote file systems!"
        def SSHClient(self):
            pass

def Files(dir,host='',username='',**kwds):
    return Files_class(dir,host,username,**kwds)

class Files_class(object):
    r"""
    Generic class for working with file and directory-based data structures.
    """
    def __init__(self, dir,host='',username='',**kwds):
        r"""
        If host is left empty we work with local files, otherwise via SFTP

        EXAMPLES::

          sage: F=FilesDB("/tmp/test/")
          Traceback (most recent call last)
          ...
          RuntimeError: Please create the data directory /tmp/test/
          sage: mkdir /tmp/test/
          sage: F=FilesDB("/tmp/test/")
          sage: F
          Filebased database structure located at: /tmp/test/

        """
        self._host = host
        self._sftp = None
        self._ssh = None
        self._directory = None
        self._verbose = kwds.get('verbose',0)
        self._dry_run = kwds.get('dry_run',False)
        if self._host <> '':
            self._ssh = paramiko.SSHClient() 
            self._ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
            self._ssh.connect(self._host, username=username, password='')
            self._sftp = self._ssh.open_sftp()
            if not self._sftp or not rexists(self._sftp,dir):
                raise RuntimeError, "Please create the data directory {0}/{1}".format(self._host,dir)
        else:
            if not os.path.exists(dir):
                raise RuntimeError, "Please create the data directory {0}".format(dir)
        self._directory = dir
        if self._host=='':
            self._known_db_file = os.path.join(self._directory, 'known.sqlite3')
        else:
            self._known_db_file = "{0}:{1}/known.sqlite3".format(host,dir)

    def __repr__(self):
         r"""
         String representation of self.
         """
         s = "Filebased database structure located at: "
         if self._host<>'':
             s+="{0}:".format(self._host)
         s+="{0}".format(self._directory)
         return s


    def check_path(self,path):
        r"""
        Check that we are not doing anything outside the directory structure.

        EXAMPLES::


          sage: F.check_path("..")
          Traceback (most recent call last)
          ... 
          ValueError: Paths outside the database directory /tmp/test/ are not allowed!
          sage: F.check_path("/home/")
          Traceback (most recent call last)
          ... 
          ValueError: Paths outside the database directory /tmp/test/ are not allowed!
          sage: F.check_path("/tmp/test/")
          True
          
        """
        i = len(self._directory)
        if not isinstance(path,str):
            raise ValueError, "{0} is not a valid path!".format(path)
        if len(path) == 0:
            return True
        if ".." in path or (path[0] == "/" and path[0:i] <> self._directory):
            # Check that the user doesn't specify an outside path
            raise ValueError,"Paths outside the database directory {0} are not allowed!".format(self._directory)
        return True
            
        
    def make_path_name(self,f="",*args):
        r"""
        Make an absolute path name
        
        EXAMPLES::


            sage: F=FilesDB("/tmp/test/")
            sage: F.make_path_name('Gamma0','001-010')
            '/tmp/test/Gamma0/001-010'
            sage: F.make_path_name('/tmp/test/Gamma0','001-010')
            '/tmp/test/Gamma0/001-010'
            sage: F.make_path_name(['gamma0', '000-009', '001', '001', '9.533695261354_01_??'],'x')
            '/tmp/test/gamma0/000-009/001/001/9.533695261354_01_??/x

        """
        if isinstance(f,tuple):
            f = list(f)
        elif not isinstance(f,list):
            f = [f]
        args = list(args)
        f.extend(args) #.extend(f[1:])
        #self.check_path(f)
        s = f[0]
        for fs in f[1:]:
            s+= "/{0}".format(fs)
        if self._sftp==None:
            s = os.path.join(self._directory,s) # , self.space_name(N,k,i))
        else:
            s = "{0}/{1}".format(self._directory,s)
        self.check_path(s)
        return s

    def makedirs(self,dir,*args):
        r"""
        Make the directory dir as a subdirectory to self._directory unless we have an absolute path.

        EXAMPLES::

        
            sage: F=FilesDB("/tmp/test/")
            sage: F.makedirs('Gamma0','001-010')
            sage: !ls /tmp/test/
            Gamma0
            sage: !ls /tmp/test/Gamma0/
            001-010
        """
        if self._dry_run: # Do not write
            return 0
        dir = self.make_path_name(dir,*args) # Make an absolute path
        if not self.isdir(dir):
            if self._sftp==None:
                os.makedirs(dir)
            else:
                self._sftp.mkdir(dir)
        elif self._verbose>0:
            print "Directory already exists!"

            
    def listdir(self,dir=""):
        r"""
        List a directory relative to self._directory.

        EXAMPLES::

            
            sage: F.listdir("")
            Traceback (most recent call last)
            ValueError:  is not a valid path!
            sage: F.listdir(".")
            ['Gamma0']

        """
        dir = self.make_path_name(dir)
        if not self.isdir(dir):
            return []
        if self._sftp == None:
            return os.listdir(dir)
        else:
            return self._sftp.listdir(dir)

    def isdir(self,path,*args):
        r"""
        Check if directory path exists.

        EXAMPLES::

        
            sage: F.isdir(".")
            True
            sage: F.isdir("Gamma0")
            True

        """
        path = self.make_path_name(path)        
        if self._sftp==None:
            return os.path.isdir(path)
        else:
            try:
                return S_ISDIR(self._sftp.stat(path).st_mode)
            except IOError:
                # Path does not exist, so by definition not a directory
                return False

    def path_exists(self,path):
        r"""
        Check if the path f exists as a local file or on the remote host.

        EXAMPLES::


            sage: F.path_exists("Gamma0")
            True
            

        """
        path = self.make_path_name(path)
        self.check_path(path)        
        if self._sftp == None:
            return os.path.exists(path)
        else:
            return rexists(self._sftp,path)

    def read_file(self,filename,**kwds):
        r"""
        Read a file.
        """
        record = {}
        filename = self.make_path_name(filename)
        if kwds.get('verbose',0)>0:
            print "Reading: {0}".format(filename)
        try:
            with open(filename,"r") as readfile:
                if kwds.get('format')=='json':
                    record = json.load(readfile)
                else:
                    record = readfile.read() 
        except IOError:
            pass
        return record

    def delete_file(self,path):
        r"""
        delete a file
        """
        path = self.make_path_name(path)
        if self._sftp == None:
            os.unlink(path)
        else:
            self._sftp.remove(path)

    def delete_dir(self,path):
        r"""
        delete a file
        """
        path = self.make_path_name(path)
        if self._sftp == None:
            os.rmdir(path)
        else:
            self._sftp.rmdir(path)
   
        
    def create_and_write_to_file(self,s,filename,overwrite=False,**kwds):
        r"""
        Create a file (if it doesn't exist) and write the string s to it.
        If overwrite = True we overwrite existing files.
        We try to get exclusive lock using a temporary file.
        """
        if self._verbose>0:
           print "filename=",filename
           print "kwds=",kwds
        if self._dry_run: # Do not write
            return 0
        self.check_path(filename) # Check the path we want to write to
        if self._sftp == None:
            return self.create_and_write_to_file_local(s,filename,overwrite,**kwds)
        else:
            return self.create_and_write_to_file_remote(s,filename,overwrite,**kwds)
    
        
    def create_and_write_to_file_local(self,s,filename,overwrite=False,**kwds):
        r"""
        Create a file (if it doesn't exist) and write a string to it.
        If overwrite = True we overwrite existing files.
        We try to get exclusive lock using a temporary file.
        """
        # Make absolute path name (if not already absolute)
        filename = self.make_path_name(filename)
        if self._verbose>0:
            print "filename_local=",filename
        if self.path_exists(filename) and not overwrite:
            return         
        filename_stripped=join(filename.split(".")[0:-1],".") 
        lockfile = "{0}.lock".format(filename_stripped)
        dirname=join(filename.split("/")[0:-1],"/" )
        dirname+="/"
        if self._verbose>0:
            print "dirname_local=",dirname
        if not self.path_exists(dirname):
            self.makedirs(dirname)
        if self._verbose>0:
            print "lock file = ",lockfile
        has_written = False
        i = 1 # avoid infinite loops
        while not has_written and i<=10000:
            try:
                fp = open(lockfile,"r")
                fp.close() 
                sleep(1) ## if lock file exists we wait for a bit.
                i+=1
            except IOError: # otherwise create lock and write to file
                fplock = open(lockfile,"w")
                fp = open(filename,"w")
                fp.write(s)
                fp.close()
                fplock.close()
                os.remove(lockfile)
                has_written = True
                break
        if i==10000:
            return 1
        return 0

    def create_and_write_to_file_remote(self,s,filename,overwrite=False,**kwds):
        r"""
        Create a remote file (if it doesn't exist) and write a string to it.
        If overwrite = True we overwrite existing files.
        We try to get exclusive lock using a temporary file.
        """
        
        raise NotImplementedError," TODO!"

