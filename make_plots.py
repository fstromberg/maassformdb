from psage import *
from multiprocessing import Pool
from mwfdb.maass_forms_db import MaassDBMongo
from psage import MySubgroup,MaassWaveForms
from psage.modform.maass.maass_forms import Maasswaveform
import pymongo

import os

password = os.getenv('MONGODB_PASSWORD','test')
user = os.getenv('MONGODB_USER','editor')

#C = pymongo.MongoClient(host='localhost',port=37010)
#DB = C['MaassWaveForms']

#DB = MaassDBMongo(host='localhost',port=37010,user=user,password=password)


def generate_plots(s,xlim=(-0.5,0.5),ylim=(0,2),num_pts=150,dpi=300,ncpus=8,chunksize=20,limit=0):
    r"""

    """
    idlist = DB._mongo_db['maassform_plots'].find({"num_pts":{"$gt": int(num_pts-1)},"dpi":{"$gt": int(dpi-1)}}).distinct('maass_id')
    l = Maass_waveforms_from_mongoDB(s,id_not=idlist,limit=limit)
    args = []
    for F,fid in l:
        q = DB._mongo_db['maassform_plots'].find({'maass_id':fid,"num_pts":{"$gt": int(num_pts-1)},"dpi":{"$gt": int(dpi-1)}})
        if q.count() == 0:  ## If it is not in the database
            print "Making new plot for N:{0} R:{1}!".format(F.level(),F.eigenvalue())
            args.append((F,fid,xlim,ylim,num_pts,dpi))
    pool = Pool(processes=ncpus)
    results = pool.imap_unordered(make_plot_for_F1,args,chunksize)
    return list(results)


def make_plot_for_F1(t):
    print t
    F,mid,xlim,ylim,num_pts,dpi = t
    import pymongo
    import bson
    import tempfile
    g = F.plot(xlim,ylim,num_pts)
    filename = 'plot-{0}-{1:0>12.4f}-{2:0>4}-{3:0>4}.png'.format(F.level(),F.eigenvalue(),dpi,num_pts)
    tmpfilename = "/tmp/{0}.png".format(mid)
    g.savefig(tmpfilename,dpi=dpi)
    data = open(tmpfilename).read()
    db = DB._mongo_db['maassform_plots']
    idp = db.insert({'level': int(F.level()),
                        'dpi':int(dpi),
                        'num_pts':int(num_pts),
                        'eigenvalue':float(F.eigenvalue()),
                        'maass_id':mid,
                        'plot': bson.binary.Binary(dumps(data))})
    if idp:
        print "inserted {0}".format(idp)
    os.unlink(tmpfilename)

def Maass_waveforms_from_mongoDB(s,with_id=True,id_not=[],verbose=0,limit=0):
    from sage.all import is_prime
    from compmf import character_conversions
    res = []
    q = DB.get_Maass_forms(s,limit=limit)
    for r in q:
        if r['_id'] in id_not:
            continue
        G = MySubgroup(Gamma0(r['Level']))
        if r.get('Conrey',0)==1:
            x = character_conversions.dirichlet_character_sage_from_conrey_character_number(r['Level'],r['Character'])
        else:
            x = character_conversions.sage_character_from_number(r['Level'],r['Character'])
        M = MaassWaveForms(G,weight = r['Weight'],sym_type = r['Symmetry'],ch=x,verbose=1)
        d = r.get('Dimension',1)
        numc = r.get('Numc',0)
        if r.get('coeff_id') is None:  ## We probably have other contributor than me....
            N = r['Level']
            if not is_prime(N):
                print "we don't have coefficients of corect format and not prime level for r={0}!".format(r['Eigenvalue'])
                continue
            if r.get('Fricke') is None:
                continue
            c = r.get('Coefficient')
            #return 
            if not c:
                c = r.get('Coefficients')
            if not c:
                continue
            coeffs = {0:{0:{0:{},1:{}}}}
            v = r['Fricke']
            for  i in range(len(c)):
                coeffs[0][0][0][i+1] =c[i]
                coeffs[0][0][1][i+1] =v*c[i]
        else:
            coeffs = DB.get_coefficients({'_id':r['_id']})
        
        #    print "coeffs=",coeffs[0][0][1].keys()
#        return 
        if not coeffs:
            print "No coefficient for M={0} and r={1} with id={2}".format(M,r['Eigenvalue'],r['_id'])
            continue
        for i in range(d):
            try:
                F = Maasswaveform(M,r['Eigenvalue'],compute = 0,dim=d,coefficients=coeffs[i])
            except ValueError:
                print "Failed for id={0} and coeffs = {1},{2}".format(r['_id'],len(coeffs),len(coeffs[0]))
            if with_id:
                res.append((F,r['_id']))
            else:
                res.append(F)
            if verbose>0:
                print "constructed Maassform of level {0} and eigenvalue {1}".format(F.level(),F.eigenvalue())
            print "F=",F
#    if len(res)==1:
#        return res[0]
    return res



def build_contour(G,**kwds):

    from sage.all import CC
    from sage.plot.all import Graphics
    from sage.all import hyperbolic_arc,hyperbolic_polygon
#    FD=G.draw_fundamental_domain(version=0,method='a',model='H',fill=False,show_tesselation=False,contour=True,draw_circle=False,rgbcolor=kwds.get('color','red'))
    FD=G.draw_fundamental_domain(version=0,method='a',model='H',fill=False,show_tesselation=True,contour=True,draw_circle=False,rgbcolor=kwds.get('color','red'),verbose=1,as_arcs=True)
    return FD
    # P consists of a list of Triangles.
    triangles = []
    minA = min(x.A for x in FD)
    minB = min(x.B for x in FD)
    maxA = max(x.A for x in FD)
    maxB = max(x.B for x in FD)    
    ## The vertice C is always a cusp
    minx = min(minA,minB)
    maxx = max(maxA,maxB)
    zl0 = CC(-1/2.0,sqrt(3.0)/2)
    zr0 = CC(1/2.0,sqrt(3.0)/2)
    zi0 = CC(0,1)
    contour = Graphics()
    contour._axes_range = FD._axes_range
    eps = 1e-15
    vertices = []
    def is_in(z):
        if z.real() > 0.5 + eps:
            return False
        if z.real() < -0.5 -eps:
            return False
        if abs(z) < 1 - eps:
            return False
        return True
    def is_exterior(z,A):
        for B in G._coset_reps_v0:
            if B == A:
                continue
            BI = B.inverse()
            z_pb = BI.acton(z)
            #print A,z_pb
            if is_in(z_pb):
                return False
        return True
    
    for i in range(len(FD)):
        P = FD[i]  ### A hyperbolic triangle which is an image of the standard triangle for SL2(Z)
        ## If x is a Vertical triangle.
        #if x.C.imag() > 0:
        #    if (x.A > xmin and x.B > xmin) and (x.A < xmax and x.B < xmax):
        #        continue
        #    triangles.append(x)
        #else:
        A = G._coset_reps_v0[i] 
        ## Check (image of) left arc, i.e. P.C - P.A
        ## We want to determine if this is contained in the interior
        ## of the fundamental domain or not.
        ## Our test is that if it gets pulled-back to the standard (closed) fundamental domain
        ## by another coset representative then it is interior.
        zl = A.acton(zl0)
        print A
        if is_exterior(zl,A):
            ## If we are here then the edge P[i].C - P[i].A
            ## is exterior and to be kept.
            arc = hyperbolic_arc(P.C,P.A)
            if contour is None:
                contour = arc
            else:
                contour = contour + arc
            zl = A.acton(zl0)
            vertices.append(P.C)
            vertices.append(P.A)
        zr = A.acton(zr0)            
        if is_exterior(zr,A):
            ## If we are here then the edge P[i].C - P[i].A
            ## is exterior and to be kept.
            arc = hyperbolic_arc(P.B,P.C)
            if contour is None:
                contour = arc
            else:
                contour = contour + arc
        zi = A.acton(zi0)            
        vertices.append(P.B)
        vertices.append(P.C)
        if is_exterior(zi,A):
            ## If we are here then the edge P[i].C - P[i].A
            ## is exterior and to be kept.
            arc = hyperbolic_arc(P.A,P.B)
            if contour is None:
                contour = arc
            else:
                contour = contour + arc
            vertices.append(P.A)
            vertices.append(P.B)
    return hyperbolic_polygon(vertices)
    #return contour
        
        
        
def build_connected_path(P):
    from sage.all import deepcopy,hyperbolic_arc
    paths = []
    ymax = P._axes_range['ymax']
    for x in P:
        if x.A.imag() >= 10000:
            ### these have to be treated specially..
            ### We truncate to the maximum y height
            ### and set the x-coordinate to the same as the other endpoint.
            A = CC(x.B.real(),ymax)
            B = CC(x.B.real(),x.B.imag())
            paths.append(hyperbolic_arc(A,B)[0])
            xmin = x.B.real()
        elif x.B.imag() >= 10000:
            ### these have to be treated specially..
            ### We truncate to the maximum y height
            ### and set the x-coordinate to the same as the other endpoint.
            A = CC(x.A.real(),x.A.imag())
            B = CC(x.A.real(),ymax)
            paths.append(hyperbolic_arc(A,B)[0])
            xmax = x.A.real()
        else:
            paths.append(x)
    ## Add a 'closing' path between the two vertical sides.
    paths.append(hyperbolic_arc(CC(xmin,ymax),CC(xmax,ymax))[0])

    new_paths = []
    ## first find the left most:
    ## an arc has a .A and .B
    As = [x.A for x in P]
    minA = min(As)
    mini = As.index(minA)
    new_paths = [P[mini]]
    paths.remove(P[mini])



    eps = 1e-15
    current = P[mini].B
#    print "paths=",paths
#    print "paths=",dir(new_paths[-1])
#    return new_paths
    while paths != []:
        current = new_paths[-1].B
        print "current=",current
        try:
            for p in paths:
                print "p.A=",p.A
                print "p.B=",p.B
                if abs(p.A-current)<eps:
                    print "appending p"
                    new_paths.append(p)
                    paths.remove(p)
                    raise StopIteration                    
                elif abs(p.B-current)<eps: ## we reverse it 
                    print "appending p reversed"
                    pnew = hyperbolic_arc(p.B,p.A)
                    new_paths.append(pnew[0])
                    paths.remove(p)
                    raise StopIteration
                else:
                    continue

            print paths
            raise ArithmeticError("Could not connect from {0}".format(current))
        except StopIteration:
            pass
    ### new paths is now a list of hyperbolic arcs
    res = []
    import matplotlib.patches as patches
    for p in new_paths:
        pt = patches.Path(p.vertices,p.codes)
        res.append(pt)
    res = patches.Path.make_compound_path(*res)
    return res


    
        
