r"""
Database schema for Maass waveforms using SQLAlchemy and Elixir.

NOTe: Now using Camelot (has better/ more intuitive session handling)

Author: Fredrik Stromberg

Note: To avoid conflicting records you should use methods in maass_forms_db.py to insert records into the database instead of creating instances of the classes  below directly. 






"""
import inspect
import os
from elixir import Entity,metadata,Field,session,setup_all,create_all,ManyToOne,OneToMany,ManyToMany,DateTime,using_table_options,using_options,has_one

from sqlalchemy.ext.associationproxy import AssociationProxy
from sqlalchemy import Table, Column, ForeignKey,UniqueConstraint,create_engine,ThreadLocalMetaData
from sqlalchemy.orm import scoped_session

import sl2z_subgroups
from sl2z_subgroups import *
from sl2z_subgroups.grp_schema import *
from sqlalchemy.types import UnicodeText,Unicode,Float
from sqlalchemy.types import Integer as saInteger
from sqlalchemy.orm import relation, backref,eagerload,sessionmaker,relationship
from sqlalchemy.ext.declarative import declarative_base
from sage.functions.other import real,imag,sqrt

f = inspect.getabsfile(inspect.currentframe())
datadir = "/".join(os.path.dirname(f).rsplit("/")[0:-1]+["data"])
print "datadir:",datadir
#mwfdb_engine = create_engine("sqlite:///{0}/maassforms.sqlite".format(datadir), echo=True)
mwfdb_engine = create_engine("sqlite:///{0}/maassforms.sqlite".format(datadir), echo=False)
mwfdb_session = scoped_session(sessionmaker(autoflush=True, expire_on_commit=False,bind=mwfdb_engine))
mwfdb_metadata = ThreadLocalMetaData()
mwfdb_metadata.bind = mwfdb_engine

__metadata__ = mwfdb_metadata
__session__ = mwfdb_session

from sqlalchemy.orm import object_session
from sage.all import CC
from sqlalchemy import types
from sqlalchemy.types import UserDefinedType,TypeDecorator

class ComplexNumberType(TypeDecorator):
    impl = types.String

    def __init__(self,precision,*arg,**kw):
        super(ComplexNumberType,self).__init__()
        self.precision = precision

    def process_bind_param(self, value, dialect=None):
        r"""
        Convert to internal string rep. from complex number of desired precision.
        """
        if value is not None:
            if isinstance(value,complex):
                re = value.real; im=value.imag
                # We can't get more precision anyway                
                value = unicode("{0}+{1}*I".format(re,im)) 
            elif isinstance(value,ComplexNumber):
                value = unicode(ComplexField(self.precision)(value))
            else:
                raise ValueError,"Can not convert {0} to ComplexNumberType!".format(value)
        return value

    def process_result_value(self, z, dialect=None):
        r"""
        Convert back from internal string rep. to complex number of desired precision.
        """

        if z is None:
            return None
        return ComplexField(self.precision)(str(z))
    
#    def __add__(self,other):
#        return ComplexNumber_DB(self.re+other.re,self.im+other.im)
    
#    def __repr__(self):
#        if self.im==0:
#            return str(self.re)
#        else:
#            return "{0}+{1}i".format(self.re,self.im)
    
    



class Maassform_DB(Entity):
    r"""
    Class for working with database of Maass waveforms.

    Note::

    (0)  A priori an element of this class represents a vector space of Maass forms,
         i.e. it can have dimension>1 (for example in the case of oldforms)
         
    (1)  I can not figure out how to make a relationship to an instance
         of SL2Zsubgroup defined in a different database (i.e. different engine)
         The attempts below are all insufficient...
         For now I am satisfied by havine p2 and p3 of the group stored here.
         and on insert we should make sure that the group is in the database of groups.
    (2) I also don't know how to make unique constraints.
        The problem is that even a naive definition of uniqueness would
        involve at least comparing Atkin-Lehner eigenvalues.
        As a consequence:
        A manual check if the data you want to insert already exists in the database is necessary.


    """
    using_options(metadata=mwfdb_metadata, session=mwfdb_session,tablename="mwfdb")  
    # Since we might have the SL2Zsubgroups in another database
    # it is better to store the unique permutation representation of the group.
    group_p2 = Field(UnicodeText,default=u'',required=True)
    group_p3 = Field(UnicodeText,default=u'',required=True)
    ## extra information about the group to make searches faster
    group_name = Field(UnicodeText,default=u'',required=True)
    group_index=Field(saInteger,required=True)
    group_level = Field(saInteger)
    ## Add cusps so that we know where we can have Fourier coefficients
    group_cusps_list = ManyToMany('Cusp_Maassform',inverse='form')
    group_cusps = AssociationProxy('group_cusps_list','rep',
                                                creator = lambda (ca,cb): Cusp_Maassform(a=ca,b=cb))
    # The weight of self
    weight = Field(Float,default=float(0))
    # The character of self
    character = ManyToOne('Character_DB',inverse='form')    
    symmetry = Field(saInteger)    # -1 or 1 for even / odd forms, 0 if unknown or not applicable
    # The eigenvalue (or rather R=sqrt(lambda-1/4) ) in double precision
    eigenvalue =  Field(Float,required=True)
    # possible multi precision eigenvalue
    eigenvalue_string = Field(UnicodeText,default=u'')
    error =  Field(Float,default=float(0))
    # We have to insert these as separate entries in order to make a unique key
    
    # Data used for computing this Maass form (good for checking)
    M0 = Field(saInteger,default=int(0))
    Y = Field(Float,default=float(0.0))
    
    atkin_lehner_eigenvalues_list = ManyToMany('ALEigenvalues',inverse='form')
    atkin_lehner_eigenvalues = AssociationProxy('atkin_lehner_eigenvalues_list','eigenvalue',
                                                creator = lambda (ca,cb,ev): ALEigenvalues(cusp_a=ca,cusp_b=cb,eigenvalue=ev))

    ## Dimension of the eigenspace corresponding to this form (i.e. with same eigenvalue, group, weight and character)
    dimension = Field(saInteger,default=int(1))
    
    ## Fourier coefficients as instances of MaassCoefficient_DB
    ## containing both floating point and possible multi-precision values
    
    coefficient_list = OneToMany('MaassCoefficient_DB',inverse='form')
    coefficients = AssociationProxy('coefficient_list','value',
                                    creator = lambda (i,cusp,n,c) : MaassCoefficient_DB(nr=int(i),cusp=cusp,n=int(n),value = c))
    numc = Field(saInteger,default=int(0))
    
    ## Encode if we give coefficients Hecke normalized (norm=0) or L2-normalized, i.e. c(1)=1/||f||_2  (norm=1)
    norm = Field(saInteger,default=int(0))

    ## metadata

    contributor = Field(UnicodeText,default=u'')
    metadata = Field(UnicodeText,default=u'')
    name = Field(UnicodeText,default=u'')  # Do we want to give Maass forms names if they get famous? 
    date = Field(DateTime)

    @property
    def group(self):
        group_p2 = Field(UnicodeText,default=u'',required=True)
        group_p3 = Field(UnicodeText,default=u'',required=True)
        return PSL2Zsubgroup.query.filter(PSL2Zsubgroup.p2==group_p2,PSL2Zsubgroup.p3==group_p3).all()[0]
    
    def get_group(self):
        return object_session(self).query(PSL2Zsubgroup).with_parent(self).all()


    def __repr__(self):
        if self.group_name<>'':
            gn = self.group_name
        else:
            gn = "subgroup of index {0} given by S2={0} and S3={1}".format(self.group_index,self.group_p2,self.group_p3)
            
        s="Maass waveform with R={0} on {1}".format(self.eigenvalue,gn)
        return s
    
    def add_atkin_lehner(self,al_dict={}):
        for k in al_dict.keys():
            ca,cb = k
            print "insert=",ca,cb,al_dict[k]
            self.atkin_lehner_eigenvalues.append((ca,cb,al_dict[k]))

    def group_cusp(self,k):
        if self._group_cusps.has_key(k):
            return self._group_cusps

    def add_coefficients(self,coefficients=[]):
        r"""
        Add coefficients from a list of dicts. The length of the list is the dimension of sels.
        Each dict of coefficients is of the form :
        (ca,cb) : { n : c }
        where (ca,cb) represents a cusp, n is an integer and c is the value of the coefficient (complex number)
        
        """
        numc = 0
        for d in len(coefficients):
            for k in coefficients[d].keys():
                if isinstance(k,(int,Integer)):
                    ca,cb = self.group_cusps(k)
                ca,cb=k
                for n in coefficients[d][k].keys():
                    c = coefficients[d][k][n]
                    if isinstance(c,complex):
                        x = c.real; y = c.imag
                    else:
                        x = c.real(); y=c.imag()
                    self.coefficients.append(d,ca,cb,n,x,y)
                    if d==0 and k==0: # We count coefficients at infinity
                        numc+=1
        self.numc = numc

    def add_one_coefficient(self,nrf,cusp,n,c,check=True):
        if isinstance(cusp,tuple):
            ca,cb=cusp
            cusp = Cusp_Maassform.query.filter(Cusp_Maassform.a==ca,Cusp_Maassform.b==cb,form=self)
        elif isinstance(cusp,Cusp_Maassform):
            pass
        elif isinstance(cusp,(int,Integer)):
            cusp = self.group_cusps_list[cusp]
        ok = 1
        if check == True:
            # Test to see if this coefficient is already in the list
            q = MaassCoefficient_DB.query.filter(MaassCoefficient_DB.form_id==self.id,
                                                 MaassCoefficient_DB.nr==nrf,
                                                 MaassCoefficient_DB.cusp==cusp,
                                                 MaassCoefficient_DB.n==n)
            #print "q.count()=",q.count()
            if q.count()==0:
                ok = 0
        if ok==1:
            self.coefficients.append((nrf,cusp,n,c))
            if nrf==0 and cusp.a==1 and cusp.b==0 and n<>0:
                self.numc+=1


def get_Character_DB(modulus,number):
    res = Character_DB.query.filter(Character_DB.modulus==modulus,Character_DB.number==number).all()
    if len(res)==0:
        res = Character_DB(modulus=modulus,number=number)
    else:
        res = res[0]
    return res
            
class Character_DB(Entity):
    r"""
    Character.
    """
    using_options(metadata=mwfdb_metadata, session=mwfdb_session)  
    modulus = Field(saInteger)
    number = Field(saInteger)
    using_table_options(UniqueConstraint( 'modulus','number' ))
    form = OneToMany(Maassform_DB)
    def __repr__(self):
        return "chi_{{ {0} }} mod {1}".format(self.number,self.modulus)
    
class MaassCoefficient_DB(Entity):
    r"""
    Coefficients for Maass forms given by complex floating point numbers
    """
    using_options(metadata=mwfdb_metadata, session=mwfdb_session)  
    nr =  Field(saInteger)
    cusp = ManyToOne('Cusp_Maassform')
    n = Field(saInteger) #,primary_key=True)
    value = Field(ComplexNumberType(53))
    form = ManyToOne(Maassform_DB)
    


    def __repr__(self):
        r"""
        Represent self. 
        """
        return str(self.value)
                      

class MaassCoefficient_gen(Entity):
    r"""
    Coefficients for Maass forms given as a string. Can be used for multiprecision or algebraic numbers.
    """
    using_options(metadata=mwfdb_metadata, session=mwfdb_session)  
    index = Field(saInteger)
    value = Field(UnicodeText,default=u"")
    form = ManyToMany(Maassform_DB)
    def __repr__(self):
        return str(self.value)

class RootOfUnity_DB(Entity):
    r"""
    Class corresponding to elements z=exp(2*pi*i*exponent/order)
    """
    #value = ManyToMany('ALEigenvalues')
    using_options(metadata=mwfdb_metadata, session=mwfdb_session)
    order = Field(saInteger)
    exponent = Field(saInteger)
    def __repr__(self):
        if self.order==2 and self.exponent==0:
            s = "1"
        elif self.order==2 and self.exponent==1:
            s = "-1"
        else:
            s = "z_\{{0}\}^\{1\}".format(self.order,self.exponent)
        return s
     
class ALEigenvalues(Entity):
    r"""
    Atkin-Lehner eigenvalues.
    """
    using_options(metadata=mwfdb_metadata, session=mwfdb_session)  
    form = ManyToMany('Maassform_DB')
    eigenvalue = RootOfUnity_DB
    cusp_a = Field(saInteger)
    cusp_b = Field(saInteger)

    def __repr__(self):
        if self.cusp_b == 0:
            cusp = "Infinity"
        else:
            cusp = "{0}/{1}".format(self.cusp_a,self.cusp_b)
        s = "Atkin-Lehner eigenvalue {0} of {1} at cusp {2}".format(self.eigenvalue,self.form,cusp)
        return s


    
class Cusp_Maassform(Entity):
    r"""
    Class representing cusps of groups associated with Maass waveforms
    """
    a = Field(saInteger)
    b = Field(saInteger)
    rep = Field(UnicodeText,default=u"")
#    form = relationship("Maassform_DB", backref=backref('group_cusps', order_by=id))
    form = ManyToMany('Maassform_DB')
    def __init__(self,a,b):
        self.a = a
        self.b = b
        if self.b == 0:
            self.rep = "Infinity"
        else:
            self.rep = "{0}/{1}".format(self.a,self.b)    

    def __repr__(self):
        return self.rep

    def __eq__(self,other):
        return self.a == other.a and self.b == other.b


        
