## -*- coding: utf-8 -*-
#*****************************************************************************
#  Copyright (C) 2013 Fredrik Strömberg <stroemberg@mathematik.tu-darmstadt.de>,
#
#  Distributed under the terms of the GNU General Public License (GPL)
#
#    This code is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#  The full text of the GPL is available at:
#
#                  http://www.gnu.org/licenses/
#*****************************************************************************
###
### Class for computing and storing Maass waveforms.
### Use either mongodb, SQL or simple file storage
###

r"""
File with routines for finding eigenvalues.
Once working they will be moved to the psage repo.

NOTE: In this file we assume the Conrey notation for characters.
This leads to necessary conversion when defining the Maasswaveform spaces

"""

datadir = "~/tmp/"

import datetime
import socket
import logging

from sage.parallel.decorate import fork
from sage.all import euler_phi,sign,sqrt,save,load,Gamma0,AbelianGroup,version,QQ,RealField,deepcopy,log_b
from psage.groups.dirichlet_conrey import *
from sage.rings.complex_number import ComplexNumber
from psage.modform.maass.all import MaassWaveForms,Maasswaveform
from psage.modform.arithgroup.all import MySubgroup
from psage.modform.maass.maass_forms import prediction,find_Y_and_M,get_M_and_Y
from psage.modform.maass.maass_forms_alg import get_coeff_fast_cplx_dp
from psage.modform.maass.maass_forms_phase2 import phase_2_cplx_dp_sym
from sage.functions.generalized import sign
from mwfdb.maass_forms_db import *

from sage.parallel.decorate import parallel


#FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
#logging.basicConfig() #format=FORMAT)


try:
    import colorlog
    from colorlog import ColoredFormatter
    LOG_LEVEL = logging.DEBUG
    logging.root.setLevel(LOG_LEVEL)
    LOGFORMAT = "  %(log_color)s%(levelname)-10s%(filename)s:%(lineno)d%(reset)s | %(log_color)s%(message)s%(reset)s"
    formatter = ColoredFormatter(LOGFORMAT)
    stream = logging.StreamHandler()
    stream.setLevel(LOG_LEVEL)
    stream.setFormatter(formatter)
    flogger = logging.getLogger(__name__)
    logger2 = logging.getLogger('detailed log')
    if not flogger.handlers:
        flogger.addHandler(stream)
    if not logger2.handlers:
        logger2.addHandler(stream)    
except:
    logging.basicConfig(
        format='%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]',
        datefmt='%Y%m%d-%H:%M%p',
    )
    print "No module colorlog present!"
    flogger = logging.getLogger(__name__)
    logger2 = logging.getLogger('detailed log')
flogger.setLevel(logging.WARNING)
logger2.setLevel(logging.WARNING)

## Global vartiables





do_parallel = 1
ncpus = 2

def find_Maass_for_range_N(Nrange=[1],Rrange=[1,5],char='all',filter='',weight=0,new=True,verbose=0,force=False,db=None,cusp_evs='all',tol=1e-10,method='TwoY',hecke_ef=False,sloppy=0):
    r"""
    new=True means that we only compute pairs (N,char) which are not computed before.
    """
    if db==None:
        db = MaassDB()
    res=[]
    R1,R2 = Rrange
    if verbose>0:
        print "locating eigenvalues for N in range {0}--{1} and R in range {2}--{3}".format(N1,N2,R1,R2)
    try:
        for N in Nrange:
            if filter=='prime':
                if not is_prime(N):
                    flogger.info("Skipping non-prime N:{0}".format(N))
                    continue
            if filter=='sqfree':
                if not is_squarefree(N):
                    flogger.info("Skipping non-square-free N:{0}".format(N))
                    continue
            if verbose>0:
                print "N=",N,"char=",char
            G=MySubgroup(Gamma0(N))
            l = find_Maass_for_one_grp(G,R1,R2,ch=char,weight=weight,new=new,verbose=verbose,force=force,db=db,
                                       cusp_evs=cusp_evs,tol=tol,method=method,hecke_ef=hecke_ef,sloppy=sloppy)
            if l:
                res.extend(l)
    except KeyboardInterrupt:
        pass
    return res
def find_Maass_for_one_grp(G,R1,R2,ch='all',weight=0,new=True,verbose=0,force=False,db=None,cusp_evs='all',tol=1e-10,method='TwoY',hecke_ef=True,sloppy=0):
    r"""
    Set cusp_evs to 'none' to not use symmetries (if you want to find the corr. eigenvalues, for example)
    """
    if isinstance(G,(int,Integer)):
        G = MySubgroup(Gamma0(G))
    print "check ch=",ch
    if not G._is_Gamma0:
        ch=0
    if ch=='all':

        if not hasattr(db,'Dirchars'):
            chars=DirichletGroup_conrey(G._level)
        elif weight==0:
            chars = db.Dirchars(G._level,parity=0)
        elif weight==1:
            chars = db.Dirchars(G._level,parity=0)
        else:
            raise NotImplementedError
        # for orbit in D.galois_orbits():
        #     listR=list()
        #     x = orbit[0]
        #     if not x.is_even():
        #         continue
        #     chars.append(x)
    elif ch>=0:
        chars = [ch] #DirichletCharacter_conrey(DirichletGroup_conrey(G._level),ch)]
        #[DirichletGroup(G._level).list()[ch % G._level]]
    #else:
    #    chars=[[DirichletGroup(G._level).list()[0]]
    res=[]
    if verbose>0:
        print "Check chars:",chars
    for x in chars:
        if isinstance(x,(int,Integer)):
            xj=x
            x = DirichletCharacter_conrey(DirichletGroup_conrey(G._level),xj)
        elif hasattr(x,"number"):
            xj = x.number()   #D.list().index(x)
        else:
            raise ArithmeticError
        if db.has_level_weight_char(G._level,weight,xj)>0 and not force:
            if verbose>0:
                print "Already have level,weight,char:",G._level,weight,xj
            continue
        d = max_assumed_dim(x)
        l=find_Maass_for_one_G_ch(G,R1,R2,ch=xj,dimstart=1,dimstop=d,weight=weight,verbose=verbose,force=force,db=db,cusp_evs=cusp_evs,tol=tol,method=method,hecke_ef=hecke_ef,sloppy=sloppy)
        res.extend(l)
    return res

def find_Maassforms_for_list_of_groups(glist,MF,R1=0.0,R2=5.0,**kwds):
    r"""
    Find Maassforms with eigenvalues in a given range for a list of groups and insert into database. 
    """
    input = []
    if kwds.get('exceptional',False):
        R1 = 0; R2=0.5
    for G in glist:
        input.append( ((G,MF,float(R1),float(R2)),kwds) )
        #print "input=",G,R1,R2
    #print "kwds=",kwds
    return find_Maassforms_for_one_group(input,**kwds)




@parallel(ncpus=4)
def find_Maassforms_for_one_group(G,MF,R1,R2,verbose=0,empty_only=1,ret_list=False,**kwds):
    r"""
    Find Maass waveforms for one group and eigenvalue in a givne interval. If empty_only=1
    then we only look at spaces where there are no Maasswaveforms yet found.
    """
    #print "Checking R1={0},R2={1} of type {2}".format(R1,R2,type(R1))
    res = [] #o=[]; rese=[]
    #print "kwds=",kwds
    # Check if this group has been computed?
    rec = record_from_group(G)
    dirname = MF.dirname(rec,check=0,for_reading=0)
    fname = "checking_{0:0>4.2f}_{1:0>4.2f}".format(float(R1),float(R2))
    checking_fname = "{0}/{1}".format(dirname,fname)
    fname = "checked_{0:0>4.2f}_{1:0>4.2f}".format(float(R1),float(R2))
    checked_fname = "{0}/{1}".format(dirname,fname)
    s = "Checking R in [{0},{1}]".format(float(R1),float(R2))
    MF.create_and_write_to_file(s,checking_fname,overwrite=True)
    M = MaassWaveForms(G,**kwds) #,sym_type=-1)
    flogger.debug("M._exceptional = {0}".format(M._exceptional))
    res = find_Maassforms_for_one_space(M,R1,R2,verbose=verbose,db=MF)
    s = "Checked R in [{0},{1}]".format(float(R1),float(R2))
    MF.create_and_write_to_file(s,checked_fname,overwrite=True)
    if ret_list:
        return {G:res} #{'odd':reso,'even':rese}
    else:
        return

def find_Maass_for_one_G_ch(G,R1,R2,ch=0,dimstart=1,dimstop=1,weight=0,verbose=0,force=False,db=None,sym='all',cusp_evs='all',tol=1e-10,method='TwoY',hecke_ef=True,sloppy=0):
    r"""
    If called without a db we simply compute and store a pickle
    """
    if isinstance(G,(int,Integer)):
        N = G
        G = MySubgroup(Gamma0(N))
    elif hasattr(G,"level"):
        if not hasattr(G,"permS"):
            G = MySubgroup(G)
        N = G.level()
    if verbose>0:
        print "Searching in Gamma0({0}) with char={1}".format(N,ch)
    if weight<>0:
        raise NotImplementedError,"Maass forms for non-zero weight are not implemented"
    if sym=='all':
        sym_types=[0,1]
    elif isinstance(sym,(int,Integer)):
        sym_types=[sym]
    else:
        sym_types=[-1]
    if db:
        db.register_work(level=N,ch=ch,weight=weight,R1=R1,R2=R2,dim=(dimstart,dimstop),verbose=verbose,force=force)
    if G._is_Gamma0:
        lab = (N,ch)
    else:
        lab = str(G)
    level=G._level
    res = []
    Dl = DirichletGroup(level).list()
    x = DirichletCharacter_conrey(DirichletGroup_conrey(level),ch).sage_character()
    chh = Dl.index(x)
    M=MaassWaveForms(G,weight=0,ch=chh)
    if cusp_evs=='all':
        c_evs=get_list_of_cusp_symmetries(M,add_zero=1)
    elif isinstance(cusp_evs,(list,dict)):
        c_evs=[cusp_evs]
    try:
        for st in sym_types:
            for evs in c_evs:
                if not compatible_sym(M,st,evs):
                    continue
                M.set_sym_type(st)
                M.set_cusp_evs(evs)
                if verbose>0:
                    print "Checking sym_type:",st
                    print "Eigenvalues:",M.cusp_evs()
                #l=[]
                l = find_Maassforms_for_one_space(M,R1,R2,dimstart=dimstart,
                                                  dimstop=dimstop,weight=weight,
                                                  verbose=verbose,method=method,force=force,db=db,tol=tol,hecke_ef=hecke_ef,
                                                  sloppy=sloppy)
                if l:
                    res.extend(l)
    except KeyboardInterrupt:
        pass
    if db:
        db.deregister_work()
    return res

def  compatible_sym(M,st,evs):
    for i in range(M._group._ncusps):
        if evs.get(i,0)<>0 and M.even_odd_symmetries()[i][0]<>1 and st<>-1:
            return 0
    return 1

def find_Maassforms_for_one_space(M,R1,R2,Mset=None,Yset=None,dimstart=1,dimstop=1,weight=0,dim=None,verbose=0,force=False,db=None,tol=1e-10,hecke_ef=False,method='TwoY',sloppy=0,get_oldforms=False,get_newforms=True):
    r"""
    Try to find Maass forms in a given space with given parameters. 
    """
    #assert m._sym_type in [0,1,-1]
    if verbose>4:
        M._verbose=verbose
    G = M.group()
    ch = M.character()
    if not G.is_congruence():
        hecke_ef = False
    else:
        N = M.level()
    if verbose>0:
        print "in find_Maassforms_for_one_space. Input:"
        print "Mset,Yset=",Mset,Yset
        print "R1,R2=",R1,R2
        print "dimstart,dimstop,dim",dimstart,dimstop,dim
        print "weight=",weight
        print "force=",force
        print "db=",db
        print "tol=",tol
        print "hecke_Ef=",hecke_ef
        print "sloppy=",sloppy
        print "cusp_evs=",M.cusp_evs()
        print "sym_type=",M.sym_type()
        print "ch=",ch
        print "Hecke eigenfunctions",hecke_ef

#    fname="Gamma0-{0}-ch-{1}.sobj".format(N,ch)
#    try:
#        dic=load(fname)
#    except:
#        dic={}

    # If we have a congruence group we want to check for different types of oldforms
    set_c = []
    if G.is_congruence():
        divisors = ZZ(N).divisors()
        setc = {(0,1):1}  ## Checks for newforms
        for d in divisors:
            setc[(0,d)] = 0
        for d in divisors:            
            set_c.append(deepcopy(setc))
        for i in range(len(divisors)):
            #print "Step {0} set divisor {1}".format(i,divisors[i])
            set_c[i][(0,divisors[i])]=1
        if verbose>0:            
            print "Locating forms ?(old and new) for congruence group with Set_C =",set_c
        dim = len(set_c)
    if dim<>None:
        dimstart=dim; dimstop=dim
    if dimstop-dimstart>1 and verbose>=0:
        print "Checking dim in [",dimstart,",",dimstop,"]"
        ## We check all smaller dimensions too...
    res=[]; l=[]
    # Get oldforms
    if get_oldforms:
        if verbose>0:
            print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GET OLDFORMS"
        l_old = find_oldforms_for_one_space(M,R1,R2,Mset=Mset,Yset=Yset,dimstart=dimstart,dimstop=dimstop,weight=weight,dim=dim,verbose=verbose,force=force,db=db,tol=tol,hecke_ef=hecke_ef,method=method,sloppy=sloppy)
        save_records(l_old,dim=dim,fnr=0,do_par=do_parallel,ncpus=ncpus,db=db)
        res.extend(l_old)

    # Get newforms of
    if get_newforms:
        if verbose>0:
            print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GET NEWFORMS"
        l_new = find_newforms_for_one_space(M,R1,R2,Mset=Mset,Yset=Yset,dimstart=dimstart,dimstop=dimstop,weight=weight,dim=dim,verbose=verbose,force=force,db=db,tol=tol,hecke_ef=hecke_ef,method=method,sloppy=sloppy)
        save_records(l_new,dim=dim,fnr=0,do_par=do_parallel,ncpus=ncpus,db=db)   
        res.extend(l_new)
    return res


def find_oldforms_for_one_space(M,R1,R2,Mset=None,Yset=None,dimstart=1,dimstop=1,weight=0,dim=None,verbose=0,force=False,db=None,tol=1e-10,hecke_ef=False,method='TwoY',sloppy=0):
    r"""
    Try to find oldforms, i.e. Maass forms of the form: f(dz) d | N
    """
    divisors = ZZ(M.group().generalised_level()).divisors()
    set_c = []; setc={}
    for d in divisors:
        setc[(0,d)] = 0
    for d in divisors:            
        set_c.append(deepcopy(setc))
    for i in range(len(divisors)):
        #print "Step {0} set divisor {1}".format(i,divisors[i])
        set_c[i][(0,divisors[i])]=1
    if verbose>0:            
        print "Locating oldforms with Set_C =",set_c
    ## We now try to find forms for all these different normalizations
    ## Note that we try to single out one-dimensional subspaces
    ##
    res=[]; l=[]
    d = len(set_c)
    for cn in range(d):
        if verbose>0:
            print "Current set_c =",cn
        l = get_element_in_range(M,R1,R2,Mset=Mset,Yset=Yset,dim=1,set_c=[set_c[cn]],fnr=0,verbose=verbose,force=force,db=db,tol=tol,hecke_ef=hecke_ef,method=method,sloppy=sloppy)
        if not l:
            continue
        res.extend(l)
    return res

def find_newforms_for_one_space(M,R1,R2,Mset=None,Yset=None,dimstart=1,dimstop=1,weight=0,dim=None,verbose=0,force=False,db=None,tol=1e-12,hecke_ef=False,method='TwoY',sloppy=0):
    r"""
    Try to find newforms, i.e. we assume that we can set c(1)=1 (which may or may not be true for non-congruence groups)
    """
    divisors = ZZ(M.group().generalised_level()).divisors()
    set_c = [{(0,1):1}]
    if verbose>0:            
        print "Locating newforms with Set_C =",set_c
    ## We now try to find forms for all these different normalizations
    ## Note that we try to single out one-dimensional subspaces
    ##
    res=[]; l=[]
    dimstart= 1
    dimstop = 1
    for d in range(dimstart,dimstop+1):
        if verbose>0:
            print "Current dim=",d
        for fnr in range(0,1):
            if verbose>0:
                print "Current function =",fnr
                print "set_c=",set_c
            l = get_element_in_range(M,R1,R2,Mset=Mset,Yset=Yset,dim=d,set_c=[set_c[fnr]],fnr=0,verbose=verbose,force=force,db=db,tol=tol,hecke_ef=hecke_ef,method=method,sloppy=sloppy)
            if not l:
                continue
            res.extend(l)
    return res

def save_records(recs,dim=1,fnr=0,do_par=0,ncpus=1,db=None):
    for rec in recs:
        try:
            R,er,Y,M0  = rec
            print "Res = ",rec,"cusp_evs:",M.cusp_evs()
            if verbose>0:
                print "Checking with dim:",d
        except:
            print "Res = ",rec
            continue
        if RR(R).is_NaN() or RR(R).is_infinity():
            continue
        F=M.get_element(R,dim=dim,do_par=do_parallel,ncpus=ncpus)
        ## Automatically Hecke eigenfun if not otherwise specified.
        if isinstance(F,list):
            F = F[fnr]
        Rst=str(R).split(".")
        Rst=(Rst[0]+"."+Rst[1][0:12])[0:12]  ## Should be enough for unique description
        if G.is_Gamma0():
            fname="{0}/{1}-{2}-{3}-{4}.sobj".format(datadir,N,ch,M.sym_type(),Rst)
        else:
            ## construct a standardized filename (also valid on different file systems)
            grname = str(G.permS.list()).replace(' ','').replace('[','S').replace(']','S').replace(',',"_")
            grname+='-'+str(G.permR.list()).replace(' ','').replace('[','R').replace(']','R').replace(',',"_")
            fname="{0}/{1}-{2}-{3}.sobj".format(datadir,grname,M.sym_type(),Rst)
            save(F,fname)
            if db<>None:
                pass
            else: # simply write to a file
                fname="{0}/{1}-{2}.sobj".format(datadir,grname,M.sym_type())
        try:
            res=load(fname)
        except IOError:
            res={}
        if res.has_key((N,ch)):
            res[(N,ch)].append((R,er,Y,M0))
        else:
            res[(N,ch)]=[(R,er,Y,M0)]
        save(res,fname)
    

def find_Maassforms_for_one_space_old(M,R1,R2,Mset=None,Yset=None,dimstart=1,dimstop=1,weight=0,dim=None,verbose=0,force=False,db=None,tol=1e-12,hecke_ef=False,method='TwoY',sloppy=0):
    #assert m._sym_type in [0,1,-1]
    if verbose>4:
        M._verbose=verbose
    N = M.group().generalised_level()
    G = M.group()
    ch = M._ch
    if not G.is_congruence():
        hecke_ef = False
    if verbose>0:
        print "cusp_evs=",M.cusp_evs()
        print "sym_type=",M.sym_type()
        print "ch=",ch
        print "Hecke eigenfunctions",hecke_ef

    fname="Gamma0-{0}-ch-{1}.sobj".format(N,ch)
    try:
        dic=load(fname)
    except:
        dic={}
    if dim<>None:
        dimstart=dim; dimstop=dim
    if dimstop-dimstart>1 and verbose>=0:
        print "Checking dim in [",dimstart,",",dimstop,"]"
        ## We check all smaller dimensions too...
    res=[]; l=[]
    for d in range(dimstart,dimstop+1):
        if verbose>0:
            print "Current dim=",d
        l = get_element_in_range(M,R1,R2,Mset=Mset,Yset=Yset,dim=d,verbose=verbose,force=force,db=db,tol=tol,hecke_ef=hecke_ef,method=method,sloppy=sloppy)
        if not l:
            continue
        res.append(l)
        for rec in l:
            try:
                R,er,Y,M0  = rec
                print "Res = ",rec,"cusp_evs:",M.cusp_evs()
                if verbose>0:
                    print "Checking with dim:",d
            except:
                print "Res = ",rec
                continue
            if RR(R).is_NaN() or RR(R).is_infinity():
                continue
            F=M.get_element(R,dim=d,do_par=do_parallel,ncpus=ncpus) ## Automatically Hecke eigenfun if not otherwise specified.
            Rst=str(R).split(".")
            Rst=(Rst[0]+"."+Rst[1][0:12])[0:12]  ## Should be enough for unique description
            if G.is_Gamma0():
                fname="{0}/{1}-{2}-{3}-{4}.sobj".format(datadir,N,ch,M.sym_type(),Rst)
            else:
                ## construct a standardized filename (also valid on different file systems)
                grname = str(G.permS.list()).replace(' ','').replace('[','S').replace(']','S').replace(',',"_")
                grname+='-'+str(G.permR.list()).replace(' ','').replace('[','R').replace(']','R').replace(',',"_")
                fname="{0}/{1}-{2}-{3}.sobj".format(datadir,grname,M.sym_type(),Rst)
            save(F,fname)
            if db<>None:
                pass
            else: # simply write to a file
                fname="{0}/{1}-{2}.sobj".format(datadir,grname,M.sym_type())
                try:
                    res=load(fname)
                except IOError:
                    res={}
                if res.has_key((N,ch)):
                    res[(N,ch)].append((R,er,Y,M0))
                else:
                    res[(N,ch)]=[(R,er,Y,M0)]
                save(res,fname)
    return res






def get_element_in_range(H,R1,R2,Mset=0,Yset=0,dim=1,ndigs=12,set_c=[],fnr=0,neps=10,hmax=100,method='TwoY',split=0,get_c=0,verbose=0,maxit=20,force=False,db=None,tol=1e-12,hecke_ef=True,sloppy=0):
    r""" Finds element of the space self with R in the interval R1 and R2

    INPUT:

    - ``R1`` -- lower bound (real)
    - ``R1`` -- upper bound (real)
    - ``method`` -- int - specify which method to use (to be described later...)
    - 'force' -- if set to False we skip intervals we already investigated
    - 'db' -- MaassDB

    """
    # Dummy element
    if hasattr(R1,'prec'):
        RF = RealField(R1.prec())
    else:
        RF = RealField(53)
    R1 = RF(R1); R2=RF(R2)
    F=Maasswaveform(H,R2,dim=dim,compute=False)
    e1 = RR(10)**-RR(neps)
    if verbose>0:
        print "R2,Mset,Yset,ndigs=",R2,Mset,Yset,ndigs
        print "set_c=",set_c
    param=H.set_default_parameters(R2,Mset,Yset,ndigs)
    Y=param['Y']
    Q=param['Q']
    M=param['M']
    if Y==None:
        M = H.smallest_M0()
        Y = get_Y_for_M_dp(H,R2,M,e1)
        if verbose>0:
            print "Recompute Y. Got:{0}".format(Y)
        Q = M+10
    Rl=list()

    if e1<tol and tol<0.5:
        neps=abs(int(log_b(tol,10)))
    if H._verbose==-2:
        P=Graphics()
    if H._verbose>0:
        print "Y=",Y
        print "M=",M
        print "Q=",Q
        print "tol=",tol
    if split==1:
        # Split interval on zeros of the K-Bessel function
        l=H.split_interval(R1,R2)
        if verbose>0:
            print "Split into intervals:"
            print "l=",l
            for [r1,r2,y] in l:
                print "[",r1,",",r2,"]:",y
        for [r1,r2,y] in l:
            [R,er]=find_single_ev(H,r1,r2,Yset=y,neps=neps,method=method,dim=dim,verbose=verbose,maxit=maxit,set_c=set_c,fnr=fnr,hecke_ef=hecke_ef,Mset=Mset,sloppy=sloppy,tol_in=tol)
            if R<>0 and er<>0:
                Rl.append([R,er])
    else:
        r2=0
        y=Y*0.97
        if R1>0:
            r1=R1
        else:
            r1=0.0 # 1.0E-14
            ## And we also check separately if  R=0 is an eigenvalue.
        while r1<R2:
            # Where is the next end point?
            # This is good for large r, but for small?
            h=abs(1.0/H.Weyl_law_Np(r1))
            h=min(h,min(0.5,RR.pi()/r1))
            h=min(h,0.05)
            h=h*0.5
            r2=r1+h
            flogger.info("Using stepsize: h={0}".format(h))
            if db and have_eigenvalue_in(H,r1,r2,neps,db):
                flogger.info("Continuing! We already have an eigenvalue in the database in :[{0} , {1}], y={2}, neps={3}".format(r1,r2,y,neps))
                r1=r2
                continue
            flogger.info("checking :[{0} , {1}], y={2}, neps={3}".format(r1,r2,y,neps))
            try:
                l=find_single_ev(H,r1,r2,Yset=y,neps=neps,dim=dim,hmax=hmax,method=method,verbose=verbose,maxit=maxit,set_c=set_c,fnr=fnr,hecke_ef=hecke_ef,sloppy=sloppy,db=db,tol_in=tol)
            except KeyboardInterrupt:
                return
            flogger.info("l={0}".format(l))
            if H._verbose==-2:
                R,er,pp=l
                P+=pp
            else:
                try:
                    R,er,Y,M=l
                except ValueError,TypeError:
                    #if verbose>0:
                    #print "l=",l
                    R,er,Y,M=-1,0,0,0

            if R >=0 and er>0:  # Remember that a close but non-zero is often recognized by negative value of r
                #if db:
                rec =  insert_maass_form(H,R,er,Y,M,dim,db,insert_coeffs=True)
                flogger.info("inserting: {0}".format(rec))
                Rl.append(rec) #[H._group._level,H.sym_type(),R,er,Y,M])
            else:
                Rl.append(l) ## We always add also bad results in the returned values.
            if r2==r1:
                r1 = r2+1E-14
            else:
                r1=r2
            
    #print "R=",R
    #print "er=",er
    if H._verbose==-2:
        return Rl,P
    else:
        return Rl

def insert_maass_form_from_rec(rec,db,format_only=False,insert_coeffs=False):
   ch = rec.get('character',1)
   dim = rec.get('dim',1)
   R =rec['eigenvalue']
   er = rec.get('error',-1)
   #'group_cusps': [(1, 0)],
   ix = rec.get('group_index')
   p2 = rec.get('group_p2','')
   p3 = rec.get('group_p3','')
   ix,nc,e2,e3,g = rec['group_sign']# : (15, 1, 3, 0, 1),
   M = rec.get('m0',0)
   sym_type=rec.get('symmetry', -1)
   wt = rec.get('weight', 0)
   Y = rec.get('y',0)
   exceptional = rec.get('exceptional',False)
   G = MySubgroup(o2=p2,o3=p3)
   H = MaassWaveForms(G,sym_type=sym_type,weight=wt,exceptional=exceptional)
   insert_maass_form(H,R,er,Y,M,dim,db,format_only=format_only,insert_coeffs=insert_coeffs)  
   
    
def insert_maass_form(H,R,er,Y,M,dim,db,format_only=False,insert_coeffs=False):

    sym_type=H.sym_type()
    cusp_evs=H.cusp_evs()
    G = H.group()
    data={
          'weight':int(H._weight),
          'dim':int(dim),
          'symmetry':int(sym_type),
          'eigenvalue':float(R),
          'error':float(abs(er)),
          'm0':int(M),
          'y':float(Y),
          'exceptional':int(H._exceptional)
          }
    if G.is_Gamma0():
        data['level']=int(H._group._level)
        data['cusp_evs']=cusp_evs
        data['character']=int(H._ch)        
    else:
         data['group_p2']=G.permS.export_as_string()
         data['group_p3']=G.permR.export_as_string()
         data['group_index']=G.index()
         data['group_sign']=G.signature()
         data['character']=int(1)
    data['group_cusps'] =  [(int(x.numerator()),int(x.denominator())) for x in G.cusps()]
    print "insert data:",data
    ## Set the group information
    G = H.group()
    creator = 'Fredrik Stromberg <fredrik314@gmail.com>'
    date = data.get('date','{0}'.format(datetime.datetime.now()))
    date = str(date).split(".")[0]
    algorithm = "Implementation in Psage"
    url = 'http://www.github.com/fredstro/psage'
    mylicense = "CC BY-SA"
    hostname = os.getenv('HOSTNAME')
    if hostname=="" or hostname=="None":
        hostname = socket.gethostname()
    sysinfo = "Sage-Version: {0}, Host: {1}".format(version(),hostname)
    metadata = "Creator: {0}, Date: {1}, Algorithm: {2}, URL: {3}, License:{4}".format(creator,date,algorithm,url,mylicense)
    metadata = metadata+","+sysinfo
    print "metadata=",metadata
    if db<>None:
        #print db.format_record(data,metadata=metadata)
        if format_only==False:
            db.insert_record(data,type='form',metadata=metadata)
        ## We also insert the (small number of) coefficients we may have:
        F = H.get_element(R,Yset=Y,Mset=M,dim=dim,do_par=do_parallel,ncpus=ncpus)
        #F.test()
        if insert_coeffs:
            db.insert_coefficients(data,{'data':F._coeffs,'metadata':metadata})
        return db.format_record(data,metadata=metadata)            

    return data

def have_eigenvalue_in(H,r1,r2,neps,db):
    tol=RR(10)**(1-neps)
    sym_type=H._sym_type
    G = H._group
    if G.is_Gamma0():
        find_data={'Level':int(H._group._level),
                   'Weight':int(H._weight),
                   'Character':int(H._ch),
                   'Symmetry':int(sym_type),
                   'Eigenvalue':{"$gte": float(r1)-float(tol),"$lt":float(r2)+float(tol)}}
    else:
        find_data = {'group_p2' : G.permS.export_as_string(),
                     'group_p3' : G.permR.export_as_string(),
                     'group_index' : G.index(),
                     'group_sign' : G.signature()}
    f=db.find(find_data)
    if (isinstance(f,list) and len(f)==0) or f.count()==0:
        return False
    ff=f.next()
    if ff.get('Error',0)>0 and ff.get('Error',0)<tol:
        return True
    else:
        return False

def find_single_ev(S,R1in,R2in,Yset=None,neps=10,method='TwoY',dim=1,verbose=None,set_c=[],fnr=0,get_c=0,hmax=100,maxit=20,db=None,hecke_ef=True,Mset=None,sloppy=0,tol_in=None):
    i=0
    if verbose==None:
        verbose=S._verbose
    else:
        verbose=verbose
    #print "verbose=",verbose
    #Mset=None
    mi=0
    R1=R1in; R3=R2in
    # We should only need to decrease Y a couple of times
    # We do it 5 times with the same M and then change M
    # and decrease Y 5 more times.
    # if this doesn't work we give up...
    #flogger.info("find single: set_c={0}".format(set_c))
    M0=0; Y0=0; message=''
    l = None
    while i<10:
        try:
            l=find_single_ev_1(S,R1,R3,Yset=Yset,Mset=Mset,neps=neps,
                               method=method,dim=dim,hmax=hmax,
                               verbose=verbose,get_c=get_c,maxit=maxit,set_c=set_c,fnr=fnr,hecke_ef=hecke_ef,sloppy=sloppy,tol_in=tol_in)
        #except KeyError:
        #    pass
        except ArithmeticError as aritherr:
            message=str(aritherr)
            flogger.critical("Arithmetic error! {0}".format(message))
            if 'Need smaller Y' in message:
                flogger.critical("We continue and change parameters!")
                Y = float(message.split('than')[-1])
                l = -1,R1,R3,Y,Mset
        if l<>None and isinstance(l,(list,tuple)):
            if l[0]==-1:
                i=i+1

                R1old=l[1]; R3old=l[2]
                Yset=l[3]*0.9
                M0=Mset
                Y0=Yset
                if mi<=5:
                    Mset=l[4]; mi+=1
                else:
                    Mset=None
                    mi=0
                if R1old>R1in and R1old<R2in:
                    R1=R1old
                if R3old>R1in and R3old<R2in:
                    R3=R3old
                flogger.debug("decreasing Y:{0}".format(Yset))
                flogger.debug("keeping M:{0}".format(Mset))
                flogger.debug("checking interval:[{0},{1}]".format(R1,R3))

            else:
                return l
        elif l<>None:
            return l
        else:
            return 0,0,0,0
    if M0==None:
        M0=0
    if (not hasattr(S._group,'level')) or (not hasattr(db,'put_problem_mongo')):
        flogger.info("Location failed at R1,R3,M0,Y0:{0}".format(R1,R3,M0,Y0))
        return 0,0,0,0
    data={'Level':int(S._group.level()),
          'Weight':int(S._weight),
          'Char':int(S._ch),
          'Dim':int(dim),
          'Symmetry':int(S._sym_type),
          'R1':float(R1),
          'R2':float(R3),
          'M0':int(M0),
          'Y':float(Y0),
          'Cusp_evs':S._cusp_evs #mongify(S._cusp_evs)
          }
    #try:
    #    #mongify(1)
    #    data = mongify(data)
    #except NameError:
    #    pass
    flogger.info("Location failed and we register a problem:{0}".format(data))
    if message=='':
        data['message']='Location routine failed!'
    else:
        data['message']='Location routine failed! '+message
    if hasattr(db,"put_problem_mongo@"):
        db.put_problem_mongo(data)
    else:
        print data['message']


def find_single_ev_1(S,R1in,R3in,Yset=None,Mset=None,neps=10,method='TwoY',verbose=0,dim=1,hmax=100,get_c=0,set_c=[],fnr=0,maxit=20,hecke_ef=False,sloppy=0,tol_in=None):
    r""" Locate a single eigenvalue on G between R1 and R2

    INPUT:(tentative)

    .- ''S''    -- space of Maass waveforms
    - ''R1in'' -- real
    - ''R1in'' -- real
    - ''Yset'' -- real (use this value of Y to compute coefficients)
    - ''neps'' -- number of desired digits
    - ''method'' -- 'TwoY' or 'Hecke'
    - ''verbose'' -- integer (default 0) level of verbosity
    - ''dim''  -- integer (default 1) look for space of this dimension
    - ''hmax'' -- integer (default 15) parameter which sets how large 'too large' derivative is
    - ''get_c'' -- integer. (default 0) set to 1 if you want to return coefficients.
    - ''maxit'' -- integer (default 20,) number of iterations before returning with an error message
    - ''hecke_ef'' -- logical (default False) if set to True we check that for Hecke eigenforms.
                             (Note: this increases the necessary precision)

    OUPUT:

    - ''R'' --


    """
    G=S.group()
    jmax=1000  # maximal number of interation
    if neps>=15:
        prec = ceil(neps*3.5)
    else:
        prec = 53
    RF = RealField(prec)
    R1in = RF(R1in)
    R3in = RF(R3in)
    R1v = [RF(R1in)]; R3v = [RF(R3in)]
    #R1=mpmath.mp.mpf(R1in);R3=mpmath.mp.mpf(R2in)
    if S._verbose>0 or verbose>0:
        flogger.setLevel(10)
        #print "mpmath.mp.dps=",mpmath.mp.dps
    flogger.debug("prec={0}".format(prec))
    flogger.debug("neps={0}".format(neps))    
    flogger.debug("Yset={0}".format(Yset))
    flogger.debug("R1={0} of type {1}".format(R1in,type(R1in)))
    flogger.debug("R3={0} of type {1}".format(R3in,type(R3in)))
    flogger.debug("Finding funciton nr. {0}".format(fnr))
    half=RF(0.5)
    if tol_in is None:
        tol=RF(2)**RF(10-prec)
    else:
        tol = tol_in
    tol = RF(tol)
    tol_z=tol.sqrt()  ## When we check for zeros we are more relaxed
    #[Y,M]=find_Y_and_M(G,R1,neps,Yset=Yset,Mset=Mset)
    M = S.smallest_M0()
    #Y = get_Y_for_M(S,R2in,M,tol)
    flogger.debug("R3in={0}, min hieight: {1}, M={2}, tol={3}".format(R3in,S.group().minimal_height(),M,tol))
    
    M,Y=get_M_and_Y(R3in,S.group().minimal_height(),M,tol)
    flogger.debug("Got M={0} and Y={1}".format(M,Y))
    # start slightly lower?
    #if Y<0: # If the required precision needs a larger M
    #    Y,M = get_Y_and_M_dp(S,R2in,tol)
    if Y<0:
        raise ArithmeticError,"Can not find Y for this precision!"
    if Yset<>None:
        if Yset < Y and Yset>0:
            Y = Yset
    Y = RF(Y)
    if Mset and Mset>0:
        M=Mset
    #dold=prec ## mpmath.mp.dps
    #mpmath.mp.dps=neps+3  # We work with low precision initially
    #R1old=R1; R3old=R3
    signs=dict();diffs=dict()
    c=dict(); h=dict()
    is_trivial=S.multiplier().is_trivial()
    st = S.sym_type()
    if not is_trivial:
        x=S.multiplier().character()
        mod=x.modulus()
    else:
        mod=S.group().generalised_level()
    flogger.debug("Computing Maass form wirth these paramters: R={0}, dim={1} and set_C={2}".format(R1in,dim,set_c))
    F=S.get_element(R1in,Yset=Y,Mset=M,dim=dim,set_c=set_c,phase2=False)
    if isinstance(F,list):
        F = F[fnr]
    if method=='TwoY':
        #if dim==1:
        #    c[1]=2 ; c[2]=3 ; c[3]=4
        #else:
        usei=1
        if len(set_c)>fnr:
            setc = set_c[fnr]
            for (ci,n) in setc:
                if setc[(ci,n)]<>0:
                    for i in range(2,100):
                        if (0,i) in setc:
                            continue
                        if i % n == 0:  # We might want to use this                    
                            if abs(F.C(0,i))>tol**0.5:
                                if verbose>0:
                                    flogger.debug("Using these coeffs: F.C(0,{0})={1}".format(i,F.C(0,i)))
                                c[usei] = i
                                usei+=1
                        if usei>3:
                            break
                if usei>3:
                    break
                
        if usei < 3:
            for i in range(dim+1,100):
                if gcd(i,mod)>1:
                    continue
                cont = 0
                if len(set_c)>fnr:
                    setc = set_c[fnr]
                    if (0,i) in setc:
                        cont = 1                        
                if i>M-2:
                    cont = 1
                if cont:
                    continue
                c[usei]=i
                usei+=1
                if usei>3:
                    break

    elif method=='Hecke':
        usei=1
        a = S.get_primitive_p()
        b = S.get_primitive_p(a)
        c[1]=a; c[2]=b; c[3]=a*b
        if M < c[3]+10:
            [Y,M]=find_Y_and_M(G,R1in,neps,Yset=Yset,Mset=c[3]+10)
        for i in range(dim+1,100):
            if gcd(i,mod)>1:
                continue
            if usei==2 and gcd(i,c[1])>1:
                continue
            c[usei]=i
            usei+=1
            if usei>2:
                break
        c[3]=c[1]*c[2]
                                                  
    flogger.info("+++++++++++++++++++++++ Start find_single_ev_1 +++++++++++++++++++++++++++++++++++++++++++++")
    flogger.info("R1,R3={0},{1}".format(R1in,R3in))
    flogger.debug("tol={0}\t tol_z={1}".format(tol, tol_z))
    flogger.debug("Y={0} \t M={1}".format(Y,M))
    flogger.debug("level={0}".format(S._group._level)+"dim={0}".format(dim)+' sym_type={0}'.format(S._sym_type)+" ch={0}".format(S._ch))
    flogger.debug("cusp_evs={0}".format(S._cusp_evs))
    flogger.debug("c={0}".format(c))

    Y1=Y*RF(0.995); Y2=RF(0.995)*Y1
    met=method
    [diffs[1 ],h[1 ]]=functional(S,R1in,M,Y1,Y2,signs,c,first_time=True,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
    if R1in == R3in:
        errest = abs(h[1])
        if errest < tol:
            return R1in,errest,Y1,M
        else:
            return 0,0,0,Y,M
    [diffs[3 ],h[3 ]]=functional(S,R3in,M,Y1,Y2,signs,c,first_time=True,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
    for j in [1,2,3]:
        flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
    #t1=(diffs[3][1]-diffs[1][1])/(R3-R1)
    #if met=='TwoY':
    #    t2=(diffs[3][2]-diffs[1][2])/(R3-R1)
    #    t3=(diffs[3][3]-diffs[1][3])/(R3-R1)
    #else:
    #    t2=0; t3=0
    flogger.debug("diffs: method={0}".format(met))
    flogger.debug("Y1={0}".format(Y1)+" Y2={0}".format(Y2)+" M={0}".format(M))
    for j in [1,2,3]:
        flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
    flogger.debug("h={0}".format(h))
    flogger.debug("R1={0},  R3={1}".format(R1in,R3in))
        #flogger.debug("deriv. approx:1:{0}".format(t1)
        #flogger.debug("deriv. approx:2:{0}".format(t2)
        #flogger.debug("deriv. approx:3:{0}".format(t3)
    if verbose>1:
        for n in list(c.keys()): #.sort():
            for j in list(diffs.keys()): #.sort():
                if diffs[j].has_key(n):
                    flogger.debug("diff[{0}][{1}]={2}".format(j,c[n],diffs[j][n]))
    ### Checking results
    if abs(h[1])>hmax or abs(h[3])>hmax:
        if verbose>0:
            flogger.debug("too large h1 or h3:{0}".format(h))
        return -1,R1in,R3in,Y,M
    if is_derivative_large(R1in,R3in,diffs[1],diffs[3],met,hmax,tol,verbose):
        return -1,R1in,R3in,Y,M
    #if max( map(abs,[t1,t2,t3]))>hmax:
    #    if verbose>0:
    #        flogger.debug("too large derivative:{0}".format(h))
    #    return -1,Y
    add_plot=0
    if verbose==-2:
        add_plot=1
        P = Graphics()
        #fp=open("testplot.txt{0}".format("write")
        lopts={'thickness':1}
        l=line([(R1,h[1]),(R3,h[3])],**lopts)
        P+=l

    # Sset signs and check zeros
    if met=='Hecke' and dim==1:
        if h[1]*h[3]>sqrt(tol):
            # We do not have a signchange
            if add_plot==1:
                return 0 ,0,P
            else:
                return 0 ,0,0,0
        else:
            if h[1]>0:
                signs[1]=1
            else:
                signs[1]=-1
    # Begin by trying to find the 'direction' of the crossing
    var=0.0
    for j in range(1 ,3 +1 ):
        var+=abs(diffs[1 ].get(j,0))+abs(diffs[3 ].get(j,0))
    # In the first step we want to catch more more sign changes so we relax the conditions
    # Since the precision will in general not be fine to have sign changes in all
    # coefficients we are satisfied with two out of three
    nsgn=0
    for j in range(1 ,3 +1 ):
        signs[j]=1
        if not diffs[1].has_key(j):
            continue
        if diffs[1 ][j]*diffs[3][j]<tol:
            nsgn+=1
    if nsgn<1:
        # If we did not have any sign change then we probably don't have a zero
        # at least, if the relative size is large
        if sum([abs(diffs[1][j])+abs(diffs[3][j]) for j in range(1,4)]) > 0.04*var:
            flogger.debug("No zero here!")
            flogger.debug("var={0}".format(var))
            flogger.debug("diffs[{0}]={1} - {2}".format(j,abs(diffs[1][j]),abs(diffs[3][j])))
            if add_plot==1:
                return 0,0,0 ,0,P
            else:
                return 0,0 ,0,Y,M
    flogger.debug("Number of sign changes={0}".format(nsgn))

    for j in range(1,4):
        if not diffs[1].has_key(j):
            continue
        if diffs[1 ][j]*diffs[3 ][j]<tol:
            if diffs[1 ][j]>0:
                signs[j]=-1
            else:
                signs[j]=1
        else:
            if abs(diffs[1][j])>abs(diffs[3][j]):
                signs[j]=-1
            else:
                signs[j]=1
    # Recompute functionals using the signs
    flogger.debug("Making preliminary search")
    flogger.debug(" R1,R3={0}".format((R1in,R3in)))
    flogger.debug(" h = {0}".format(h))
    for j in [1,2,3]:
        flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
    flogger.debug(" signs={0}".format(signs))
    for k in [1,3]:
        h[k]=0
        for j in range(1,3+1):
            if not diffs[k].has_key(j):
                continue
            h[k]=h[k]+signs[j]*diffs[k][j]
    Rnew=prediction(h[1 ],h[3 ],R1in,R3in)
    Rnew_old=Rnew ## Keep track of the previous estimate
    flogger.debug(" new h={0}".format(h))
    flogger.debug(" Prediction 1={0}".format(prediction(diffs[1 ][1],diffs[3 ][1],R1v[0],R3v[0])))
    if diffs[1].has_key(2):
        flogger.debug(" Prediction 2={0}".format(prediction(diffs[1 ][2],diffs[3 ][2],R1v[0],R3v[0])))
    if diffs[1].has_key(3):
        flogger.debug(" Prediction 3={0}".format(prediction(diffs[1 ][3],diffs[3 ][3],R1v[0],R3v[0])))
    flogger.debug(" tol={0}".format(tol))
    flogger.debug(" Rnew={0}".format(Rnew))
    flogger.debug(" R3in+2tol={0}".format(R3in+2*tol))
    flogger.debug(" R1in-2tol={0}".format(R1in-2*tol))
    if Rnew > R3in+2*tol or Rnew<R1in-2*tol:
        ## Try to take an average prediction instead
        Rnew = sum([prediction(diffs[1 ][j],diffs[3 ][j],R1in,R3in) for j in range(1,4)])
        Rnew = Rnew/RF(3)
        flogger.debug("Try a new Rnew = {0}".format(Rnew))
        if Rnew > R3in+2*tol or Rnew<R1in-2*tol:
            return 0,0,0,Y,M

    [diffs[2],h[2]]=functional(S,Rnew,M,Y1,Y2,signs,c,first_time=False,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
    flogger.debug("R1,Rnew,R3={0}".format((R1in,Rnew,R3in)))
    flogger.debug("h={0}".format(h))
    ## We use two different error estimates: errest_x and errest_h
    ## estimated from values of x and of h(x) respectively
    ### Checking results
    if abs(h[2])>hmax and sloppy>1:
        if verbose>0:
            flogger.debug("got too large h 1={0}".format(h))
        return -1,R1in,R3in,Y,M
    zero_in=is_zero_in(st,h,diffs,tol_z,verbose)
    if zero_in == -1:
        R3v.insert(0,Rnew); R1v.insert(0,R1v[0]); h[3]=h[2 ]; diffs[3 ]=diffs[2 ]; errest_x=abs(Rnew-R1v[0])
        if add_plot==1:
            P+=line([(R1,h[1]),(R3,h[3])],**lopts)
    else:
        R1v.insert(0,Rnew); R3v.insert(0,R3v[0]); h[1 ]=h[2 ]; diffs[1 ]=diffs[2 ]; errest_x=abs(Rnew-R3v[0])
        if add_plot==1:
            P+=line([(R1v[0],h[1]),(R3v[0],h[3])],**lopts)
    step=0
    if is_derivative_large(R1v[0],R3v[0],diffs[1],diffs[3],met,hmax,tol_z,verbose) and sloppy>1:
        return -1,R1v[0],R3v[0],Y,M
    errest_x_old=errest_x
    errest_h = sum(map(abs,h.values()))/3.0
    errest_h_old = errest_h
    #R1v=[R1]; R3v=[R3]
    for j in range(maxit):
        flogger.info(" ----------------- step: {0} -----------------------".format(j))
        Rnew_old=Rnew
        Rnew=prediction(h[1 ],h[3 ],R1v[0],R3v[0])
        [diffs[2 ],h[2 ]]=functional(S,Rnew,M,Y1,Y2,signs,c,first_time=False,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
        errest_h_old = errest_h; errest_x_old = errest_x
        errest_h = abs(h[2]) #sum(map(abs,h.values()))/3.0
        if method=='Hecke' and diffs[1].has_key(-1):
            errest_h = max([errest_h,abs(diffs[1][-1]),abs(diffs[3][-1]),abs(diffs[2][-1])])
            flogger.debug("R1,R3v[0],Rnew,errest_h,tol={0}".format((R1v[0],R3v[0],Rnew,errest_h,tol)))

        ## The prediction tries to find zero but now we see how close we actually were:
        Rnew1=prediction(h[1 ],h[2 ],R1v[0],Rnew)
        Rnew2=prediction(h[2 ],h[3 ],Rnew,R3v[0])            
        flogger.debug("Rnew_1 ={0}".format(abs(Rnew1)))
        flogger.debug("Rnew_2 ={0}".format(abs(Rnew2)))
        flogger.debug("Rnew_1-Rnew ={0}".format(abs(Rnew1-Rnew)))
        flogger.debug("Rnew_2-Rnew {0}=".format(abs(Rnew2-Rnew)))        
        errest_x = abs(R1v[0]-R3v[0]) ## Too strict?
        errest_x = max( abs(Rnew1-Rnew),abs(Rnew2-Rnew))
        flogger.debug("[R1,R3] = [{0},{1}".format(R1v[0],R3v[0]))
        flogger.debug("\t Rnew_old = {0}".format(Rnew_old))
        flogger.debug("\t Rnew_new(pred) = {0}".format(Rnew))
        flogger.debug("Rnew - Rnew_old".format(abs(Rnew-Rnew_old)))
        flogger.debug("\t h={0}".format(h))
        flogger.debug("\t R1[0],R3[0]={0}".format((R1v[0],R3v[0])))
        for ii in range(len(R1v)):
            flogger.debug("\t R1[{0}],R3[{0}]={1},{2}".format(ii,R1v[ii],R3v[ii]))        
        flogger.debug("Current errest_x={0}".format(errest_x))
        flogger.debug("Current errest_h={0}".format(errest_h))        
        flogger.debug("Old errest_x={0}".format(errest_x_old))
        flogger.debug("Old errest_h={0}".format(errest_h_old))
        errest = max(errest_x,errest_h)
        
        for j in [1,2,3]:
            flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
        flogger.debug("h={0}".format(h))
        
          
        # Check if we landed outside previous interval
        if Rnew > R3v[0] or Rnew<R1v[0]:
            flogger.debug("Rnew is outside the previous interval!")
            ## Try to take one of the other predictions instead
            #return -1,R1v[1],R3v[1],Y,M
            try: 
                for j in range(1,4):
                    Rnew = prediction(diffs[1 ][j],diffs[3 ][j],R1v[0],R3v[0])
                    flogger.debug("Try new Rnew = Prediction({0}) = {1}".format(j,Rnew))
                    if Rnew < R3v[0] and Rnew>R1v[0]:
                        raise StopIteration
                flogger.debug("We couldn't find any prediction inside the interval!")
                return -1,R1v[1],R3v[1],Y,M
            except StopIteration:
                pass
        ###
        ### Checks for errors in computation or parameter choices.
        ### We return signals that new parameters need to be used.
        ### 
        ## See if the derivative has been too large:
        if is_derivative_large(R1v[0],Rnew,diffs[1],diffs[2],met,hmax,tol_z,verbose) and sloppy >1:
            flogger.debug("The derivative in [R1,Rnew] is too large!")
            return -1,R1v[1],R3v[1],Y,M
        if is_derivative_large(Rnew,R3v[0],diffs[2],diffs[3],met,hmax,tol_z,verbose) and sloppy >1:
            flogger.debug("The derivative in [Rnew,R3] is too large!")
            return -1,R1v[1],R3v[1],Y,M
        ## See if the functional value is too large
        if abs(h[2])>hmax:
            return -1,R1v[1],R3v[1],Y,M
        # Check if we got larger error estimate in x or h (shouldn't happen)
        if errest_x > errest_x_old or errest_h > errest_h_old:
            if verbose>0:
                flogger.debug("Got larger error estimate: {0} > {1}".format(errest_x,errest_x_old))
                flogger.debug("Got larger error estimate: {0} > {1}".format(errest_h,errest_h_old))
            return -1,R1v[1],R3v[1],Y,M
        # Check if the R doesn't move but we have too large functional values. 
        # Normally the funciton is much smaller.
        if errest_x < tol and errest_h > 10*tol:
            return -1,R1v[1],R3v[1],Y,M

        # Check if we are done
        if errest < tol:  ## Strict
            flogger.debug("Error estimate is {0} < tol= {1}".format(errest,tol))
            if add_plot==1:
                return Rnew,errest,P
            else:
                return Rnew,errest,Y,M
      
        # We now see if we can find a zero in [R1,Rnew] or [R2,Rnew]      
        zero_in=is_zero_in(st,h,diffs,tol_z,verbose)

        flogger.debug("zero_in = {0}".format(zero_in))       
        if zero_in==0 and abs(errest)<tol:
            if add_plot==1:
                return Rnew,errest,P
            return Rnew,errest,Y,M
        elif zero_in not in [1,-1]:
            if verbose>0:
                flogger.debug("No zero! Breaking!")
            return Rnew,-errest,Y,M
            #flogger.debug("break!!!!"
            #    break #raise StopIteration()
        stepz={}
        if zero_in==-1:
            flogger.debug("There is a zero in [R1,Rnew]={0}".format((R1v[0],Rnew)))
            stepz[0]=abs(Rnew-R3v[0]); stepz[1]=abs(R1v[0]-R3v[0])            
            errest_x_old = errest_x
            if R1v[0] < Rnew:
                R3v.insert(0,Rnew); R1v.insert(0,R1v[0]); h[3 ]=h[2 ]; diffs[3 ]=diffs[2 ]
            errest_x=abs(Rnew-R1v[0]) # strict error estimate
            if hecke_ef:
                errest_h = max(errest_h,diffs[2][-1])

        elif zero_in==1:
            flogger.debug("There is a zero in [Rnew,R3]={0}".format((Rnew,R3v[0])))
            stepz[0]=abs(Rnew-R1v[0]); stepz[1]=abs(R1v[0]-R3v[0])                        
            errest_x_old = errest_x
            if R3v[0] > Rnew:
                R1v.insert(0,Rnew); R3v.insert(0,R3v[0]); h[1 ]=h[2 ]; diffs[1 ]=diffs[2 ]            
            errest_x=abs(Rnew-R3v[0]) # strict error estimate
            if hecke_ef:
                errest_h = max(errest_h,diffs[2][-1])
        # Check if the error estimate in x gets worse (shouldn't happen)
        if 2*errest_x_old < errest_x and errest_x_old<1e-10:
            flogger.warning("New error estimate in x {0} is larger than previous {1}. Improve parameters!".format(errest_x,errest_x_old))
            return -1,R1v[1],R3v[1],Y,M
        if add_plot==1:
            P+=line([(R1v[0],h[1]),(R3v[0],h[3])],**lopts)
        # If we have gone in the same direction too many times we need to modify our approach
        step=step+zero_in
        flogger.debug("step={0}".format(step))
        if step>2 or step < -2:    # Have changed the left end point too many times. Need to change the right also.
            ss = sign(step)
            if step > 2: 
                flogger.debug("Have moved the left end point too many times!")
            else:
                flogger.debug("Have moved the right end point too many times!")
            ## continue to decrease R3 as long as we detect a zero.
            try:
                for zj in range(2):
                    for jj in range(1,100):
                        Rtest=Rnew +ss*half**jj*stepz[zj]  # Need to test if this modified R3 work:
                        if step > 2:
                            flogger.debug("Rtest1(R){0}={1}".format(jj,Rtest))
                        else:
                            flogger.debug("Rtest1(L){0}={1}".format(jj,Rtest))
                        [diffs[2 ],h[2 ]]=functional(S,Rtest,M,Y1,Y2,signs,c,False,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
                        flogger.debug("h={0}".format(h))
                        for j in [1,2,3]:
                            flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
                        flogger.debug("R1,Rtest,R3={0}".format((R1v[0],Rtest,R3v[0])))
                        if is_derivative_large(R1v[0],Rtest,diffs[1],diffs[2],met,hmax,tol_z,verbose) and sloppy>1:
                            return -1,R1v[1],R3v[1],Y,M
                        if abs(h[2])>hmax:
                            return -1,R1v[1],R3v[1],Y,M
                        t = is_zero_in(st,h,diffs,tol_z,verbose)
                        flogger.debug("zero in:{0}".format(t))
                        if t == -1: # have zero in [R1v[0],Rtest]
                            R3v.insert(0,Rtest); R1v.insert(0,R1v[0]); h[3]=h[2]; diffs[3]=diffs[2]; step=step-2
                            flogger.debug("Set R3: R1,R3={0}".format((R1v[0],R3v[0])))
                        elif t==1: # have zero in [Rtest,R3v[0]]
                            R1v.insert(0,Rtest); R3v.insert(0,R3v[0]); h[1]=h[2]; diffs[1]=diffs[2]; step=step+2
                            flogger.debug("Set R1: R1,R3={0},{1}".format(R1v[0],R3v[0]))
                        else:
                            continue
                        # Check if we caught the zero or if we jumed too far
                        step = 0
                        if ss*t > 0:
                            flogger.debug("Get back to main loop!")
                            raise StopIteration
                        elif ss*t < 0:
                            flogger.debug("Get back to main loop with new (good) prediction!")
                            Rnew = Rtest
                            raise StopIteration
                        
                        # Also test if by now the difference at R3 is small enough
                        #i,er = is_minimum_at(h,diffs,tol_z,verbose)
                        #flogger.debug("i,er={0},{1}".format(i,er))
                        #if er<tol and errest_x < tol:
                        #    if i==1:
                        #        return R1v[0],er,Y,M
                        #    if i==2:
                        #        return Rtest,er,Y,M
                        #    if i==3:
                        #        return R3v[0],er,Y,M
                        if t == 1:
                            flogger.debug("Continue to move the left endpoint again!")
                        else:
                            flogger.debug("Continue to move the right endpoint again!")
                        
            except StopIteration:
                pass
            if add_plot==1:
                P+=line([(R1v[0],h[1]),(R3v[0],h[3])],**lopts)
            
    errest=min(errest_x,errest_h)
    if j>=maxit-1:
        flogger.info("Warning: may be problems in the locating function!")
        return Rnew,-errest,Y,M
    elif errest>0 and errest<1.0:
        return Rnew_old,-errest,Y,M
        #raise ArithmeticError
####


def find_single_ev_2(S,R1in,R2in,Yset=None,Mset=None,neps=10,method='TwoY',verbose=0,dim=1,hmax=100,get_c=0,set_c=[],fnr=0,maxit=20,hecke_ef=False,sloppy=0):
    r""" Locate a single eigenvalue on G between R1 and R2

    INPUT:(tentative)

    .- ''S''    -- space of Maass waveforms
    - ''R1in'' -- real
    - ''R1in'' -- real
    - ''Yset'' -- real (use this value of Y to compute coefficients)
    - ''neps'' -- number of desired digits
    - ''method'' -- 'TwoY' or 'Hecke'
    - ''verbose'' -- integer (default 0) level of verbosity
    - ''dim''  -- integer (default 1) look for space of this dimension
    - ''hmax'' -- integer (default 15) parameter which sets how large 'too large' derivative is
    - ''get_c'' -- integer. (default 0) set to 1 if you want to return coefficients.
    - ''maxit'' -- integer (default 20,) number of iterations before returning with an error message
    - ''hecke_ef'' -- logical (default False) if set to True we check that for Hecke eigenforms.
                             (Note: this increases the necessary precision)

    OUPUT:

    - ''R'' --


    """
    G=S.group()
    jmax=1000  # maximal number of interation
    if neps>=15:
        prec = ceil(neps*3.5)
    else:
        prec = 53
    RF = RealField(prec)
    R1 = RF(R1in); R3 = RF(R2in)
    #R1=mpmath.mp.mpf(R1in);R3=mpmath.mp.mpf(R2in)
    if S._verbose>0:
        flogger.setLevel(10)
        #print "mpmath.mp.dps=",mpmath.mp.dps
    flogger.debug("prec={0}".format(prec))
    flogger.debug("Yset={0}".format(Yset))
    flogger.debug("R1={0} of type {1}".format(R1,type(R1)))
    flogger.debug("R3={0} of type {1}".format(R3,type(R3)))
    flogger.debug("Finding funciton nr. {0}".format(fnr))
    half=RF(0.5)
    tol=RF(2)**RF(8-prec)
    tol_z=tol.sqrt()  ## When we check for zeros we are more relaxed
    #[Y,M]=find_Y_and_M(G,R1,neps,Yset=Yset,Mset=Mset)
    M = S.smallest_M0()
    #Y = get_Y_for_M(S,R2in,M,tol)
    M,Y=get_M_and_Y(R2in,S.group().minimal_height(),M,tol)
    # start slightly lower?
    #if Y<0: # If the required precision needs a larger M
    #    Y,M = get_Y_and_M_dp(S,R2in,tol)
    if Y<0:
        raise ArithmeticError,"Can not find Y for this precision!"
    if Yset<>None:
        if Yset < Y and Yset>0:
            Y = Yset
    Y = RF(Y)
    if Mset and Mset>0:
        M=Mset
    #dold=prec ## mpmath.mp.dps
    #mpmath.mp.dps=neps+3  # We work with low precision initially
    R1old=R1; R3old=R3
    signs=dict();diffs=dict()
    c=dict(); h=dict()
    is_trivial=S.multiplier().is_trivial()
    st = S.sym_type()
    if not is_trivial:
        x=S.multiplier().character()
        mod=x.modulus()
    else:
        mod=S.group().generalised_level()
    F=S.get_element(R1in,dim=dim,set_c=set_c)
    if isinstance(F,list):
        F = F[fnr]
    if method=='TwoY':
        #if dim==1:
        #    c[1]=2 ; c[2]=3 ; c[3]=4
        #else:
        usei=1
        if len(set_c)>fnr:
            setc = set_c[fnr]
            for (ci,n) in setc:
                if setc[(ci,n)]<>0:
                    for i in range(2,100):
                        if (0,i) in setc:
                            continue
                        if i % n == 0:  # We might want to use this                    
                            if abs(F.C(0,i))>tol**0.5:
                                if verbose>0:
                                    flogger.debug("Using these coeffs: F.C(0,{0})={1}".format(i,F.C(0,i)))
                                c[usei] = i
                                usei+=1
                        if usei>3:
                            break
                if usei>3:
                    break
                
        if usei < 3:
            for i in range(dim+1,100):
                if gcd(i,mod)>1:
                    continue
                cont = 0
                if len(set_c)>fnr:
                    setc = set_c[fnr]
                    if (0,i) in setc:
                        cont = 1                        
                if i>M-2:
                    cont = 1
                if cont:
                    continue
                c[usei]=i
                usei+=1
                if usei>3:
                    break

    elif method=='Hecke':
        usei=1
        a = S.get_primitive_p()
        b = S.get_primitive_p(a)
        c[1]=a; c[2]=b; c[3]=a*b
        if M < c[3]+10:
            [Y,M]=find_Y_and_M(G,R1,neps,Yset=Yset,Mset=c[3]+10)
        for i in range(dim+1,100):
            if gcd(i,mod)>1:
                continue
            if usei==2 and gcd(i,c[1])>1:
                continue
            c[usei]=i
            usei+=1
            if usei>2:
                break
        c[3]=c[1]*c[2]
                                                  
    flogger.info("+++++++++++++++++++++++ Start find_single_ev_1 +++++++++++++++++++++++++++++++++++++++++++++")
    flogger.debug("tol={0}\t tol_z={1}".format(tol, tol_z))
    flogger.debug("Y={0} \t M={1}".format(Y,M))
    flogger.debug("level={0}".format(S._group._level)+"dim={0}".format(dim)+' sym_type={0}'.format(S._sym_type)+" ch={0}".format(S._ch))
    flogger.debug("cusp_evs={0}".format(S._cusp_evs))
    flogger.debug("c={0}".format(c))
    #c[0]=dim+1 ; c/[1]=dim+2 ; c[2]=dim+3
    #c[1]=2 ; c[2 ]=3 ; c[3 ]=4
    #met='Hecke
    #flogger.debug("find single_ev_1: set_c={0}".format(set_c
    Y1=Y*RF(0.995); Y2=RF(0.995)*Y1
    met=method
    [diffs[1 ],h[1 ]]=functional(S,R1,M,Y1,Y2,signs,c,first_time=True,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
    [diffs[3 ],h[3 ]]=functional(S,R3,M,Y1,Y2,signs,c,first_time=True,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
    for j in [1,2,3]:
        flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
    #t1=(diffs[3][1]-diffs[1][1])/(R3-R1)
    #if met=='TwoY':
    #    t2=(diffs[3][2]-diffs[1][2])/(R3-R1)
    #    t3=(diffs[3][3]-diffs[1][3])/(R3-R1)
    #else:
    #    t2=0; t3=0
    flogger.debug("diffs: method={0}".format(met))
    flogger.debug("Y1={0}".format(Y1)+" Y2={0}".format(Y2)+" M={0}".format(M))
    for j in [1,2,3]:
        flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
    flogger.debug("h={0}".format(h))
    flogger.debug("R1={0},  R3={1}".format(R1,R3))
        #flogger.debug("deriv. approx:1:{0}".format(t1)
        #flogger.debug("deriv. approx:2:{0}".format(t2)
        #flogger.debug("deriv. approx:3:{0}".format(t3)
    if verbose>1:
        for n in list(c.keys()): #.sort():
            for j in list(diffs.keys()): #.sort():
                if diffs[j].has_key(n):
                    flogger.debug("diff[{0}][{1}]={2}".format(j,c[n],diffs[j][n]))
    ### Checking results
    if abs(h[1])>hmax or abs(h[3])>hmax:
        if verbose>0:
            flogger.debug("too large h1 or h3:{0}".format(h))
        return -1,R1old,R3old,Y,M
    if is_derivative_large(R1,R3,diffs[1],diffs[3],met,hmax,tol,verbose):
        return -1,R1old,R3old,Y,M
    #if max( map(abs,[t1,t2,t3]))>hmax:
    #    if verbose>0:
    #        flogger.debug("too large derivative:{0}".format(h))
    #    return -1,Y
    add_plot=0
    if verbose==-2:
        add_plot=1
        P = Graphics()
        #fp=open("testplot.txt{0}".format("write")
        lopts={'thickness':1}
        l=line([(R1,h[1]),(R3,h[3])],**lopts)
        P+=l

    # Sset signs and check zeros
    if met=='Hecke' and dim==1:
        if h[1]*h[3]>sqrt(tol):
            # We do not have a signchange
            if add_plot==1:
                return 0 ,0,P
            else:
                return 0 ,0,0,0
        else:
            if h[1]>0:
                signs[1]=1
            else:
                signs[1]=-1
    # Begin by trying to find the 'direction' of the crossing
    var=0.0
    for j in range(1 ,3 +1 ):
        var+=abs(diffs[1 ].get(j,0))+abs(diffs[3 ].get(j,0))
    # In the first step we want to catch more more sign changes so we relax the conditions
    # Since the precision will in general not be fine to have sign changes in all
    # coefficients we are satisfied with two out of three
    nsgn=0
    for j in range(1 ,3 +1 ):
        signs[j]=1
        if not diffs[1].has_key(j):
            continue
        if diffs[1 ][j]*diffs[3][j]<tol:
            nsgn+=1
        flogger.debug("nsgn={0}".format(nsgn))
        if nsgn<1:
            # If we did not have any sign change then we probably don't have a zero
            # at least, if the relative size is large
            if abs(diffs[1][j])+abs(diffs[3][j]) > 0.01*var:
                flogger.debug("var={0}".format(var))
                flogger.debug("diffs[{0}]={1} - {2}".format(j,abs(diffs[1][j]),abs(diffs[3][j])))
                if add_plot==1:
                    return 0,0,0 ,0,P
                else:
                    return 0,0 ,0,Y,M
    for j in range(1,4):
        if not diffs[1].has_key(j):
            continue
        if diffs[1 ][j]*diffs[3 ][j]<tol:
            if diffs[1 ][j]>0:
                signs[j]=-1
            else:
                signs[j]=1
        else:
            if abs(diffs[1][j])>abs(diffs[3][j]):
                signs[j]=-1
            else:
                signs[j]=1
    # Recompute functionals using the signs
    flogger.debug("Making preliminary search")
    flogger.debug(" R1,R3={0}".format((R1,R3)))
    flogger.debug(" h = {0}".format(h))
    for j in [1,2,3]:
        flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
    flogger.debug(" signs={0}".format(signs))
    for k in [1,3]:
        h[k]=0
        for j in range(1,3+1):
            if not diffs[k].has_key(j):
                continue
            h[k]=h[k]+signs[j]*diffs[k][j]
    Rnew=prediction(h[1 ],h[3 ],R1,R3)
    Rnew_old=Rnew ## Keep track of the previous estimate
    flogger.debug(" new h={0}".format(h))
    flogger.debug(" Prediction 1={0}".format(prediction(diffs[1 ][1],diffs[3 ][1],R1,R3)))
    if diffs[1].has_key(2):
        flogger.debug(" Prediction 2={0}".format(prediction(diffs[1 ][2],diffs[3 ][2],R1,R3)))
    if diffs[1].has_key(3):
        flogger.debug(" Prediction 3={0}".format(prediction(diffs[1 ][3],diffs[3 ][3],R1,R3)))
    flogger.debug(" tol={0}".format(tol))
    flogger.debug(" Rnew={0}".format(Rnew))
    flogger.debug(" R2in+2tol={0}".format(R2in+2*tol))
    flogger.debug(" R1in-2tol={0}".format(R1in-2*tol))
    if Rnew > R2in+2*tol or Rnew<R1in-2*tol:
        ## Try to take an average prediction instead
        Rnew = sum([prediction(diffs[1 ][j],diffs[3 ][j],R1,R3) for j in range(1,4)])
        Rnew = Rnew/RF(3)
        flogger.debug("Try a new Rnew = {0}".format(Rnew))
        if Rnew > R2in+2*tol or Rnew<R1in-2*tol:
            return 0,0,0,Y,M

    [diffs[2],h[2]]=functional(S,Rnew,M,Y1,Y2,signs,c,first_time=False,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
    flogger.debug("R1,Rnew,R3={0}".format((R1,Rnew,R3)))
    flogger.debug("h={0}".format(h))
    ## We use two different error estimates: errest_x and errest_h
    ## estimated from values of x and of h(x) respectively
    ### Checking results
    if abs(h[2])>hmax and sloppy>1:
        if verbose>0:
            flogger.debug("got too large h 1={0}".format(h))
        return -1,R1old,R3old,Y,M
    zero_in=is_zero_in(st,h,diffs,tol_z,verbose)
    if zero_in == -1:
        R3old=R3
        R3=Rnew; h[3]=h[2 ]; diffs[3 ]=diffs[2 ]; errest_x=abs(Rnew-R1)
        if add_plot==1:
            P+=line([(R1,h[1]),(R3,h[3])],**lopts)
    else:
        R1old=R1
        R1=Rnew; h[1 ]=h[2 ]; diffs[1 ]=diffs[2 ]; errest_x=abs(Rnew-R3)
        if add_plot==1:
            P+=line([(R1,h[1]),(R3,h[3])],**lopts)
    step=0
    if is_derivative_large(R1,R3,diffs[1],diffs[3],met,hmax,tol_z,verbose) and sloppy>1:
        return -1,R1old,R3old,Y,M
    errest_x_old=errest_x
    errest_h = abs(h[2])
    errest_h_old = abs(h[2])
    for j in range(maxit):
        flogger.info(" ----------------- step: {0} -----------------------".format(j))
        Rnew_old=Rnew
        Rnew=prediction(h[1 ],h[3 ],R1,R3)
        diffs[2]={}  ## Reset this
        flogger.debug("[R1,R3] = [{0},{1}".format(R1,R3))
        flogger.debug("\t Rnew_old = {0}".format(Rnew_old))
        flogger.debug("\t Rnew_new(pred) = {0}".format(Rnew))
        flogger.debug("\t h={0}".format(h))
        flogger.debug("\t R1,R3={0}".format((R1,R3)))
        flogger.debug("Current errest_x={0}".format(errest_x))
        flogger.debug("Current errest_h={0}".format(errest_h))        
        if Rnew > R3+errest_x or Rnew<R1-errest_x:
            flogger.debug("Rnew is outside previous interval!")
            ## Try to take an average prediction instead
            Rnew = sum([prediction(diffs[1 ][j],diffs[3 ][j],R1,R3) for j in range(1,4)])
            Rnew = Rnew/RF(3)
            flogger.debug("Try new Rnew = {0}".format(Rnew))
            if Rnew > R2in+2*tol or Rnew<R1in-2*tol:
                return 0,0,0,Y,M
        for j in [1,2,3]:
            flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
        flogger.debug("h={0}".format(h))
        if diffs[1].has_key(1):            
            flogger.debug("Prediction 1={0}".format(prediction(diffs[1 ][1],diffs[3 ][1],R1,R3)))
        if diffs[1].has_key(2):
            flogger.debug("Prediction 2={0}".format(prediction(diffs[1 ][2],diffs[3 ][2],R1,R3)))
        if diffs[1].has_key(3):
            flogger.debug("Prediction 3={0}".format(prediction(diffs[1 ][3],diffs[3 ][3],R1,R3)))
        errest_x_old = errest_x
        errest_h_old = errest_h        
        flogger.debug("Old errest_x={0}".format(errest_x_old))
        flogger.debug("Old errest_h={0}".format(errest_h_old))
        errest_x=max(abs(Rnew-R1),abs(Rnew-R3))
        ## First rigorous check:
        if errest_x > tol:
            ##  Check if the new predictions are close together
            errest_x = abs(Rnew - prediction(diffs[1 ][1],diffs[3 ][1],R1,R3))
            errest_x = max(errest_x,abs(Rnew - prediction(diffs[1][2],diffs[3][2],R1,R3)))
            errest_x = max(errest_x,abs(Rnew - prediction(diffs[1][3],diffs[3][3],R1,R3)))
            ##
            if verbose>0:
                flogger.debug("Max dist. of predictions = {0}".format(errest_x))
        if method=='Hecke' and diffs[1].has_key(-1):
            #if diffs[1][-1]>tol:
            errest_h = max(errest_h,abs(diffs[1][-1]),abs(diffs[3][-1]))
            flogger.debug("R1,R3,Rnew,errest_h,tol={0}".format((R1,R3,Rnew,errest_h,tol)))
        else:
            flogger.debug("R1,R3,Rnew,errest_x,tol={0}".format((R1,R3,Rnew,errest_x,tol)))
        errest = min(errest_x,errest_h)
        if errest < tol: ## Liberal check
            if add_plot==1:
                return Rnew,errest,P
            else:
                return Rnew,errest,Y,M
        ## If the error estimate gets worse we try a larger value of M0
        if errest_x > errest_x_old or errest_h > errest_h_old:
            if verbose>0:
                flogger.debug("Got larger error estimate: {0} > {1}".format(errest_x,errest_x_old))
                flogger.debug("Got larger error estimate: {0} > {1}".format(errest_h,errest_h_old))
            return -1,R1old,R3old,Y,M
        
        #if Rnew>R3old+2*tol or Rnew < R1old-2*tol:
        if Rnew>R2in+2*tol or Rnew < R1in-2*tol and sloppy>0:
            if verbose>0:
                flogger.debug("We are outside the desired interval and therefore break!")
            break


        [diffs[2 ],h[2 ]]=functional(S,Rnew,M,Y1,Y2,signs,c,first_time=False,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
        if is_derivative_large(R1,Rnew,diffs[1],diffs[2],met,hmax,tol_z,verbose) and sloppy >1:
            return -1,R1old,R3old,Y,M
        ## make the estimate of the 'regular' values
        tmp = []
        for ii in diffs[2].keys():
            if ii < 0: continue
            tmp.append(abs(diffs[2][ii]))
        errest_x_old = abs(errest_x); errest_h_old = abs(errest_h)
        errest_h = max(tmp) #   map(abs,diffs[2].values()))
        #errest = abs(errest)
        flogger.debug("h={0}".format(h))
        flogger.debug("R1,Rnew,R3={0}".format((R1,Rnew,R3)))
        flogger.debug("errest_x={0}".format(errest_x))
        flogger.debug("errest_h={0}".format(errest_h ))           
        for j in [1,2,3]:
            flogger.debug("diffs[{0}]={1}".format(j,diffs.get(j)))
        if hecke_ef:
            errest_h_old = errest_h
            errest_h = max(errest_x,abs(diffs[2][-1]))
        errest = min(errest_x,errest_h)
        if errest<tol:
            if add_plot==1:
                return Rnew,errest,P
            return Rnew,errest,Y,M
        if 2*errest_x_old < errest_x and errest_x_old<1e-10:
            if verbose>0:
                flogger.debug("New error estimate in x {0} is larger than previous {1}".format(errest,errest_old))
            if add_plot==1:
                return Rnew_old,errest_x_old,P
            return Rnew_old,errest_x_old,Y,M
        if 2*errest_h_old < errest_h and errest_h_old<1e-10:
            if verbose>0:
                flogger.debug("New error estimate in h {0} is larger than previous {1}".format(errest,errest_old))
            if add_plot==1:
                return Rnew_old,errest_h_old,P
            return Rnew_old,errest_h_old,Y,M
        
        if abs(h[2])>hmax:
            return -1,R1old,R3old,Y,M
        zero_in=is_zero_in(st,h,diffs,tol_z,verbose)
        ## If we haven't movedbut the error estimates
        ## are still too large we are probably stuck in a loop.
        ## So let's back up and check with different Y
        if abs(Rnew-Rnew_old)<tol:
            return -1,R1old,R3old,Y,M
        flogger.debug("zero_in = {0}".format(zero_in))       
        if zero_in==0 and abs(errest)<tol:
            if add_plot==1:
                return Rnew,errest,P
            return Rnew,errest,Y,M
        elif zero_in not in [1,-1]:
            if verbose>0:
                flogger.debug("No zero! Breaking!")
            return Rnew,-errest,Y,M
            #flogger.debug("break!!!!"
            #    break #raise StopIteration()
        stepz={}
        if zero_in==-1:
            stepz[0]=abs(Rnew-R3)
            stepz[1]=abs(R1-R3)            
            R3old=R3
            errest_x_old = errest_x
            R3=Rnew; h[3 ]=h[2 ]; diffs[3 ]=diffs[2 ]
            errest_x=abs(Rnew-R1) # strict error estimate
            if hecke_ef:
                errest_h = max(errest_h,diffs[2][-1])

        elif zero_in==1:
            stepz[0]=abs(Rnew-R1)
            stepz[1]=abs(R1-R3)                        
            R1old=R1
            errest_x_old = errest_x
            R1=Rnew; h[1 ]=h[2 ]; diffs[1 ]=diffs[2 ]            
            errest_x=abs(Rnew-R3) # strict error estimate
            if hecke_ef:
                errest_h = max(errest_h,diffs[2][-1])
        if 2*errest_x_old < errest_x and errest_x_old<1e-10:
            if verbose>0:
                flogger.debug("New error estimate in x {0} is larger than previous {1}".format(errest_x,errest_x_old))
            return -1,R1old,R3old,Y,M
        if add_plot==1:
            P+=line([(R1,h[1]),(R3,h[3])],**lopts)
        # If we have gone in the same direction too many times we need to modify our approach
        step=step+zero_in
        if verbose>0:
            flogger.debug("step={0}".format(step))
        if step>2:    # Have changed the left end point too many times. Need to change the right also.
            ## continue to decrease R3 as long as we detect a zero.
            try:
                for zj in range(1):
                    for jj in range(1,100):
                        Rtest=Rnew + half**jj*stepz[zj]  # Need to test if this modified R3 work:
                        if verbose>0:
                            flogger.debug("Rtest1(R){0}={1}".format(jj,Rtest))
                        [diffs[2 ],h[2 ]]=functional(S,Rtest,M,Y1,Y2,signs,c,False,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)
                        flogger.debug("h={0}".format(h))
                        flogger.debug("R1,Rtest,R3={0}".format((R1,Rtest,R3)))
                        if is_derivative_large(R1,Rtest,diffs[1],diffs[2],met,hmax,tol_z,verbose) and sloppy>1:
                            return -1,R1old,R3old,Y,M
                        if abs(h[2])>hmax:
                            return -1,R1old,R3old,Y,M
                        t = is_zero_in(st,h,diffs,tol_z,verbose)
                        if t ==-1: # all is ok we still have a zero
                            R3old=R3
                            R3=Rtest; h[3]=h[2]; diffs[3]=diffs[2]; step=step-1
                            flogger.debug("Set R3: R1,R3={0}".format((R1,R3)))
                        elif t==1:
                            R1old=R1
                            R1=Rtest; h[1]=h[2]; diffs[1]=diffs[2]; step=step+1
                            flogger.debug("Set R1: R1,R3={0},{1}".format(R1,R3))
                            flogger.debug("Get back to main loop!")
                            raise StopIteration
                        # Also test if by now the difference at R3 is small enough
                        i,er = is_minimum_at(h,diffs,tol_z,verbose)
                        flogger.debug("i,er={0},{1}".format(i,er))
                        if er<tol:
                            if i==1:
                                return R1,er,Y,M
                            if i==2:
                                return Rtest,er,Y,M
                            if i==3:
                                return R3,er,Y,M
                        flogger.debug("Continue to move the endpoint again!")
            except StopIteration:
                pass
            if add_plot==1:
                P+=line([(R1,h[1]),(R3,h[3])],**lopts)

        elif step<-2: # Have gone too many times to the left need to choose a new R1
            Rtest1=Rnew - half*stepz
            Rtest2=(R1+Rtest1)*half
            if verbose>0:
                flogger.debug("Rtest1(L)={0}".format(Rtest1))
                flogger.debug("Rtest2(L)={0}".format(Rtest2))              
            t = [0,0]
            diffs_test={}; h_test={}
            for ri in [0,1]:
                Rtest = [Rtest1,Rtest2][ri]
                if verbose>0:
                    flogger.debug("Rnew={0}".format(Rnew))
                    flogger.debug("Rtest1(L)={0}".format(Rtest))
                    flogger.debug("stepz={0}".format(stepz))
                    flogger.debug("|R1-Rnew|={0}".format(abs(R1-Rnew)))
                [diffs[2],h[2]]=functional(S,Rtest,M,Y1,Y2,signs,c,False,method=met,dim=dim,set_c=set_c,fnr=fnr,verbose=verbose,hecke_ef=hecke_ef)                
                if (is_derivative_large(R1,Rtest,diffs[1],diffs[2],met,hmax,tol_z,verbose) or abs(h[2])>hmax) and sloppy>1:
                    return -1,R1old,R3old,Y,M
                t[ri] = is_zero_in(st,h,diffs,tol_z,verbose) # all is ok
                diffs_test[ri]=diffs[2]
                h_test[ri]=h[2]
                if t[ri]==1:  # all ok we have zero in [Rtest,R3]
                    break
            if t[0] == 1: # change R1 to Rtest1
                    R1old=R1; R1=Rtest1; step=step+1; h[1]=h[2]; diffs[1]=diffs[2]
                    if verbose>0:
                        flogger.debug("Set R1: R1,R3={0},{1}".format(R1,R3))
                    continue
            elif t[0]==-1 and t[1]==1:
                # we have a zero in [Rtest2,Rtest1] 
                R3old=R3; R3=Rtest1; h[3]=h_test[0]; diffs[3]=diffs_test[0]
                R1old=R1; R1=Rtest2; h[1]=h_test[1]; diffs[1]=diffs_test[1]
                step = 0
                if verbose>0:
                    flogger.debug("Set R1 and R3: R1,R3={0},{1}".format(R1,R3))
                    continue
                # Also test if by now the difference at R3 is small enough
            else:
                i,er = is_minimum_at(h,diffs,tol_z,verbose)
                if verbose>0:
                    flogger.debug("i,er={0},{1}".format(i,er))
                if er<tol:
                    if i==1:
                        return R1,er,Y,M
                    if i==2:
                        return Rtest,er,Y,M
                    if i==3:
                        return R3,er,Y,M
                    # Otherwise something is most likely wrong....
                    # We probably didn't have a zero here to begin with
                    return Rtest,-er,Y,M
            if add_plot==1:
                P+=line([(R1,h[1]),(R3,h[3])],**lopts)
    errest=min(errest_x,errest_h)
    if j>=maxit-1:
        flogger.info("Warning: may be problems in the locating function!")
        return Rnew,-errest,Y,M
    elif errest>0 and errest<1.0:
        return Rnew_old,-errest_old,Y,M

    
def is_zero_in1(h,diffs,tol=1E-12,verbose=0):
    r"""
    Tells which interval contains changes of sign.
    and make sure that the individual tests are also having sign-changes in the same interval.

    INPUT:

    - ''h'' -- dictionary h[j]=[h1,h2,h3]

    OUTPUT:

    - integer  (-1 0 1)

    EXAMPLES::


    """
    logger2.debug("diffs={0}".format(diffs))
    zi=dict(); i=0
    if h[1]*h[2] < 0:
        zi[-1]=1; i=-1
    if h[3]*h[2] < 0:
        zi[1]=1; i=1
    if zi.values().count(1) >1: # need to split
        return -2
    logger2.debug("i1={0}".format(i))
    if i==-1:
        ok=0
        for i in diffs[1].keys():
            if abs(diffs[1][i]*diffs[2][i])>tol:
                ok=1
        if ok==1:
            ## Test if we also had zero in the other...
            if h[3]*h[2] < 0:
                ok=0
                for i in diffs[1].keys():
                    if abs(diffs[3][i]*diffs[2][i])>tol:
                        ok=1
                if ok==0:
                    zi[1]=1; i=1

    if i==1:
        for i in diffs[3].keys():
            if abs(diffs[3][i]*diffs[2][i])>tol:
                return 0

    #s="Neeed to split! Not implemented!"
    #raise ValueError,s
    return i


def is_derivative_large(R1,R2,d1,d2,method='TwoY',hmax=100,tol=1E-10,verbose=0):
    t=dict()
    if abs(R2-R1)<tol:
        return 0
    for j in d1.keys():
        if j<0:
            continue
        t[j]=(d1[j]-d2[j])/(R2-R1)
        #if verbose>0:
        #    print "diff(",j,":",R1,")=",t[j]
    #print "t=",t
    #print "t.values()=",t.values()
    #print "abs(t)=",map(abs,t.values())
    #print "max(abs(t)=",max(map(abs,t.values()))
    mmax = max(map(abs,t.values()))
    if mmax>hmax:
        if verbose>0:
            print "too large derivative! max=",mmax
        return 1
    return 0


def is_minimum_at(h,diffs,tol=1E-12,verbose=0):
    a1=sum(map(abs,diffs[1].values()))
    if a1<tol:
        return 1,a1
    a2=sum(map(abs,diffs[1].values()))
    if a2<tol:
        return 2,a2
    a3=sum(map(abs,diffs[1].values()))
    if a3<tol:
        return 3,a3
    return 0,0


def is_zero_in(st,h,diffs,tol=1E-12,verbose=0):
    r"""
    Tells which interval contains changes of sign.
    and make sure that the individual tests are also having sign-changes in the same interval.

    INPUT:

    - ''h'' -- dictionary h[j]=[h1,h2,h3]

    OUTPUT:

    - integer  (-1 0 1)

    EXAMPLES::


    """
    for x in diffs.keys():
        logger2.debug("diffs[{0}]={1}".format(x,diffs[x]))
    zi=dict(); i=0
    ok=1
    a1=0; a2=0; a3=0
    for j in diffs[1].keys():
        if j <=0:
            continue
        a1+=abs(diffs[1][j])
        a2+=abs(diffs[2][j])
        a3+=abs(diffs[3][j])
        
    logger2.debug("a1={0} \t a2={1}\t a3={2}".format(a1,a2,a3))

    ## Try to decide which interval: [R1,R2] or [R2,R3] has the best chance of containing a zero

    ## First look for simultaneous sign changes:
    ## First actual sign change and then approximate...
    logger2.debug("tol :{0}".format(tol))
    zero_count={}
    ##  If we don't use symmetry we are using tests for both real and imaginary parts of coefficients.
    if st == -1:
        l = 0
        for j in diffs[1].keys():
            if j>0:
                l+=1
        l = QQ(l)/QQ(2)
#    print "keys=",diffs[1].keys()
#    print "l=",l
    for toll in [0,tol]:
        zero_count[toll]=[{},{}]
        for j in diffs[1].keys():
            zero_count[toll][0][j]=0
            zero_count[toll][1][j]=0                       
        ok=1; i=0
        zero_in_1=1; zero_in_2=1;
        for j in diffs[1].keys():
            if j<=0: continue
            if diffs[1][j]*diffs[2][j]<toll:
                zero_count[toll][0][j]=1
        for j in diffs[2].keys():
            if j<=0: continue
            if diffs[2][j]*diffs[3][j]<toll:
                zero_count[toll][1][j]=1
        if st==-1:
            for j in diffs[1].keys():
                if j>l: continue
                if zero_count[toll][0][j]==0 and zero_count[toll][0][j+l]==0:
                    zero_in_1=0
                    break
            for j in diffs[1].keys():
                if j>l: continue
                if zero_count[toll][1][j]==0 and zero_count[toll][1][j+l]==0:
                    zero_in_2=0
                    break
        else:
            for j in diffs[1].keys():
                if j<=0: continue
                if zero_count[toll][0][j]==0:
                    zero_in_1=0
                if zero_count[toll][1][j]==0:
                    zero_in_2=0
        logger2.debug("zero_in_1,2= {0} - {1}".format(zero_in_1,zero_in_2))
        logger2.debug("zero_count={0}".format(zero_count))
#        if zero_in_1==1 and zero_in_2 == 1:
#            if zero_count[toll][1].count(1)>zero_count[toll][0].count(1):
#                zero_in_1 = 0
#       if verbose>0:
#            print "zero_in_1,2=",zero_in_1,zero_in_2
        if zero_in_1==1 and zero_in_2==1:
            # possible zero in both intervals
            ## See if there is a true zero in one of the intervals
            if zero_count[0][0].values().count(1)>zero_count[0][1].values().count(1):
                i=-1
            elif zero_count[0][0].values().count(1)<zero_count[0][1].values().count(1):
                i=1
            elif a1<a2:
                i=-1
            else:
                i=1
        elif zero_in_1==1:
            i=-1
        elif zero_in_2==1:
            i=1
        else:
            i=0
        if toll==0 and i<>0:
            break
        if toll>0 and i<>0:
            ## make extra check
            if i==-1:
                a1 = max(map(abs,diffs[1].values()))
                a2 = max(map(abs,diffs[2].values()))
                if max(a1,a2)>sqrt(tol):
                    i=0
            else:
                a2 = max(map(abs,diffs[2].values()))
                a3 = max(map(abs,diffs[3].values()))
                if max(a2,a3)>sqrt(tol):
                    i=0
    if verbose>0:
        logger2.debug("zero in :".format(i))
    if i==0: ## We make a more loose test for zero before giving up
        a1=sum(map(abs,diffs[1].values()))
        a2=sum(map(abs,diffs[2].values()))
        a3=sum(map(abs,diffs[3].values()))
        if verbose>0:
            logger2.debug("a1={0}\t a2={1}\t a3={2}".format(a1,a2,a3))
    ## If the differences are really small at both endpoints of the interval
    ## we "probably" have a zero...
        aa1=max(a1,a2)
        aa2=max(a2,a3)
        if aa1<aa2 and (aa1)<tol:
            if verbose>0:
                logger2.debug("aa1<aa2 and aa1={0} < tol={1}".format(aa1,tol))
            return -1
        if aa2<aa1 and (aa2)<tol:
            if verbose>0:
                logger2.debug("aa2<aa1 and aa2={0} < tol={1}".format(aa2,tol))
            return 1
    #if h[1]*h[2] < 0:
    #    zi[-1]=1; i=-1
    #if h[3]*h[2] < 0:
    #    zi[1]=1; i=1
    #if zi.values().count(1) >1: # need to split
    #    return -2
    #print "i1=",i
    #s="Neeed to split! Not implemented!"
    #raise ValueError,s
    return i



def is_zero_in_old(h,diffs,tol=1E-12,verbose=0):
    r"""
    Tells which interval contains changes of sign.
    and make sure that the individual tests are also having sign-changes in the same interval.

    INPUT:

    - ''h'' -- dictionary h[j]=[h1,h2,h3]

    OUTPUT:

    - integer  (-1 0 1)

    EXAMPLES::


    """
    if verbose>0:
        for x in diffs.keys():
            print "diffs[",x,"]=",diffs[x]
    zi=dict(); i=0
    ok=1
    a1=0; a2=0; a3=0
    for j in diffs[1].keys():
        if j <=0:
            continue
        a1+=abs(diffs[1][j])
        a2+=abs(diffs[2][j])
        a3+=abs(diffs[3][j])
        
    if verbose>0:
        print "a1=",a1," a2=",a2," a3=",a3

    ## Try to decide which interval: [R1,R2] or [R2,R3] has the best chance of containing a zero

    ## First look for simultaneous sign changes:
    ## First actual sign change and then approximate...
    if verbose>0:
        print "tol :",tol
    zero_count={}
    for toll in [0,tol]:
        zero_count[toll]=[0,0]
        ok=1; i=0
        zero_in_1=1; zero_in_2=1;
        for j in diffs[1].keys():
            if j<=0: continue
            if diffs[1][j]*diffs[2][j]<toll:
                zero_count[toll][0]+=1
        for j in diffs[1].keys():
            if j<=0: continue
            if diffs[1][j]*diffs[2][j]>toll:
                if verbose>0:
                    print "diffs1[",j,"]*diffs2[",j,"]=",diffs[1][j]*diffs[2][j]
                zero_in_1=0
                break
        for j in diffs[2].keys():
            if j<=0: continue
            if diffs[2][j]*diffs[3][j]<toll:
                zero_count[toll][1]+=1
        for j in diffs[2].keys():
            if j<=0: continue
            if diffs[2][j]*diffs[3][j]>toll:
                if verbose>0:
                    print "diffs2[",j,"]*diffs3[",j,"]=",diffs[2][j]*diffs[3][j]
                zero_in_2=0
                break
        if verbose>0:
            print "zero_in_1,2=",zero_in_1,zero_in_2
            print "zero_count=",zero_count
        if zero_in_1==1 and zero_in_2==1:
            # possible zero in both intervals
            ## See if there is a true zero in one of the intervals
            if zero_count[0][0]>zero_count[0][1]:
                i=-1
            elif zero_count[0][0]<zero_count[0][1]:
                i=1
            elif a1<a2:
                i=-1
            else:
                i=1
        elif zero_in_1==1:
            i=-1
        elif zero_in_2==1:
            i=1
        else:
            i=0
        if toll==0 and i<>0:
            break
        if toll>0 and i<>0:
            ## make extra check
            if i==-1:
                a1 = max(map(abs,diffs[1].values()))
                a2 = max(map(abs,diffs[2].values()))
                if max(a1,a2)>sqrt(tol):
                    i=0
            else:
                a2 = max(map(abs,diffs[2].values()))
                a3 = max(map(abs,diffs[3].values()))
                if max(a2,a3)>sqrt(tol):
                    i=0
    if verbose>0:
        print "zero in :",i
    if i==0: ## We make a more loose test for zero before giving up
        a1=sum(map(abs,diffs[1].values()))
        a2=sum(map(abs,diffs[2].values()))
        a3=sum(map(abs,diffs[3].values()))
        if verbose>0:
            print "a1=",a1," a2=",a2," a3=",a3
    ## If the differences are really small at both endpoints of the interval
    ## we "probably" have a zero...
        aa1=max(a1,a2)
        aa2=max(a2,a3)
        if aa1<aa2 and (aa1)<tol:
            if verbose>0:
                print "aa1<aa2 and aa1=",aa1,"<tol=",tol
            return -1
        if aa2<aa1 and (aa2)<tol:
            if verbose>0:
                print "aa2<aa1 and aa2=",aa2,"<tol=",tol
            return 1
    #if h[1]*h[2] < 0:
    #    zi[-1]=1; i=-1
    #if h[3]*h[2] < 0:
    #    zi[1]=1; i=1
    #if zi.values().count(1) >1: # need to split
    #    return -2
    #print "i1=",i
    #s="Neeed to split! Not implemented!"
    #raise ValueError,s
    return i

def functional(S,r,M,Y1,Y2,signs,c,first_time=False,method='Hecke',dim=1,set_c=[],fnr=0,hecke_ef=1,verbose=0):
    r"""
    Computes the functional we use as an indicator of an eigenvalue.

    INPUT:

    -''S'' -- space of Maass waveforms
    -''r'' -- real
    -''M'' -- integer
    -''Y1''-- real
    -''Y2''-- real
    -''signs'' -- dict
    -''c'' -- set which coefficients to use
    -''ST''-- normalization
    -''first_time'' --
    -''method'' -- string in ['Hecke','TwoY','Innerprod']
    -''ndigs'' -- integer (number of digits wanted)

    OUTPUT:

    -  list of real values



    """
    diffsx=dict()
    h=0
    logger2.info("r,Y1,Y2={0}".format((r,Y1,Y2)))
    logger2.info("prec={0}".format(r.prec()))
    logger2.info("dim={0}".format(dim)+'\t method={0}'.format(method))
    logger2.info("set_c={0}".format(set_c))
    prec = r.prec()
    #if (ndigs>=15 and not isinstance(r,sage.libs.mpmath.ext_main.mpf)):
    #    raise TypeError,"Need mpmath input! got r={0}".format(r
    #C1=Maassform_coeffs(S,r,M,ST,Yset=Y1,ndigs=ndigs)
    #dim=1
    #set_c={}
    #verbose=S._verbose
    sym_type=S.sym_type()
    cusp_evs=S.atkin_lehner_eigenvalues() ## a dict
    Norm=S.set_norm(dim,set_c=set_c)
    Q=M+20
    do_cplx=1
    #if S.multiplier().is_real() and sym_type in [0,1]:
    do_cplx= True ## not S._use_real -- symmetry is not yet implemented in the real algorithm...
    if do_cplx:
        chi = S.multiplier()._character
        mod   = chi.modulus()
        sqch   = {}
        o = chi.order()
        for j in range(mod):
            if chi(j)<>1:
                sqch[j]=chi(j).complex_embedding(prec).sqrt()
            else:
                sqch[j]=1
    #if verbose>=0:
    #    logger2.debug("cusp_evs funs:{0}".format(cusp_evs
    #logger2.debug("sqch={0}".format(sqch
    #raise ArithmeticError
    if method<>'Innerprod':
        if do_cplx:
            logger2.info("r,Y1,M,Q={0}".format((r,Y1,M,Q)))
            logger2.debug("Norm={0}".format(Norm))
            logger2.debug("c = {0}, signs={1}".format(c,signs))
            if dim>1:
                if S.is_congruence():
                    p = S.get_primitive_p()
                    logger2.info("Hecke prime={0}".format(p)+" fnr={0}".format(fnr)+"dim={0}".format(dimension))
                    logger2.info("set_c={0}".format(set_c))
                    #Ctmp=S.Hecke_eigenfunction_from_coeffs(C1,p)
                    F = S.get_element(r,Yset=Y1,Mset=M,dim=dim,do_par=do_parallel,ncpus=ncpus,set_c=set_c)
                    logger2.info("len(F)={0}".format(len(F)))
                    if len(F)<>dim:
                        raise ArithmeticError,"Error in dimension!"
                    Ctmp={}
                    for j in range(len(F)):
                        Ctmp[j]=F[j]._coeffs[0]
                    C1=Ctmp[fnr]
                    if hecke_ef==1:
                        diffsx[-1]=S.test_Hecke_relation(C1,signed=True)
                        logger2.info("Hecke C1(a)C1(b)-C1(ab)={0}".format(diffsx[-1]))
                        logger2.info("C1.keys={0}".format(C1[0].keys()))
                        if C1[0].has_key(c[1]*c[2]):
                            logger2.info("C1({0})={1}".format(c[1]*c[2],C1[0][c[1]*c[2]]))
                    for j in c.keys():
                        logger2.debug("C1_Hecke0[0][{0}]={1}".format(c[j],C1[0][c[j]]))
                    for j in c.keys():
                        C1[0][c[j]]=C1[0][c[j]]/sqch[ c[j] % mod]
                    if p not in c.values():
                        logger2.debug("C1_Hecke[0][{0}]={1}".format(p,C1[0][p]))
                    for j in c.keys():
                        logger2.debug("C1_Hecke[0][{0}]={1}".format(c[j],C1[0][c[j]]))
                else:
                    C1=get_coeff_fast_cplx_dp(S,r,Y1,M,Q,Norm)
                    C1=C1[fnr]
            else:
                C1=get_coeff_fast_cplx_dp(S,r,Y1,M,Q,Norm)
                C1=C1[fnr]
                for j in c.keys():
                    logger2.debug("C_Hecke1[0][{0}]={1}".format(c[j],C1[0][c[j]]))
                for j in c.keys():
                    C1[0][c[j]]=C1[0][c[j]]/sqch[ c[j] % mod]
                for j in c.keys():
                    logger2.debug("C1_Hecke1[0][{0}]={1}".format(c[j],C1[0][c[j]]))
                if hecke_ef==1:
                    diffsx[-1]=S.test_Hecke_relation(C1,signed=True)
                    logger2.debug("Hecke C1(a)C1(b)-C1(ab)={0}".format(diffsx[-1]))
                    if C1[0].has_key(c[1]*c[2]):
                        logger2.info("C1({0})={1}".format(c[1]*c[2],C1[0][c[1]*c[2]]))
        else:
            C1=get_coeff_fast_real_dp(S,RR(r),RR(Y1),int(M),int(Q),Norm)
            #if dim>1:
            C1=C1[fnr]
    else:
        raise NotImplementedError
    if method=='TwoY':
        if do_cplx:
            logger2.info("do_cplx: r,Y2,M,Q={0}".format(r,Y2,M,Q,Norm,cusp_evs))
                #logger2.info("sqch={0}".format(sqch
            C2=get_coeff_fast_cplx_dp(S,RR(r),float(Y2),int(M),int(Q),Norm,cusp_ev=cusp_evs)
            C2 = C2[fnr]
            if dim>1:
                ## If we have a congruence group we try to do a Hecke eigenfunction.
                #if verbose>0:
                #    for k in range(dim):
                #        for j in c.keys():
                #            logger2.info("C2[{0}][{1}]={2}".format(k,c[j],C2[k][0][c[j]])
                if S.group().is_congruence():
                    p = S.get_primitive_p()
                    logger2.info("Hecke prime={0}".format(p)+" fnr={0}".format(fnr))
                    F = S.get_element(r,Yset=Y2,Mset=M,dim=dim,do_par=do_parallel,ncpus=ncpus,set_c=set_c)
                    logger2.info("len(F)={0}".format(len(F)))
                    if len(F)<>dim:
                        raise ArithmeticError,"Error in dimension!"
                    Ctmp2={}
                    for j in range(len(F)):
                        Ctmp2[j]=F[j]._coeffs[0]
                    C2=Ctmp2[fnr]
                    #Ctmp=S.Hecke_eigenfunction_from_coeffs(C2,p)
                    #C2=Ctmp[fnr]
                    if hecke_ef==1:
                        diffsx[-2]=S.test_Hecke_relation(C2,signed=True)
                        logger2.info("Hecke C2(a)C2(b)-C2(ab)={0}".format(diffsx[-1]))
                    for j in c.keys():
                        C2[0][c[j]]=C2[0][c[j]]/sqch[ c[j] % mod]
                    if p not in c.values():
                        logger2.info("C2_Hecke[0][{0}]={1}".format(p,C2[0][p]))
                    for j in c.keys():
                        logger2.info("C2_Hecke[0][{0}]={1}".format(c[j],C2[0][c[j]]))
                else:
                    C2=C2[fnr]

                #logger2.info("C2.keys(){0}".format(C2.keys()
                #logger2.info("C2[0].keys(){0}".format(C2[0].keys()
                #return
            else:
                for j in c.keys():
                    logger2.debug("C2_Hecke0[0][{0}]={1}".format(c[j],C2[0][c[j]]))
                if C2[0].has_key(c[1]*c[2]):
                    logger2.debug("C2({0})={1}".format(c[1]*c[2],C2[0][c[1]*c[2]]))
                for j in c.keys():
                    C2[0][c[j]]=C2[0][c[j]]/sqch[ c[j] % mod]
                for j in c.keys():
                    logger2.info("C2[0][{0}]={1}".format(c[j],C2[0][c[j]]))

                        
                if hecke_ef==1:
                    diffsx[-2]=S.test_Hecke_relation(C2,signed=True)
                    logger2.info("Hecke C2(a)C2(b)-C2(ab)={0}".format(diffsx[-2]))

        else:
            C2=get_coeff_fast_real_dp(S,RR(r),RR(Y2),int(M),int(Q),Norm)
            #if dim>1:
            C2=C2[fnr]
        #if dim>1:
        #    C1=C1[fnr]
        #s    C2=C2[fnr]
        if sym_type == -1:
            numt = len(c.keys())
            for j in c.keys():  # 
                den = abs(C1[0][c[j]])+abs(C2[0][c[j]])
                if isinstance(C1[0][c[j]],ComplexNumber):
                    diffsx[j]=(C1[0][c[j]]-C2[0][c[j]]).real()/den
                    diffsx[numt+j]=(C1[0][c[j]]-C2[0][c[j]]).imag()/den
                    logger2.debug("abs(C1[{0}])+abs(C2[{0}])={1}".format(c[j],den))
                    logger2.debug("diffs0={0}".format((C1[0][c[j]]-C2[0][c[j]]).real()))
                    logger2.debug( "diffs/den={0}".format(diffsx[j]))
                elif isinstance(C1[0][c[j]],complex):
                    diffsx[j]=(C1[0][c[j]]-C2[0][c[j]]).real/den
                    diffsx[numt+j]=(C1[0][c[j]]-C2[0][c[j]]).imag/den
                    logger2.debug("abs(C1[{0}])+abs(C2[{0}])={1}".format(c[j],den))
                    logger2.debug("diffs0={0}".format((C1[0][c[j]]-C2[0][c[j]]).real))
                    logger2.debug( "diffs/den={0}".format(diffsx[j]))
                else:
                    raise ArithmeticError,"Got coefficients of unknown type {0}".format(type(C1[0][c[j]]))
        else:
            for j in c.keys():
                den = abs(C1[0][c[j]])+abs(C2[0][c[j]])
                if isinstance(C1[0][c[j]],ComplexNumber):
                   diffsx[j]=(C1[0][c[j]]-C2[0][c[j]]).real()/den
                elif isinstance(C1[0][c[j]],complex):
                    diffsx[j]=(C1[0][c[j]]-C2[0][c[j]]).real
                else:
                    raise ArithmeticError,"Got coefficients of unknown type {0}".format(type(C1[0][c[j]]))
        h=0.0
        for j in [1,2,3]:
            logger2.debug("C1:2[{0}]={1} : {2}".format(c[j],C1[0][c[j]],C2[0][c[j]]))
            logger2.debug("diffsx[1]={0}".format(diffsx[1]))
        logger2.debug("c={0}".format(c))
        logger2.debug("signs={0}".format(signs))
        for j in c.keys():
            #print "h0={0}".format(h
            if(not first_time and signs.keys().count(j)>0 ):
                #print "h={0}".format(h,"+{0}".format(signs[j]*diffsx[j]
                h=h+signs[j]*diffsx[j]
            else:
                #print "signs={0}".format(signs
                #print "diffsx={0}".format(diffsx
                if signs.keys()==[]:
                    # We use the sign of the first test.
                    for k in c.keys():
                        if diffsx[1]<>0:
                            signs[k]=sign(diffsx[k])/sign(diffsx[1])
                        else:
                            signs[k]=sign(diffsx[k])
                        #print "sgns[{0}".format(k,"]={0}".format(signs[k]
                h=h+signs[j]*diffsx[j]
            #else:
            #    h=h+abs(diffsx[j])
            #print "h1={0}".format(h
        return [diffsx,h]
    elif method=='Hecke':
        if dim>1:
            ### We need to make a Hecke eigenform if possible
            #raise NotImplementedError,"Use TwoY for dimension>1!"
            for j in range(min(3,dim)):
                if S._sym_type==-1:
                    diffsx[j+1]=abs(real(Ctmp[j][0][-1]))-1
                    logger2.info("C[{0}][0](-1)={1}".format(j,Ctmp[j][0][-1]))
                elif cusp_evs[j]==0:
                    diffsx[j+1]=abs(real(Ctmp[j][1][1]))-1
                    logger2.info("C[{0}][1](1)={1}".format(Ctmp[j][1][1]))
                else:
                    ## Try the Hecke relation
                    diffsx[j+1]=diffsx[-1]
            if dim==2:
                if S._character.order()==222:
                    N = S._group._level
                    diffsx[3]=abs(Ctmp[0][0][N])-1
                    logger2.info("C[0][0]({0})={1}".format(N,Ctmp[j][0][N]))
                else:
                    # Try another Hecke relation
                    a = S.get_primitive_p(p)
                    b = S.get_primitive_p(a)
                    diffsx[3]=S.test_Hecke_relation(Ctmp[0],a,b,signed=True)
                    logger2.info("C[0][0]({0})={1}".format(a,Ctmp[0][0][a]))
                    logger2.info("C[0][0]({0})={1}".format(b,Ctmp[0][0][b]))
                    logger2.info("C[0][0]({0})={1}".format(a*b,Ctmp[0][0][a*b]))
            logger2.info("diffsx({0})={1}".format(r,diffsx))
            h=0
            for j in range(1,4):
                if not first_time and signs.keys().count(j)>0:
                    htmp=signs[j]*diffsx[j]
                else:
                    if signs.keys()==[]:
                        # We use the sign of the first test.
                        for k in c.keys():
                            if diffsx[1]<>0:
                                signs[k]=sign(diffsx[k])/sign(diffsx[1])
                            else:
                                signs[k]=sign(diffsx[k])
                        #logger2.debug("sgns[{0}".format(k,"]={0}".format(signs[k]
                    htmp=signs[j]*diffsx[j]
                #logger2.debug("h({0}".format(r,j,")={0}".format(h
                h = h+htmp
            logger2.info("h({0})={1}".format(r,h))
        elif dim==1:
            difftmp=C1[0][c[1]]*C1[0][c[2]]-C1[0][c[3]]*S._multiplier._character(c[3])
            if isinstance(difftmp,(float,complex)):
                diffsx[1]=difftmp.real
            else:
                diffsx[1]=difftmp.real()
                logger2.info("C[{0}]={1}".format(c[1],C1[0][c[1]]))
                logger2.info("C[{0}]={1}".format(c[2],C1[0][c[2]]))
                logger2.info("C[{0}]={1}".format(c[3],C1[0][c[3]]))
                logger2.info("difftmp={0}".format(difftmp))
                logger2.info("diffsx={0}".format(diffsx))
            if not first_time and signs.keys().count(1)>0:
                h=signs[1]*diffsx[1]
            else:
                h = diffsx[1]
        return [diffsx,h]

    elif method=='Innerprod':
        raise NotImplementedError


def plot_functional(S,R1,R2,Yset=None,neps=10,method='TwoY',clist=[],verbose=0,hmax=10,Mset=None,numpts=100,axes=None,dim=1,fnr=0,ret_list=0):
    r"""
    Make a detailed plot.
    """
    G=S._group
    if clist<>[]:
        Mmin=max(max(clist),4)+3
    else:
        Mmin=7
    if Yset==None or Yset>G.minimal_height():
        [Y,M]=find_Y_and_M(G,R1,neps,Mset=Mmin)
    else:
        [Y,M]=find_Y_and_M(G,R1,neps,Yset=Yset,Mset=Mmin)
    if Mset<>None and Mset>Mmin:
        M=Mset
    print "M=",M
    signs=dict()
    signs=dict();diffs=dict()
    c=dict(); h=dict()
    is_trivial=S.multiplier().is_trivial()
    if not is_trivial:
        x=S.multiplier().character()
        mod=x.modulus()
    else:
        mod=S.group().generalised_level()
        #print "mod=",mod
    if method=='Hecke':
        a = S.get_primitive_p()
        b = S.get_primitive_p(a)
        c[1]=a; c[2]=b; c[3]=a*b
        if M < c[3]+10:
            [Y,M]=find_Y_and_M(G,R1,neps,Yset=Yset,Mset=c[3]+10)
    elif not clist:
        usei=1
        for i in range(dim+1,100):
            if gcd(i,mod)>1:
                #print "gcd(",i,")",gcd(i,mod)
                continue
            c[usei]=i
            usei+=1
            if usei>3:
                break
    else:
        for j in range(len(clist)):
            c[j]=clist[j]
    mycolors=['red','green','blue']
    mycolors.extend(colors.keys())
    Y1=Y; Y2=RR(0.995)*Y1
    ll=dict()
    if method=='TwoY':
        numc=len(c.keys())
        for j in range(numc):
            ll[j]=list()
    else:
        ll[1]=list()
    stepz=RR(R2-R1)/RR(numpts)
    diffs_old={}
    deriv={}
    i = 0
    for j in range(numpts):
        r=R1+j*stepz
        diffs,h=functional(S,r,M,Y1,Y2,signs,c,first_time=True,method=method,dim=dim,ndigs=neps,verbose=verbose,fnr=fnr)
        if method=='TwoY':
            for k in range(numc):
                ll[k].append((r,diffs[k+1]))
                if k==1:
                    signs[k]=sign(diffs[k+1])
        else:
            ll[1].append((r,diffs[1]))
            if j==1:
                signs[1]=sign(diffs[1])
        if diffs_old:
            if is_derivative_large(R1-stepz,R1,diffs_old,diffs,method,hmax,1E-10,verbose) or abs(h)>hmax:
                print "Decrease Y!"
                Y1=Y1*0.95
                Y2=Y2*0.95
                i=i+1
                if i>100:
                    raise ArithmeticError,"Can not get smooth functions!"
                if verbose>0:
                    print "Y1,Y2=",Y1,Y2
            #for k in range(numc):
            #    deriv[k]=diffs[k+1]-diffs_old[k+1]
            #    deriv[k]=deriv[k]/stepz
            #    if verbose>0:
            #s        print "Derivate[",k,"]=",deriv[k]
        diffs_old=diffs

    if method=='TwoY':
        G=Graphics()
        if axes<>None:
            if not axes.has_key('xmin'):
                axes['xmin']=R1
            if not axes.has_key('xmax'):
                axes['xmax']=R2
            G.set_axes_range(**axes)
            #G.set_axes_range(xmin=R1,xmax=R2)
        print "xmin,xmax=",R1,R2
        print "axes=",G.get_axes_range()
        if ret_list:
            return ll
        #print ll
        for j in range(numc):
            n = len(ll[j])
            #print j,":",c[j]
            #print "len(ll[j])",n
            label="c({0})".format(c[j+1])
            p=plot(spline(ll[j]),R1,R2, color=mycolors[j],legend_label=label)
            #print "axes 0=",G.get_axes_range()
            G=G+p
            #print "axes 1=",G.get_axes_range()

    else:
        label='c({0})c({1})-c({2})'.format(c[1],c[3],c[3])
        #print ll
        n = len(ll[1])
        G=plot(spline(ll[1]),R1,R2, color=mycolors[0],legend_label=label)
    if axes<>None:
        G.set_axes_range(**axes)
    return G



def plot_minus_1(S,R1,R2,Yset=None,neps=10,method='TwoY',clist=[],verbose=0,hmax=10,Mset=None,numpts=50,axes=None,dim=1,fnr=-1,ret_list=0):
    r"""
    Plot C(0)(-1)
    """
    assert S._sym_type not in [0,1]
    G=S._group
    if clist<>[]:
        Mmin=max(max(clist),4)+3
    else:
        Mmin=7
    if Yset==None or Yset>G.minimal_height():
        [Y,M]=find_Y_and_M(G,R1,neps,Mset=Mmin)
    else:
        [Y,M]=find_Y_and_M(G,R1,neps,Yset=Yset,Mset=Mmin)
    if Mset<>None and Mset>Mmin:
        M=Mset
    print "M=",M
    signs=dict()
    signs=dict();diffs=dict()
    stepz=RR(R2-R1)/RR(numpts)
    diffs_old={}
    deriv={}
    i = 0
    Q=M+10
    if dim==1:
        data=[]
        data2=[]
    else:
        data={}
        data2={}
        for j in range(dim):
            if fnr>=0 and j<>fnr:
                continue
            data[j]=[]
            data2[j]=[]
    NN = S.set_norm(dim)
    mycolors=['red','green','blue']
    mycolors.extend(colors.keys())
    #p=2
    N = ZZ(S._group._level)
    p = N.factor()[0][0]
    for d,m in N.factor():
        if m>1:
            p = d
            break
    x = S._character
    if x.modulus() == N/p:
        if ZZ(p*p).divides(N):
            cmp_to=0
        else:
            cmp_to=1
    else:
        cmp_to=0
    for j in range(numpts):
        r=R1+j*stepz
        X = get_coeff_fast_cplx_dp(S,r, Y,M, Q,NN)
        X = X[fnr]
        if dim==1:
            data.append((r,abs(X[0][-1])-1))
            data2.append((r,abs(X[0][p])-cmp_to) )
        else:
            H=S.Hecke_eigenfunction_from_coeffs(X,3)
            for j in range(dim):
                if fnr>=0 and j<>fnr:
                    continue
                data[j].append((r,abs(H[j][0][-1])-1))
                data2[j].append( (r,abs(H[j][0][p])-cmp_to) )
        if verbose>0:
            print r
    if dim==1:
        label='|c(-1)|-1'
        n = len(data)
        G=plot(spline(data),R1,R2, color=mycolors[0],legend_label=label,ymin=-1,ymax=1)
        label = '|c{0}({1})|-{2}'.format(p,p,cmp_to)
        G+=plot(spline(data2),R1,R2, color='black',legend_label=label,ymin=-1,ymax=1)
    else:
        G=Graphics()
        for j in range(dim):
            if fnr>=0 and j<>fnr:
                continue
            label='|c{0}(-1)|-1'.format(j)
            G+=plot(spline(data[j]),R1,R2, color=mycolors[j],legend_label=label,ymin=-1,ymax=1)
            label = '|c{0}({1})|-{2}'.format(j,p,cmp_to)
            G+=plot(spline(data2[j]),R1,R2, color='black',legend_label=label,ymin=-1,ymax=1)


    return G


def plot_ev_diff(M,l,N=100):
    r"""
    l is a list of eivenvalues corresponding to the space M
    """
    evs=map(lambda x:x[0],l)
    R1=min(evs)
    R2=max(evs)
    h=(R2-R1)/RR(N)
    v=[]
    mycolors=['red','green','blue']
    mycolors.extend(colors.keys())
    for j in range(N):
        r=R1+h*RR(j)
        t=M.Weyl_law_N(r)
        for x in evs:
            if x<=r:
                t=t-1
        v.append((r,t))
    label='N(T)-#{R<=T}'
    G=plot(spline(v),R1,R2,color=mycolors[0],legend_label=label)
    return G

import numpy as np










def get_eigenvectors(A):
    ev=A.eigenvalues()
    E=A.parent().one()
    for x in ev:
        B=A-x*E
        Q,R=B.qr_decomposition()
        # Now find kernel of upper-triangular R
        eigenvec=Vector_complex_dense(U.column(0).parent(),0)
        nz=R.nrows()-1
        # If there is only one non-zero element in the row then the corresponding variable is zero
        for j in range(A.nrows()):
            if R.row(R.nrows()-1-j).list().count(0)<>R.nrows()-1:
                eigenvec[j]=0
        ## first figure out where the first non-zero row is
        for j in range(A.nrows()):
            ## first figure out where the first non-zero row is
            if R.row(R.nrows()-1-j).list().count(0)<>R.nrows():
                nz=R.nrows()-1-j
                break
        print "non-zero at row:",nz
        for i in range(A.nrows()-1,0,-1):
            pass



def find_kernel(U):
    piv=dict() #<int*>sage_malloc(sizeof(int)*N)
    used=dict() #<int*>sage_malloc(sizeof(int)*N)

    N=U.nrows()
    for j in range(N):
        piv[j]=0
        used[j]=0
    rhs=Vector_complex_dense(U.column(0).parent(),0)
    for m in range(N):
        temp=0.0
        maxi=0
        #Locate maximum
        for j in range(N):
            if used[j]<>0:
                continue
            if abs(U[j,m]) <= temp:
                continue
            maxi=j
            temp=abs(U[j,m])
        piv[m]=maxi
        used[maxi]=1
        temp2=U[maxi,m]
        print "temp2=",temp2
        if temp2==0.0:
            print 'ERROR: pivot(',m,') == 0, system bad!!!'
            raise ArithmeticError
        for j in range(m+1,N): #from m+1<=j<N+num_rhs: # do j=M+1,N+1
            U[maxi,j]=U[maxi,j]/temp2
            #! eliminate from all other rows
        for j in range(maxi): #from 0<=j<=maxi-1:
            TT=U[j,m]
            print "TT=",TT
            for k in range(m+1,N): #from m+1<=k<N+num_rhs: #K=M+1,N+1
                U[j,k]=U[j,k]-U[maxi,k]*TT
            rhs[j]=rhs[j]-U[maxi,k]*TT
        for j in range( maxi+1,N): #from maxi+1<=j<N: #DO J=Maxi+1,N
            TT=U[j,m]
            for k in range(m+1,N): #from m+1<=k<N+num_rhs: #do K=M+1,N+1
                U[j,k]=U[j,k]-U[maxi,k]*TT
    #!! now remember we have pivot for x_j at PIV(j) not j
    m_offs=0
    print U
    #return U
    C=Vector_complex_dense(U.column(0).parent(),0)
    for m in range(N):
        C[m]=U[piv[m-m_offs],N-1]
    return C


def cusp_symmetrizable(M,j):
    G = M._group
    a,b,c,d = G._cusp_data[j]['normalizer']
    #print a,b,c,d
    #A = [a*d+b*c,-2*a*b,-2*d*c,a*d+b*c]

    if G._is_Gamma0:
        if G._level.divides(2*d*c):
            return 1
    else:
        A = [a*d+b*c,-2*a*b,-2*d*c,a*d+b*c]
        if A in G:
            return 1

    return 0

def get_list_of_cusp_symmetries(M,add_zero=0,verbose=0,only_order2=1):
    res=[]
    ## First see which cusps are symmetrizable with respect to the space M
    ## (i.e. with group and character)
    sym_cusps=[]
    evs={}
    orders=[]
    keys={}
    i=0
    cs=M.cusp_symmetries()
    if not M._character.is_trivial() or not M._group._level.is_squarefree():
        add_zero=1
    for j in range(M._group._ncusps):
        o,d = cs.get(j,(0,0)) #G._cusp_normalizer_is_normalizer[j]
        sym_cusps.append(j)
        if o==2:
            eps=[1,-1]
        elif o==3:
            z3=CyclotomicField(3).gen().complex_embedding()
            eps=[1,z3,z3**2]
        elif o==4:
            z4=CC(1,0)
            eps=[1,z4,-1,z4**3]
        elif o>4:
            zo=CyclotomicField(o).gen().complex_embedding()
            eps = [zo**k for k in range(o)]
        if o>1:
            evs[j]=eps
            keys[j]=i
            i+=1
            if add_zero==1:
                evs[j].append(0)
                #i+=1
                if verbose>0:
                    print "Adding 0 evs[",j,"]=",evs[j]
                orders.append(o+1)
            else:
                orders.append(o)

            if verbose>0:
                print "evs=",evs

    SG = AbelianGroup(orders)
    if verbose>0:
        print "Symmetry group: ",SG

    for x in SG:
        l = x.list()
        if verbose>0:
            print "l=",l
        cusp_evs={}
        for j in evs.keys():
            cusp_evs[j]=evs[j][ l[keys[j]]]
        res.append(cusp_evs)
    return res


def get_list_of_MySubgroups(d):
    l=[]
    for sig in d.keys():
        ps = get_permS_from_sig(sig)
        for pr in d[sig]['pgl_conj'].keys():
            print "ps=",ps.to_cycles()
            print "pr=",pr.to_cycles()
            G=MySubgroup(o2=ps,o3=pr)
            l.append(G)

    return l


def merge_collection_into(db=None,A='FS',B='HT'):
    r"""
    Merges the two collection and put the resulting into A.
    """
    if db==None:
        return
    finds=db._mongo_db[B].find()
    for f in finds:
        db._mongo_db[A].save(f)


def complete_Maassforms(data={},db=None,verbose=0,**kwds):
    if not db:
        return 0
    maassid = db.find_Maass_form_id(data,**kwds)
    for idd in maassid:
        try:
            complete_record(idd,db,verbose=verbose,**kwds)
        except KeyboardInterrupt:
            break





@fork
def complete_record(maassid,db,get_coeffs=0,force_check=0,verbose=0,delete=0,**kwds):
    r"""
    Check the Maass form with the given id and see that
    all properties are set.

    """
    print "maassid=",maassid
    f = db.get_Maass_forms(maassid)
    print f
    if len(f)==0:
        return 0
    f  = f[0]
    R  = f.get('Eigenvalue',None)
    N  = f.get('Level',None)
    wt  = f.get('Weight',None)
    ch = f.get('Character',None)
    er = f.get('Error',None)
    date = f.get('date',None)
    st = f.get('Symmetry',None)
    cev= f.get('Cusp_evs',None)
    nc = f.get('Numc',None)
    dim = f.get('Dim',None)
    cid = f.get('coeff_id',None)
    conrey=f.get('Conrey',0)
    if ch>euler_phi(N):
        conrey=int(1)
    f_M0 = f.get('M0',0)
    f_Y = f.get('Y',0)
    if dim==0:
        dim=1
    Y0  = f.get('Y',0.5)
    M0 = f.get('M0',0)
    tol=kwds.get('tol',kwds.get('prec',kwds.get('eps',1e-5)))
    #if db._verbose>0:
    ## TODO: Set appripriate c's here.
    set_c=[]
    print "N,wt,ch,R,tol=",N,wt,ch,R,tol
    if R==None or N==None or wt==None or ch==None: # We can't do anything without level, eigenvalue, weight or character...
        return 0
    ## We only want to check for cusp eigenvalues in the cases where they do exist:
    ## Remove Nan's:
    if RR(R).is_infinity() or RR(R).is_NaN() or R<=0.0:
        if verbose>0:
            print "Invalid value of parameter R:{0}".format(R)
        db._collection.remove(maassid)
    ## Change to Sage character
    Dl = DirichletGroup(N).list()
    x = DirichletCharacter_conrey(DirichletGroup_conrey(N),ch).sage_character()
    chh = Dl.index(x)
    print "chh:",chh
    if force_check:
        dim=0  ## We recheck all possible dimensions
    M = MaassWaveForms(N,ch=chh,weight=wt)
    if (len(filter(lambda x:x[0]==1,M.cusp_symmetries().values()))==1) and st and er and not force_check:
        ## In this case we have no cusp eigenvalues but there also shouldn't be any...
        do_cev=cev
    else:
        do_cev = None
    got_match=0
    if st==None or do_cev==None or er==None:
        kwds={}
        if st in [0,1] and not force_check<=1:
            sym_types=[st]
        else:
            sym_types=[0,1]
        if isinstance(cev,(list,dict)) and cev<>[] and cev<>{} and not force_check:
            l=[cev]
        else:
            l = get_list_of_cusp_symmetries(M)
        if dim == None or dim<=0:
            dimmax = M.max_assumed_dim()
        else:
            dimmax=1
        if dimmax<dim:
            dimmax = dim
        res={}
        if verbose>0:
            print "dimmax=",dimmax
        havedim={}
        for d in range(1,dimmax+1):
            res[d]=[]
            if havedim.has_key(d):
                continue
            if verbose>0:
                print "Testing dimension:",d
            for st in sym_types:
                M.set_sym_type(st)
                if verbose>0:
                    print "sym_type=",M._sym_type
                got_match=0
                for x in l:
                    if verbose>0:
                        print "x=",x
                    try:
                        M.set_cusp_evs(x)
                    except ValueError:
                        continue
                    # If we have a match already we don't want to give up the first eigenvalue
                    if got_match==1 and x[1]==0:
                        if verbose>1:
                            print "Already got a match. Discarding the eignvalues:{0}".format(x)
                        continue
                    if verbose>1:
                        print "dim=",d
                        print "tol=",tol
                        print "ev=",M.cusp_evs()
                        print "sym=",M.sym_type()
                        print "R=",R,type(R)
                        print "er=",er
                        print "Y,M0=",Y0,M0
                        #print "M=",M
                    Y=Y0
                    # Test if this is the correct symmetry:
                    ## And we also see if the Y-value is ok.
                    M00 = M.smallest_M0()
                    if M0 < M00:
                        M0 = M00
                    minerr=1
                    for yi in range(10):
                        Rf = RDF(R)
                        Yf = RR(float(Y))
                        if verbose>2:
                            print "checking Y,M0,d=",Y,M0,d
                            print "R=",R
                            print "R=",float(R)
                            print "Rf=",Rf,type(Rf)
                            if hasattr(Rf,"prec"):
                                print "Rf.prec()=",Rf.prec()
                            if hasattr(Yf,"prec"):
                                print "Yf.prec()=",Yf.prec()
                            print "M._prec=",M._prec
                        if verbose>3:
                            M._verbose=2
                        else:
                            M._verbose=0
                        F = M.get_element(Rf,Yset=Yf,Mset=M0,dim=d,do_par=do_parallel,ncpus=ncpus,set_c=set_c)
                        if d==1:
                            if N==1 or chh==0:
                                err = F.test(format='float',method='Hecke')
                            else:
                                err = F.test(format='float',method='pcoeff')
                            if verbose>1:
                                print "tol=",tol
                                print "err=",err
                        else:
                            err=0
                            for i in range(len(F)):
                                if chh<>0:
                                    err1= F[i].test(format='float',method='pcoeff')
                                else:
                                    err1=1.0
                                err2 =F[i].test(format='float')
                                errtmp=abs(min(err1,err2))
                                if verbose>1:
                                    #print "er1,er2=",err1,err2
                                    print "err[",i,"]=",errtmp
                                if errtmp<tol: ## If we have one small error we probably have correct size of Y
                                    yi=4
                                if errtmp>err:
                                    err=errtmp
                            if verbose>1:
                                print "tol=",tol
                                print "err=",err
                        if err<minerr:
                            minerr=err
                        if er <>None and er>0:
                            if err < max(tol,er):
                                if verbose>0:
                                    print "err=",err
                                    print "err<tol!"
                                got_match=1
                                res[d].append((M.cusp_evs(),st,err,M0,Y))
                                break
                        else:
                            # print "HERE"
                            if err < tol:
                                # print "ERRRRRRRRRRRR"
                                got_match=1
                                if isinstance(err,float):
                                    res[d].append((M.cusp_evs(),st,err,M0,Y))
                                else:
                                    res[d].append((M.cusp_evs(),st,err,M0,Y))
                                break
                        if yi>3 and err>0.5:  ## Probably not correct symmetry
                            break
                        Y=Y*0.9
                    if verbose==1:
                        print "minerr=",minerr
            ## Check how large dim. we already have
            if len(res[d])>0:
                havedim[len(res[d])]=1
        res2={}
        mult=0
        for d in res.keys():
            if len(res[d])>0:
                res2[d]=res[d]
                mult=mult+len(res2[d])
        res = res2
        if verbose>0:
            print "Results:",res
            print "mult:",mult
        if delete==1:
            if max(map(len,res.values()))==0:
                if verbose>0:
                    print "Removing bad eigenvalue R:{0}".format(R)
                db._collection.remove(maassid)
        max_nset=M._group._ncusps - M.cusp_symmetries().values().count((0,0))
        new_ids=[]
        inserted=0
        for i in range(1):
            for d in res.keys():
                if len(res[d])==0:
                    continue
                if verbose>0:
                    print "R:{0} Dim: {1}".format(R,d)
                #key = {'_id':maassid}
                for j in range(len(res[d])):
                    cres = res[d][j]
                    evs= cres[0]
                    numset=len(evs)-evs.count(0)
                    if numset==max_nset or i==0:
                        key = {
                            "Level":int(N),
                            "Weight":float(wt),
                            "Character":int(ch),
                            "Symmetry":int(cres[1]),
                            "Cusp_evs":cres[0],
                            "Eigenvalue":float(R)
                            }
                        find = db._collection.find(key)
                        # If it is already in there I simply update
                        fc = find.count()
                        if verbose>0:
                            print "key=",key
                            print "num finds:",fc
                        if fc==1:
                            find = find.next()
                            new_id=find.get('_id',None)
                            if new_id==None:
                                continue
                            if verbose>0:
                                print "Update id:",new_id
                            key = {'_id':new_id}
                            toset={"$set":{
                                    "Error":float(cres[2]),
                                    "Dim":int(d),
                                    "M0":int(cres[3]),
                                    "Y":float(cres[4]),
                                    "date":date,
                                    "Numc":int(nc),
                                    "Dim":int(d)}
                                   }
                            db._collection.update(key,toset,upsert=False)
                            new_ids.append(new_id)
                            inserted=1
                            if verbose>0:
                                print "Update record!"
                                if verbose>1:
                                    print "toset=",toset

                        elif fc==0:
                            toset={"Eigenvalue":float(R),
                                   "Level":int(N),
                                   "Weight":int(wt),
                                   "Character":int(ch),
                                   "Cusp_evs":cres[0],
                                   "Symmetry":int(cres[1]),
                                   "Error":float(cres[2]),
                                   "Dim":int(d),
                                   "M0":int(cres[3]),
                                   "Y":float(cres[4]),
                                   "date":date,
                                   "Conrey":conrey,
                                   "Numc":int(nc),
                                   "Dim":int(d)}
                            if verbose>0:
                                print "Insert new record!"
                                if verbose>1:
                                    print "toset=",toset
                            new_id=db._collection.insert(toset,upsert=True)
                            new_ids.append(new_id)
                            inserted=1
                        else:
                            warn.warning("Got more than one record with the same symmetries!")
            if inserted==1:
                break
        # Check if the initial id is amongst the new ones. Otherwise we delete the old record
        if verbose>0:
            print "new ids:",new_ids
        if not bson.objectid.ObjectId(maassid) in new_ids and inserted>=1:
            if verbose>0:
                print "Removing old id!"
            db._collection.remove({'_id':maassid})
    #if verbose>0:
    #        #key = {'_id':maassid}
    #        #f=db._collection.find(key)
    #        for ff in new_ids:
    #
    #print "Completed record "
    #for s in res[d]:
    #    print s
    for newid in new_ids:
        if verbose>0:
            print "newid=",newid
            f = db.get_Maass_forms(newid)
            if len(f)>0:
                db.display_one_record(f[0])
        f = db.get_Maass_forms(newid)
        if verbose>0:
            print "f=",f
        if len(f)==0:
            continue
        f = f[0]
        nc = f.get('Numc')
        if nc < get_coeffs and get_coeffs>0:
            if verbose>0:
                print "Will compute {0} coefficients!".format(get_coeffs)
                print "tol={0}".format(tol)
            if tol<1:
                ndig=abs(int(log_b(tol,10)))
            else:
                ndig=abs(int(tol))
            if verbose>0:
                print "ndig={0}".format(ndig)
            compute_coeffs_for_one_form(newid,db,numc=get_coeffs,ndig=ndig,verbose=verbose)
            if verbose>0:
                print "Computed coefficients!"



def compute_coeffs_for_one_form(maassid,db,numc=100,ndig=10,verbose=0):
    r"""
    Data is a dictinary containing all relevant information...
    """
    if verbose>0:
        print "In compute_coeffs for one!"
    data=db.get_Maass_forms(maassid)
    if len(data)==0:
        return 0
    data=data[0]
    numc0=data.get('Numc',0)
    if numc0>=numc:
        if db._verbose>0:
            print "Already got {0} coefficients!".format(numc0)
        return 1
    level=data.get('Level',0)
    weight=data.get('Weight',0)
    ch=data.get('Character',0)
    conrey  = data.get('Conrey',0)
    if verbose>0:
        print "conrey, ch:",conrey,ch
    if conrey<>0 or ch>=euler_phi(level):
        chh = db.getDircharSageFromConrey(level,ch)
    else:
        chh=ch
    if verbose>0:
        print "chh:",chh
    dim=data.get('Dim',0)
    sym_type=data.get('Symmetry',-1)
    R=data.get('Eigenvalue',0)
    cusp_evs=data.get('Cusp_evs',[])
    err=data.get('Error',0)
    if verbose>0:
        print "err:",err
        print "ndig:",ndig
    if err>0:
        ndig = min(ndig,abs(int(log_b(err*10,10))))
    key ={'_id':maassid}
    if level==0 or R==0:
        return
    if verbose>0:
        print "data=",data
    i = db.register_coeff_work(data)
    if not i:
        return
    if verbose>0:
        print "Compute coefficents for N,wt,ch,R",level,weight,chh,R
        print "sym_type=",sym_type," cusp_evs=",cusp_evs
    Rst=str(R).split(".")
    Rst=(Rst[0]+"."+Rst[1][0:10])[0:12]
    fname='{0}-{1}-{2}-{3}-{4}'.format(level,weight,ch,sym_type,Rst)
    print "'fname=",fname
    M = MaassWaveForms(level,weight=weight,ch=chh,sym_type=sym_type,cusp_evs=cusp_evs)
    if verbose>0:
        print "numc=",numc
        print "numc=",ndig
        print "dim=",dim
    #F = M.get_element(R)
    #F.
    minY=M.group().minimal_height()
    C=phase_2_cplx_dp_sym(M,R,5,numc,M0in=0,Yin=minY,ndig=ndig,dim=dim,retf=1,verbose=verbose-1)
    if C=={}:
        return
    #    if dim==1:
    #print "C=",C
    fs = gridfs.GridFS(db._mongo_db,'Coefficients')
    fid=fs.put(dumps(C),filename='c-'+fname,maass_id=maassid,level=int(level),weight=int(weight),eigenvalue=float(R),numc=int(numc))

    if fid:
        print "Got coefficents!: ",type(C)
        ## Update number of coefficients.
        if db._verbose>0:
            print "Update number of coefficients!"
        key ={'_id':maassid}
        vals = {"$set":{"Numc":int(numc),"coeff_id":fid}}
        print "key=",key
        print "vals=",vals
        db._collection.update(key,vals,upsert=True)
    #db.deregister_coeff_work()
    ## We look for



def fix_AtkinLehner(data={},db=None,verbose=0,**kwds):
    if not db:
        return 0
    maassid = db.find_Maass_form_id(data,**kwds)
    i=0
    for idd in maassid:
        f = db.get_Maass_forms({'_id':idd})
        if len(f)<0 or len(f)>1:
            print "Error: more than one record with this id! f={0}".format(f)
            continue
        f=f[0]
        N = f.get('Level',0)
        if N<=0:
            print "Error: level<=0 for f={0}".format(f)
            return
        cev=f.get("Cusp_evs",[])
        if cev==[]:
            # make a set of eigenvalues set to zero
            nc = Gamma0(N).ncusps()
            cev = [int(0) for i in range(nc)]
            cev[0]=int(1)
            for col in db._show_collection:
                col.update({'_id':idd},{ "$set" :{'Cusp_evs': cev }},upsert=False)
        elif cev[0]==0:
            cev[0]=1
            for col in db._show_collection:
                db._collection.update({'_id':idd},{ "$set" :{'Cusp_evs': cev }},upsert=False)
    return 1

def remove_duplicates(data={},db=None,precise=1,tol=0.0,**kwds): #dryrun=0):
    verbose=kwds.get('verbose',0)
    dryrun=kwds.get('dryrun')
    maassid = db.find_Maass_form_id(data)
    maassidtmp=copy(maassid)
    i=0
    for idd in maassid:
        if idd not in maassidtmp:
            continue
        f = db.get_Maass_forms({'_id':idd})
        if len(f)==0:
            continue
        f=f[0]
        if verbose>2:
            print "Comparing f:",f
        find={}
        if precise==1:
            find['Eigenvalue']  = f.get('Eigenvalue',None)
        else:
            R= f.get('Eigenvalue',0)
            ep = float(max(f.get('Error',1),1e-8))
            ep=min(ep,tol)
            find['r1']=R-ep; find['r2']=R+ep
            #find['Eigenvalue']={"$gte":R-ep,"$lte":R+ep}
        find['Level']  = f.get('Level',None)
        find['Weight'] = f.get('Weight',None)
        find['Character'] = f.get('Character',None)
        find['Symmetry'] = f.get('Symmetry',None)
        find['Cusp_evs']= f.get('Cusp_evs',None)
        nc = f.get('Numc',None)
        #print "find=",find
        if verbose>1:
            print "search:",find
        newids = db.find_Maass_form_id(find,**kwds)
        if verbose>1:
            print "Got {0} records".format(len(newids))
        if idd in newids:
            newids.remove(idd)
        if len(newids)==0:
            if verbose>1:
                print "only got one match so no duplicate!"
            continue
        if verbose>1:
            print "newids: ",newids
        for idnew in newids:
            i+=1
            fnew = db.get_Maass_forms({'_id':idnew})[0]
            ncnew=fnew.get('Numc')
            key = {'_id':idnew}
            if ncnew<=nc:
                for coll in db._show_collection:
                    if coll.find(key).count()==0:
                        continue
                    if verbose>0:
                        print "remove f:{0}".format(fnew.get('Eigenvalue',None))
                    if dryrun==0:
                        coll.remove(key)
                    else:
                        print "Rem: {0} from coll {1}".format(fnew,coll.name)
                    if idnew in maassidtmp:
                        #maassidtmp.remove(idnew)
                        pass
            elif ncnew>nc:
                for coll in db._show_collection:
                    if verbose>0:
                        print "remove orig f:{0}".format(f.get('Eigenvalue',None))
                    if dryrun==0:
                        coll.remove({'_id':idd})
                    else:
                        print "Rem orig: {0}".format(f)
                        #coll.remove({'_id':idd})
                    if idd in maassidtmp:
                        maassidtmp.remove(idd)
                        pass


def Hecke_check(data={},db=None,verbose=0,**kwds):
    maassid = db.find_Maass_form_id(data,**kwds)
    ni=0
    for idd in maassid:
        key = {'_id':idd}
        f = db.get_Maass_forms(key)
        if len(f)>1:
            print "Wrong lentgth!"
            return 0
        f = f[0]
        nc = f.get('Numc',0)
        if nc==0:
            continue
        C = db.get_coefficients(key)
        if len(C)==0:
            continue
        C = C[0]
        no_c=0
        if len(C[0])==0 or len(C[0][0])==0:
            no_c=1
        if dict_depth(C)==3:
            nnc = len(C[0][0].keys())
            if nnc>0 and nnc<nc:
                print "Have only {0} coefficients! Should be {1}".format(nnc,nc)
                no_c=1
        if no_c<>0:
            ## We have 0 coefficients?"
            inserts = {"$set":{'Numc':int(0)}}
            db._collection.update(key,inserts,upsert=False)
            if verbose>0:
                print "Updated numc from {0} to {1}".format(nc,0)
            continue
        dim = f.get('Dim')
        err = f.get('Error')
        for j in range(dim):
            try:
                c2 = C[j][0][2]
                c3 = C[j][0][3]
                c6 = C[j][0][6]
            except:
                print "No Hecke form? f={0}".format(f)
                print "C.keys()=",C[j][0].keys()
                return
            er = abs(c2*c3-c6)
            if er>err or er > 1e-5:
                if er<1e-5:
                    ## Maybe update the error?
                    inserts = {"$set":{'Error':float(er)}}
                    db._collection.update(key,inserts,upsert=False)
                    if verbose>0:
                        print "Updated error from {0} to {1}".format(err,er)
                else:
                    ni+=1
                    if verbose>0:
                        print "No Hecke form? f={0}".format(f)
                        print "c2=",c2
                        print "c3=",c3
                        print "c6=",c6
                        print "c2c3=",c2*c3
                        print "er=",er
                        return
    print "Number of problems:{0}".format(ni)


def check_coefficients(data={},db=None,verbose=0,**kwds):
    if not db:
        return 0
    maassid = db.find_Maass_form_id(data,**kwds)
    i=0
    dryrun=kwds.get('dryrun',0)
    if verbose>0:
        print "num of maassids:{0}".format(len(maassid))
    for idd in maassid:
        #print "idd=",idd
        if idd==None:
            continue
        f = db.get_Maass_forms({'_id':idd})
        if len(f)>1:
            print "Too many f with id:{0}".format(idd)
            return
        if verbose>1:
            print "f=",f
        f=f[0]
        numc=f.get('Numc',0)
        if numc==0:
            continue
        dim = f.get('Dim',1)
        if dim==0:
            dim=1
        nc = Gamma0(f.get('Level',1)).ncusps()
        cid=f.get('coeff_id',None)
        stop=0
        if not cid:
            Calt = f.get('Coefficients',[])
            n = len(Calt)
            if n>=numc:
                continue
            stop=0
            print "Did not have coefficients file or list!"
            print "f=",f
            print "Should have {0} coefficients!".format(numc)
            db._collection.update({'_id':idd},{ "$set" :{'Numc': int(0) }})
        else:
            fs = gridfs.GridFS(db._mongo_db,'Coefficients')
            fget=fs.get(cid)
            C=loads(fget.read())
            if isinstance(C,dict) and stop==0:
                n1=len(C.keys())
                if verbose>1:
                    print "n1=",n1
                    print "Dim=",dim
                if n1<>dim:
                    stop=1
                    print "Wrong length of first key!"
                if n1>0:
                    n2 = len(C[0].keys())
                    if n2<>nc:
                        print "Wrong length of second key!"
                        stop=1
            elif isinstance(C,list):
                n1 = len(C)
                if verbose>2:
                    print "n1=",n1
                    print "dim=",dim
                if n1<>dim:
                    stop=1
                    print "Wrong length of first list! n1=",n1

                if n1>0:
                    n2 = len(C[0])
                    if verbose>1:
                        print "n2=",n2
                        print "nc=",nc
                    if n2==0 and n1<=1:
                        print "f=",f
                        print "Did not have coefficients file!"
                        print "Should have {0} coefficients!".format(numc)
                        db._collection.update({'_id':idd},{ "$set" :{'Numc': int(0) }})
                        stop=1
                        continue
                    if n2==dim:
                        n3=len(C[0][0])
                        if n3==nc:
                            C=C[0]
                            fname=fget.name
                            newcid=fs.put(dumps(C),
                                          filename=fname,
                                          maass_id=idd,
                                          level=f.get('Level',int(0)),
                                          weight=f.get('Weight',int(0)),
                                          eigenvalue=f.get('Eigenvalue',float(0)),
                                          numc=int(numc))
                            if newcid:
                                db._collection.update({'_id':idd},{ "$set" :{'coeff_id': newcid }})
                                fs.delete(cid)
                            stop=0
                            print "OK, fixed by shifting!"
                            continue
                    if n2<>nc:
                        stop=1
                        print "Wrong length of second list!"
                        n3=len(C[0][0])
                        print "n2=",n2
                        print "n3=",n3
                        print "dim=",dim
                        print "nc=",nc
                        if n2==dim and n3==nc:
                            C=C[0]
                            fname=fget.name
                            newcid=fs.put(dumps(C),
                                          filename=fname,
                                          maass_id=idd,
                                          level=f.get('Level',int(0)),
                                          weight=f.get('Weight',int(0)),
                                          eigenvalue=f.get('Eigenvalue',float(0)),
                                          numc=int(numc))
                            if newcid:
                                db._collection.update({'_id':idd},{ "$set" :{'coeff_id': newcid }})
                                fs.delete(cid)
                            stop=0
                            print "OK, fixed by shifting!"

        if stop==1:
            i=1+i
            print "Did not have appropriate coefficients file!"
            print "Should have {0} coefficients!".format(numc)
            if dryrun==0:
                db._collection.update({'_id':idd},{ "$set" :{'Numc': int(0) }})
                fs.delete(cid)
                print "Deleted possibly wrong formatted coefficients!"
            else:
                print "Would have deleted coefficients!"
            if verbose>0:
                print "f=",f
            continue
        else:
            pass #print "OK:",f
    print i


def set_Conrey_character_label(data={},db=None,**kwds):
    if not db:
        return 0
    maassid = db.find_Maass_form_id(data,**kwds)
    #print maassid
    for idd in maassid:
        f = db.get_Maass_forms(idd)
        conrey=f.get('Conrey',False)
        if conrey:
            continue
        chi = f.get('Character',None)
        N = f.get('Level',None)
        if chi==None or N==None:
            continue
        cnr = db.getDircharConrey(N,chi)
        key = {'_id':idd}
        values={"$set":{'Conrey': int(cnr)}}
        for coll in db._show_collection:
            coll.update(key,values,upsert=False)
    return 1


def improve_eigenvalues(data={},db=None,verbose=0,**kwds):
    if not db:
        return 0
    maassid = db.find_Maass_form_id(data,**kwds)
    #print maassid
    for idd in maassid:
        try:
            improve_eigenvalue_from_id(idd,db,verbose=0,**kwds)
        except KeyboardInterrupt:
            break


def improve_eigenvalue_from_id(maassid,db,ndig=10,verbose=0):
    r"""
    Try to refine the eigenvalue corresponding to the maassid up to the desired precision.
    """
    f = db.get_Maass_forms(maassid)
    if len(f)==0:
        return 0
    f  = f[0]
    R  = f.get('Eigenvalue',None)
    N  = f.get('Level',None)
    wt  = f.get('Weight',None)
    ch = f.get('Character',None)
    er = f.get('Error',None)
    date = f.get('date',None)
    st = f.get('Symmetry',None)
    cev= f.get('Cusp_evs',None)
    nc = f.get('Numc',None)
    dim = f.get('dim',None)
    Y  = f.get('Y',0.5)
    M0 = f.get('M0',0)
    ndig=kwds.get('ndig',0)
    if N==None or R==None or ch==None or wt==None or st==None:
        if verbose>0:
            print "Record does not contain sufficient information! \n rec:{0}".format(f)
        return
    if ndig<=0:
        tol=kwds.get('tol',kwds.get('prec',kwds.get('eps',1e-8)))
    else:
        tol=RR(10)**-RR(ndig)
    if er<tol:
        if verbose>0:
            print "Eigenvalue already have enough precision!"
        return
    S = MaassWaveForms(N,k,ch,sym_type=st,cusp_evs=cev)
    if er<>None:
        eps = er/RR(100)
    else:
        eps = 0.1
    l=find_single_ev(S,2.5,2.75,dim=dim,verbose=verbose,**kwds) #verbose=3,method='TwoY',hmax=500,db=DB,hecke_ef=0)
    try:
        R_new,er_new,Y_new,M0_new = l
    except:
        er_new = 1
    if er_new>er:
        if verbose>0:
            print "Find single ev failed!"
        return
    # Else update record
    key ={'_id':maassid}
    vals = {"$set":{"Eigenvalue":float(R_new),"Y":float(Y_new),"Error":float(er_new),"M0":int(M0_new)}}
    idd = db._collection.update(key,vals,upsert=True)
    if idd and verbose>0:
        print "key=",key
        print "vals=",vals
        print "Updated Record!"


def max_assumed_dim(x):
    r"""
    If the character has components of order two the dimension
    of a generic type Maass form doubles.
    Similarly if N has square factors the dimension of generic spaces
    might increase by multiples of two.
    """
    d=1
    if not x.is_trivial():
        for xx in x.decomposition():
            if hasattr(xx,"order"):
                m = xx.order()
            elif hasattr(xx,"multiplicative_order"):
                if xx==0:
                    m=1
                else:
                    m=xx.multiplicative_order()
            else:
                raise ArithmeticError,"Character does not have any order!"
            if m==2:
                d=d*2

    for p,m in factor(x.modulus()):
        if m>1:
            d=d*2
    return d





def insert_coefficients(data,coeffs,db):
    ## First check if we already have coefficients for the form in question.
    maassid = db.find_Maass_form_id(data)

    find_data={'Level':data.get('Level',0),
               'Weight':data.get('Weight',0),
               'Char':data.get('Char')}
    db._collection_coeff
def print_diffs(diffs):
    r"""
    Print the diffs dictionary in a better way.
    
    """
    s = ""
    for j in [1,2,3]:
        s+="diffs[{0}] = {1} \n".format(j,diffs.get(j))
    return s
