## -*- coding: utf-8 -*-
#*****************************************************************************
#  Copyright (C) 2013 Fredrik Strömberg <stroemberg@mathematik.tu-darmstadt.de>,
#
#  Distributed under the terms of the GNU General Public License (GPL)
#
#    This code is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#  The full text of the GPL is available at:
#
#                  http://www.gnu.org/licenses/
#*****************************************************************************
###
### Implements routines for converting different database classes
### (mainly defined in maass_forms_db.py and maass_forms_schema.py

from maass_forms_schema import Character_DB

def CharacterDB(modulus,number):
    r"""
    Fetch or insert new instance of Character_DB
    """
    character = Character_DB.query.filter_by(modulus=modulus,number=number).all()
    if len(character)>0:
        character = character[0]
    else:
        character = Character_DB(modulus=modulus,number=number)
    return character
    
